import React, { useState, Fragment } from 'react';
import {Modal, Spinner, Button} from 'reactstrap';
import Webcam from "react-webcam";
import Notification from "./components/Notification";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Color from '../../../utils/Colors';
import AfirmationModal from './components/AfirmationModal'
import {useHistory} from "react-router-dom"
import {
    translate,
} from 'react-switch-lang';
import { useFormik } from 'formik';
toast.configure()

var formData = new FormData();
var dataPhoto = null;
var dataLatitude = null;
var dataLongitude = null;

const OvertimeIn = (props) => {
    const history = useHistory()
    const [state, setState] = useState({
        isLoading: false,
        isGetLocation: false,
        isSuccess: false,
        isLocNotMatch: false,
        isLocNotAccurate: false,
        isTimeNotMatch: false,
        isFaceNotMatch: false,
        isAlready: false,
        isFailed: false,
        attendanceHistoryId: 0,
        isDetect: true,
        isNotSupport: false,
        isAllowed: false,
        RecognizeUser: null
    });
    const { t } = props;
    const webcamRef = React.useRef(null);
    // eslint-disable-next-line
    const [imgSrc, setImgSrc] = React.useState(null);

    const {values, ...formik} = useFormik ({
        initialValues: {
            latitude: '',
            longitude: '',
            faceImage:''
        }
    });

    // eslint-disable-next-line
    const findCoordinates = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                dataLatitude = position.coords.latitude;
                dataLongitude = position.coords.longitude;
            },
            error => console.log(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    };

    const findCoordinatesLowAccuracy = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                dataLatitude = position.coords.latitude;
                dataLongitude = position.coords.longitude;
            },
            error => console.log(error.message),
        );
    };


    const takePicture = React.useCallback(() => {
        setState({
            isLoading: true,
        })
        dataPhoto = webcamRef.current.getScreenshot();
        sendData();
        // eslint-disable-next-line
    }, [webcamRef, setImgSrc]);

    const showNotification = () => {
        if (state.isSuccess) {
            return (
                <AfirmationModal
                    visible={true}
                    title="Success"
                    text={`${t('Wajah anda berhasil dikenali oleh sistem, silahkan lanjutkan presensi dengan memasukkan PIN anda')}`}
                    onPressButton={() => {
                        setState({ isSuccess: false });
                        history.push({
                            pathname: '/tab/overtimepinin',
                            state: {
                                checkInData: values,
                                userData: state.RecognizeUser.data
                            }
                        });
                    }}
                    titleButton="OK"
                    titleColor={Color.white}
                    bgColorButton={Color.primaryColor}
                />
            );
        }
        if (state.isFaceNotMatch) {
            return (
                <AfirmationModal
                    visible={true}
                    title="Warning !"
                    text={`${t('wajahtidaksesuai')}`}
                    onPressButton={() => {
                        setState({ isFaceNotMatch: false });
                        history.push({
                            pathname: '/tab/overtimepinin',
                            state: {
                                checkInData: values,
                                userData: {
                                    personnelId: null,
                                    userFullName: null
                                }
                            }
                        });
                    }}
                    titleButton="OK"
                    titleColor={Color.white}
                    bgColorButton={Color.primaryColor}
                />
            );
        }
        if (state.isAlready) {
            return (
                <AfirmationModal
                    visible={true}
                    title="Failed"
                    text={t('pernahcheckin')}
                    onPressButton={() => {
                        setState({ isAlready: false });
                        history.push("/tab")
                    }}
                    titleButton="OK"
                    titleColor={Color.white}
                    bgColorButton={Color.primaryColor}
                />
            );
        }
        if (state.isFailed) {
            return (
                <AfirmationModal
                    animationType="none"
                    visible={true}
                    title="Failed"
                    text={t('checkingagal')}
                    onPressButton={() => {
                        setState({ isFailed: false });
                        history.push("/tab")
                    }}
                    titleButton="Kembali"
                    titleColor={Color.white}
                    bgColorButton={Color.primaryColor}
                />
            );
        }
    };
    const DataURIToBlob = (dataURI) => {
        const splitDataURI = dataURI.split(',')
        const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
        const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

        const ia = new Uint8Array(byteString.length)
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i)
        }
        return new Blob([ia], { type: mimeString })
    }
    const sendData = async () => {
        setState({ isLoading: true })
        let file = DataURIToBlob(dataPhoto);
        console.log(file);
        formik.setFieldValue('latitude', dataLatitude);
        formik.setFieldValue('longitude', dataLongitude);
        formik.setFieldValue('faceImage', file, 'faceCheckIn.jpg');

        formData.append('latitude', dataLatitude);
        formData.append('longitude', dataLongitude);
        formData.append('faceImage', file, 'faceCheckIn.jpg');

    const url = '/api/external/attendance/recognize';
        const session = sessionStorage.getItem('session');
        await fetch(url, {
            method: 'POST',
            headers: {
                Authorization: 'Bearer ' + session,
            },
            body: formData,
        })
            .then(response => {
                const statusCode = response.status;
                const data = response.json();
                return Promise.all([statusCode, data]);
            })
            .then(([res, data]) => {
                console.log(res, data);
                if (res === 200) {
                    setState({
                        isLoading: false,
                        isSuccess: true,
                        RecognizeUser: data
                    })
                } else if (res === 404) {
                    setState({
                        isLoading: false,
                        isFaceNotMatch: true,
                    });
                } else if (res === 302) {
                    setState({
                        isLoading: false,
                        isAlready: true,
                    });
                } else {
                    setState({
                        isLoading: false,
                        isFailed: true,
                    });
                }
            })
            .catch(error => {
                console.log('Error:', error);
            });
    };

    const checkGeo = async () => {
        if (navigator.geolocation) {
            await navigator.geolocation.getCurrentPosition(
                position => {
                    dataLatitude = position.coords.latitude;
                    dataLongitude = position.coords.longitude;
                    console.log(dataLatitude)
                    setState({
                        isDetect: false,
                        isNotSupport: false,
                        isAllowed: true,
                    })
                },
                error => setState({
                    isDetect: false,
                    isNotSupport: true,
                    isAllowed: false,
                }),
                { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 });

        } else {
            setState({
                isDetect: false,
                isNotSupport: true,
                isAllowed: false,
            })
        }
    }

    const showNotSupport = () => {
        if (state.isNotSupport) {
            return <Notification
                onClick={() => { setState({ isNotSupport: false }, window.location.replace("/tab")) }}
                visible={true}
                title="Error"
                desc={t('akseskameradilarang')}
                actionTitle="Back"
            />
        }
    };

    const videoConstraints = {
        facingMode: "user"
    };

    return (
        <Fragment>
            {showNotSupport()}
            {showNotification()}

            <div className="margin-30" style={ state.isLoading ? {filter: 'blur(5px)', height: window.outerHeight} : {height: window.outerHeight} }>
                <Button onClick={() => history.goBack()} className="back-tablet-button p-4">
                    <i className="fa fa-times fa-3x" />
                </Button>
                <div className="text-center flexbox-wrapper">
                    <div className="flexboxi-1 text-center">
                        <Webcam
                            audio={false}
                            ref={webcamRef}
                            screenshotFormat="image/jpeg"
                            forceScreenshotSourceSize={true}
                            mirrored={true}
                            style={{ height: '100%', width: '100vw' }}
                            videoConstraints={videoConstraints}
                            onUserMedia={checkGeo}
                            onUserMediaError={()=>{setState({
                                isDetect: false,
                                isNotSupport: true,
                                isAllowed: false,
                            })}

                            }
                        />
                        <button className="button-capture" onClick={() => {
                            if (dataLatitude !== null) {
                                setState({
                                    isLoading: true,
                                })
                                takePicture();
                            } else {
                                setState({
                                    isLoading: true,
                                })
                                findCoordinatesLowAccuracy();
                                setTimeout(() => {
                                    takePicture();
                                }, 5000);
                            }
                        }} />

                    </div>
                </div>
            </div>

            <Modal className="text-center p-3" backdropClassName="back-home" contentClassName="modal-home shadow" style={{borderRadius:"15px", marginTop:"40vh", height:"50%"}} isOpen={state.isLoading}>
                <Spinner size="xl" className="mx-auto mt-3" />
                <h2 className="mt-2 mb-3">Tunggu Sebentar...</h2>
            </Modal>
        </Fragment>
    );

};


export default translate(OvertimeIn);
