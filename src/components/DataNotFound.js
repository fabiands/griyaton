import React from 'react';
import Illustration from '../assets/assets_ari/481.png';

const DataNotFound = () => {
    return (
        <div className="text-center">
            <img src={Illustration} alt="not found" />
        </div>
    );
}

export default DataNotFound;
