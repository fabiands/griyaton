import React, { useState, useEffect } from "react";
import request from "../../../utils/request";
import { Table, Button, Modal, ModalBody, Spinner, Card, CardBody, Row, Col, ModalHeader, Progress } from "reactstrap";
import { Link } from "react-router-dom";
import * as moment from 'moment';
import LoadingAnimation from "../../../components/LoadingAnimation";
import { toast } from 'react-toastify';
import { t } from 'react-switch-lang';
import { useUserPrivileges } from "../../../store";
import { useSelector } from "react-redux";
import ReactMarkdown from "react-markdown";
import { Responsive } from "responsive-react/dist/Responsive";
import { Radar } from "react-chartjs-2";
import Star from "../../../components/Star";
import DiscResultAdmin from "../Assesment/disc/DiscResultAdmin";
toast.configure();

function PersonnelAssessmentResult({ personnel }) {
    const [loading, setLoading] = useState(true);
    const [results, setResults] = useState([]);
    const { can } = useUserPrivileges();
    const isAdminPanel = useSelector(state => state.isAdminPanel);

    useEffect(() => {
        const url = `v1/personnels/${personnel.id}/assessments`;
        request.get(url).then(res => {
            setResults(res.data.data);
        }).finally(() => {
            setLoading(false);
        });
    }, [personnel.id]);

    if (loading) {
        return <LoadingAnimation />
    }

    const handleDeleted = (deletedId) => {
        setResults(results.filter(r => r.id !== deletedId))
    }

    const mbti = results.find((ass) => ass.testName === 'mbti')
    const papikostick = results.find((ass) => ass.testName === 'papikostick')
    const disc = results.find((ass) => ass.testName === 'disc')

    return (<div>
        <AssessmentMbti resultId={mbti?.id} isAdminPanel={isAdminPanel} can={can} onDeleted={handleDeleted} />
        <AssessmentPapikostick resultId={papikostick?.id} isAdminPanel={isAdminPanel} can={can} onDeleted={handleDeleted} />
        <AssesmentDisc resultId={disc?.id} isAdminPanel={isAdminPanel} can={can} onDeleted={handleDeleted} />
    </div>)
}

const AssessmentMbti = ({ resultId, isAdminPanel, can, onDeleted }) => {
    const [modalAses, setModalAses] = useState(false);
    const toggleAses = () => setModalAses(!modalAses);
    const [result, setResult] = useState(null);
    const [loading, setLoading] = useState(false);
    const [deletingId, setDeletingId] = useState(null);
    const [deleteLoading, setDeleteLoading] = useState(false);
    let deleteButton = false;

    if (isAdminPanel) {
        deleteButton = can('delete-assessment-result')
    }

    useEffect(() => {
        if (resultId) {
            setLoading(true)
            request.get('v1/assessment/results/' + resultId)
                .then(res => {
                    setResult(res.data.data);
                })
                .finally(() => setLoading(false))
        }
    }, [resultId])

    if (loading) {
        return <LoadingAnimation />
    }

    function doDelete() {
        if (!can('delete-assessment-result')) {
            toast.error('Maaf anda tidah boleh melakukan aksi ini.')
            return
        }
        if (deleteLoading) return;
        setDeleteLoading(true);
        request.delete('v1/assessment/results/' + deletingId)
            .then(() => {
                setDeletingId(null);
                onDeleted(deletingId)
                toast.success('Berhasil dihapus.');
            })
            .catch(err => {
                toast.error('Terjadi kesalahan');
                throw err;
            })
            .finally(() => {
                setDeleteLoading(false);
            });
    }

    if (!result) {
        return <>
            <Card>
                <CardBody>
                    <Row className="md-company-header mb-3 mt-2">
                        <Col className="d-flex flex-column justify-content-center align-items-center">
                            <h5 className="content-sub-title mb-0">{t('Test MBTI')}</h5>
                            <small>Anda belum melakukan assesment ini</small>
                            <br />
                            <Link to="/assessment/mbti-tes" className="btn btn-netis-primary btn-md">Lakukan Asesmen</Link>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </>
    }
    return (
        <>
            <Card>
                <CardBody>
                    <Row className="md-company-header mb-3 mt-2">
                        <Col sm="6" md="6" lg="6">
                            <h5 className="text-uppercase content-sub-title mb-0">
                                {result.testName} - {result.result.type}
                            </h5>
                        </Col>
                        <Col sm="6" md="6" lg="6" className="text-right">
                            <Button className="btn btn-netis-color" onClick={toggleAses}>
                                {t("lihatdetail")}
                            </Button>
                            {deleteButton &&
                                <Button color="netis-danger ml-2" onClick={() => setDeletingId(result.id)}>{t("hapus")}</Button>
                            }
                        </Col>

                        <div className="col-12 d-flex mb-3 text-muted">
                            <i>{moment(result.created_at).format("DD MMMM YYYY")}</i>
                        </div>
                    </Row>

                    <div className="p-3 rounded border">
                        <ProgressGroup
                            typeA={"Introvert"}
                            valueA={result.scores.introvert}
                            typeB={"Extrovert"}
                            valueB={result.scores.extrovert}
                        />
                        <ProgressGroup
                            typeA={"Sensing"}
                            valueA={result.scores.sensing}
                            typeB={"Intuition"}
                            valueB={result.scores.intuition}
                        />
                        <ProgressGroup
                            typeA={"Feeling"}
                            valueA={result.scores.feeling}
                            typeB={"Thinking"}
                            valueB={result.scores.thinking}
                        />
                        <ProgressGroup
                            typeA={"Judging"}
                            valueA={result.scores.judging}
                            typeB={"Perceiving"}
                            valueB={result.scores.perceiving}
                        />
                    </div>
                </CardBody>
            </Card>

            <Modal isOpen={modalAses} toggle={toggleAses} className="modal-lg">
                <ModalHeader toggle={toggleAses}>{t("detailasesmen")}</ModalHeader>
                <ModalBody>
                    <div className="mb-8">
                        <h3>{t("karakteristik")}</h3>
                        <ReactMarkdown source={result.result.characterisctics} />
                    </div>

                    <div className="mb-3 mt-2">
                        <h3>{t("fungsikognitif")}</h3>
                        <h4 className="h5">
                            <i>{t("kemampuanberpikir")}</i>
                        </h4>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <div className="border rounded p-3 h-100">
                                <h5>{t("fungsidominan")}</h5>
                                <i>
                                    <ReactMarkdown source={result.result.dominan.name} />
                                </i>
                                <ReactMarkdown source={result.result.dominan.desc} />
                            </div>
                        </div>

                        <div className="col-sm-6">
                            <div className="border rounded p-3 h-100">
                                <h5>{t("fungsisekunder")}</h5>
                                <i>
                                    <ReactMarkdown source={result.result.sekunder.name} />
                                </i>
                                <ReactMarkdown source={result.result.sekunder.desc} />
                            </div>
                        </div>
                        <div className="col-sm-12 mt-3">
                            <h3>{t("partneralami")}</h3>
                            <span className="text-uppercase h5">
                                &nbsp;&nbsp;&nbsp;&nbsp;<i>{result.result.partner1}</i> &{" "}
                                <i>{result.result.partner2}</i>
                            </span>
                        </div>
                        <div className="col-sm-12 mt-3">
                            <h3>{t("saranpengembangan")}</h3>
                            <ReactMarkdown source={result.result.suggestion} />
                        </div>
                    </div>
                </ModalBody>
            </Modal>

            <Modal isOpen={!!deletingId} toggle={() => {
                if (!deleteLoading) {
                    setDeletingId(null)
                }
            }}>
                <ModalBody>
                    <h6>{t('yakinmenghapus')}</h6>
                    <div className="d-flex justify-content-end">
                        {!deleteLoading && <Button className="mr-2" color="light" onClick={() => setDeletingId(null)}>{t('batal')}</Button>}
                        <Button color="netis-primary" onClick={() => doDelete()} disabled={deleteLoading}>
                            {deleteLoading ? <React.Fragment><Spinner size="sm" color="light" /> menghapus...</React.Fragment> : t('hapus')}
                        </Button>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
};

const AssessmentPapikostick = ({ resultId, isAdminPanel, can, onDeleted }) => {
    const [modalPapi, setModalPapi] = useState(false);
    const togglePapi = () => setModalPapi(!modalPapi);
    const [result, setResult] = useState(null);
    const [loading, setLoading] = useState(false);
    const [deletingId, setDeletingId] = useState(null);
    const [deleteLoading, setDeleteLoading] = useState(false);
    let deleteButton = false;

    if (isAdminPanel) {
        deleteButton = can('delete-assessment-result')
    }

    useEffect(() => {
        if (resultId) {
            setLoading(true)
            request.get('v1/assessment/results/' + resultId)
                .then(res => {
                    setResult(res.data.data);
                })
                .finally(() => setLoading(false))
        }
    }, [resultId])

    if (loading) {
        return <LoadingAnimation />
    }
    const groupingLabel = {
        F: { category: "Followership", color: "#e53935" },
        W: { category: "Followership", color: "#e53935" },
        N: { category: "Work Direction", color: "#8e24aa" },
        G: { category: "Work Direction", color: "#8e24aa" },
        A: { category: "Work Direction", color: "#8e24aa" },
        L: { category: "Leadership", color: "#3949ab" },
        P: { category: "Leadership", color: "#3949ab" },
        I: { category: "Leadership", color: "#3949ab" },
        T: { category: "Activity", color: "#039be5" },
        V: { category: "Activity", color: "#039be5" },
        X: { category: "Social Nature", color: "#00897b" },
        S: { category: "Social Nature", color: "#00897b" },
        B: { category: "Social Nature", color: "#00897b" },
        O: { category: "Social Nature", color: "#00897b" },
        C: { category: "Work Style", color: "#7cb342" },
        D: { category: "Work Style", color: "#7cb342" },
        R: { category: "Work Style", color: "#7cb342" },
        Z: { category: "Temperament", color: "#fb8c00" },
        E: { category: "Temperament", color: "#fb8c00" },
        K: { category: "Temperament", color: "#fb8c00" },
    };

    if (!result) {
        return <>
            <Card>
                <CardBody>
                    <Row className="md-company-header mb-3 mt-2">
                        <Col className="d-flex flex-column justify-content-center align-items-center">
                            <h5 className="content-sub-title mb-0">{t('TesPapikostick')}</h5>
                            <small>Anda belum melakukan assesment ini</small>
                            <br />
                            <Link to="/assessment/papikostick-tes" className="btn btn-netis-primary btn-md">Lakukan Asesmen</Link>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </>
    }
    const colorsByIndex = Object.keys(groupingLabel).map(
        (label) => groupingLabel[label].color
    );

    const data = {
        labels: "FWNGALPITVXSBORDCZEK".split(""),
        datasets: [
            {
                label: "RESULT",
                borderWidth: 1,
                pointRadius: 2,
                backgroundColor: "rgba(0,0,0,0.3)",
                borderColor: (option) => {
                    return colorsByIndex[option.dataIndex];
                },
                pointBackgroundColor: (option) => colorsByIndex[option.dataIndex],
                pointBorderColor: (option) => colorsByIndex[option.dataIndex],
                data: "FWNGALPITVXSBORDCZEK".split("").map((huruf) => {
                    if (huruf === "Z") {
                        return 9 - result.scores.huruf;
                    } else if (huruf === "K") {
                        return 9 - result.scores.huruf;
                    } else return result.scores.huruf;
                }),
            },
        ],
    };

    const options = {
        tooltips: {
            callbacks: {
                title: (tooltipItem, data) => {
                    return (
                        groupingLabel[data.labels[tooltipItem[0].index]].category +
                        " (" +
                        data.labels[tooltipItem[0].index] +
                        ")"
                    );
                },
                label: (tooltipItem) => parseInt(tooltipItem.value),
            },
        },
        legend: {
            display: false,
        },
        title: {
            display: true,
            // text: 'Hasil PAPIKostick'
        },
        scale: {
            gridLines: {
                display: false,
                circular: true,
            },
            angleLines: {
                // display: false,
            },
            ticks: {
                display: false,
                max: 9,
                min: 0,
                stepSize: 1,
                beginAtZero: true,
                showLabelBackdrop: true,
            },
            pointLabels: {
                display: false,
                fontStyle: "bold",
                fontSize: 12,
                fontColor: Object.keys(groupingLabel).map(
                    (label) => groupingLabel[label].color
                ),
            },
        },
    };

    const template = {
        Followership: ["F", "W"],
        "Work Direction": ["N", "G", "A"],
        Leadership: ["L", "P", "I"],
        Activity: ["T", "V"],
        "Social Nature": ["X", "S", "B", "O"],
        "Work Style": ["C", "D", "R"],
        Temperament: ["Z", "E", "K"],
    };
    const groupingDesc = {
        F: t("Keb Membantu Atasan"),
        W: t("Keb Mengikuti Aturan dan Pengawasan"),
        N: t("Keb dalam Menyelesaikan Tugas (Kemandirian)"),
        G: t("Peran Pekerja Keras"),
        A: t("Keb dalam Berprestasi"),
        L: t("Peran Kepemimpinan"),
        P: t("Keb Mengatur Orang Lain"),
        I: t("Peran Dalam Membuat Keputusan"),
        T: t("Peran Dalam Kesibukan Kerja"),
        V: t("Peran Dalam Semangat Kerja"),
        X: t("Keb untuk Diperhatikan"),
        S: t("Peran Dalam Hubungan Sosial"),
        B: t("Keb Diterima dalam Kelompok"),
        O: t("Keb Kedekatan dan Kasih Sayang"),
        C: t("Peran Dalam Mengatur"),
        D: t("Peran Bekerja dengan Hal-hal Rinci"),
        R: t("Peran Penalaran Teoritis"),
        Z: t("Keb untuk Berubah"),
        E: t("Peran Dalam Pengendalian Emosi"),
        K: t("Keb untuk Agresif"),
    };

    const group =
        result.result &&
        Object.keys(result.result).map((category) => {
            const item =
                result.result &&
                template[category].map((code, idx) => ({
                    code: code,
                    scores: result.scores[code],
                    description: result.result[category][idx],
                }));

            return {
                kategory: category,
                items: item,
            };
        });

    function titleColor(resultTitle) {
        switch (resultTitle) {
            case "Followership":
                return "#e53935";
            case "Work Direction":
                return "#8e24aa";
            case "Leadership":
                return "#3949ab";
            case "Activity":
                return "#039be5";
            case "Social Nature":
                return "#00897b";
            case "Work Style":
                return "#7cb342";
            case "Temperament":
                return "#fb8c00";
            default:
                return "#FFFFFF";
        }
    }

    function doDelete() {
        if (!can('delete-assessment-result')) {
            toast.error('Maaf anda tidah boleh melakukan aksi ini.')
            return
        }
        if (deleteLoading) return;
        setDeleteLoading(true);
        request.delete('v1/assessment/results/' + deletingId)
            .then(() => {
                setDeletingId(null);
                onDeleted(deletingId)
                toast.success('Berhasil dihapus.');
            })
            .catch(err => {
                toast.error('Terjadi kesalahan');
                throw err;
            })
            .finally(() => {
                setDeleteLoading(false);
            });
    }

    return (
        <>
            <Card>
                <CardBody>
                    <Row className="md-company-header mb-3 mt-2">
                        <Col sm="6" md="6" lg="6">
                            <h5 className="content-sub-title mb-0">PAPI Kostick</h5>
                        </Col>
                        <Col sm="6" md="6" lg="6" className="text-right">
                            <Button className="btn btn-netis-color" onClick={togglePapi}>
                                {t("lihatdetail")}
                            </Button>
                            {deleteButton &&
                                <Button color="netis-danger ml-2" onClick={() => setDeletingId(result.id)}>{t("hapus")}</Button>
                            }
                        </Col>
                    </Row>

                    <div className="mx-auto grafik-papikostick-admin">
                        <Responsive displayIn={["mobile"]}>
                            <Radar data={data} options={options} width={100} height={92} />
                        </Responsive>
                        <Responsive displayIn={["tablet"]}>
                            <Radar data={data} options={options} width={100} height={67} />
                        </Responsive>
                        <Responsive displayIn={["laptop"]}>
                            <Radar data={data} options={options} width={100} height={48} />
                        </Responsive>
                    </div>
                </CardBody>
            </Card>

            <Modal isOpen={modalPapi} toggle={togglePapi} className="modal-xl">
                <ModalHeader toggle={togglePapi}>Hasil PAPI Kostick</ModalHeader>
                <ModalBody>
                    <Table responsive className="table-black">
                        <thead>
                            <tr className="border-bottom">
                                <th className="text-center w-10">{t("Kategori")}</th>
                                <th className="text-center w-25">{t("Aspek")}</th>
                                <th className="text-center w-15">{t("Nilai")}</th>
                                <th className="text-center w-50">{t("deskripsi")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {group &&
                                group.map((objKategori) => {
                                    return (
                                        objKategori &&
                                        objKategori.items.map((objItem, idx) => (
                                            <tr>
                                                {idx === 0 && (
                                                    <td
                                                        rowSpan={objKategori.items.length}
                                                        style={{
                                                            backgroundColor: titleColor(objKategori.kategory),
                                                            color: "#FFFFFF",
                                                        }}
                                                    >
                                                        {objKategori.kategory}
                                                    </td>
                                                )}
                                                <td>
                                                    {objItem.code} : {groupingDesc[objItem.code]}
                                                </td>
                                                <td>
                                                    <Star value={(objItem.scores + 1) / 2} />(
                                                    {objItem.scores})
                                                </td>
                                                <td>{objItem.description}</td>
                                            </tr>
                                        ))
                                    );
                                })}
                        </tbody>
                    </Table>
                </ModalBody>
            </Modal>
            <Modal isOpen={!!deletingId} toggle={() => {
                if (!deleteLoading) {
                    setDeletingId(null)
                }
            }}>
                <ModalBody>
                    <h6>{t('yakinmenghapus')}</h6>
                    <div className="d-flex justify-content-end">
                        {!deleteLoading && <Button className="mr-2" color="light" onClick={() => setDeletingId(null)}>{t('batal')}</Button>}
                        <Button color="netis-primary" onClick={() => doDelete()} disabled={deleteLoading}>
                            {deleteLoading ? <React.Fragment><Spinner size="sm" color="light" /> menghapus...</React.Fragment> : t('hapus')}
                        </Button>
                    </div>
                </ModalBody>
            </Modal>
        </>

    );
};

const AssesmentDisc = ({ resultId, isAdminPanel, can, onDeleted }) => {
    const [result, setResult] = useState(null);
    const [loading, setLoading] = useState(false);
    const [deletingId, setDeletingId] = useState(null);
    const [deleteLoading, setDeleteLoading] = useState(false);
    let deleteButton = false;

    if (isAdminPanel) {
        deleteButton = can('delete-assessment-result')
    }

    useEffect(() => {
        if (resultId) {
            setLoading(true)
            request.get('v1/assessment/results/' + resultId)
                .then(res => {
                    setResult(res.data.data);
                })
                .finally(() => setLoading(false))
        }
    }, [resultId])

    if (loading) {
        return <LoadingAnimation />
    }

    function doDelete() {
        if (!can('delete-assessment-result')) {
            toast.error('Maaf anda tidah boleh melakukan aksi ini.')
            return
        }
        if (deleteLoading) return;
        setDeleteLoading(true);
        request.delete('v1/assessment/results/' + deletingId)
            .then(() => {
                setDeletingId(null);
                onDeleted(deletingId)
                toast.success('Berhasil dihapus.');
            })
            .catch(err => {
                toast.error('Terjadi kesalahan');
                throw err;
            })
            .finally(() => {
                setDeleteLoading(false);
            });
    }

    if (!result) {
        return <>
            <Card>
                <CardBody>
                    <Row className="md-company-header mb-3 mt-2">
                        <Col className="d-flex flex-column justify-content-center align-items-center">
                            <h5 className="content-sub-title mb-0">{t('Test MBTI')}</h5>
                            <small>Anda belum melakukan assesment ini</small>
                            <br />
                            <Link to="/assessment/mbti-tes" className="btn btn-netis-primary btn-md">Lakukan Asesmen</Link>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </>
    }
    return (
        <>
            <Card>
                <CardBody>
                    <Row className="md-company-header mb-3 mt-2">
                        <Col sm="6" md="6" lg="6">
                            <h5 className="text-uppercase content-sub-title mb-0">
                                {result.testName} - {result.result.type}
                            </h5>
                        </Col>
                        <Col sm="6" md="6" lg="6" className="text-right">
                            {deleteButton &&
                                <Button color="netis-danger ml-2" onClick={() => setDeletingId(result.id)}>{t("hapus")}</Button>
                            }
                        </Col>

                        <div className="col-12 d-flex mb-3 text-muted">
                            <i>{moment(result.created_at).format("DD MMMM YYYY")}</i>
                        </div>
                    </Row>

                    <div className="p-3 rounded border">
                        <DiscResultAdmin result={result} isAdmin={true} />
                    </div>
                </CardBody>
            </Card>
            <Modal isOpen={!!deletingId} toggle={() => {
                if (!deleteLoading) {
                    setDeletingId(null)
                }
            }}>
                <ModalBody>
                    <h6>{t('yakinmenghapus')}</h6>
                    <div className="d-flex justify-content-end">
                        {!deleteLoading && <Button className="mr-2" color="light" onClick={() => setDeletingId(null)}>{t('batal')}</Button>}
                        <Button color="netis-primary" onClick={() => doDelete()} disabled={deleteLoading}>
                            {deleteLoading ? <React.Fragment><Spinner size="sm" color="light" /> menghapus...</React.Fragment> : t('hapus')}
                        </Button>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
};

function ProgressGroup({ typeA, typeB, valueA, valueB }) {
    return (
        <div className="progress-group">
            <div className="progress-group-prepend">
                <span
                    className={
                        "progress-group-text" +
                        (valueA >= valueB ? " font-weight-bold" : "")
                    }
                >
                    {typeA}
                </span>
            </div>

            <div className="progress-group-bars">
                <Progress multi>
                    <Progress bar color="netis-color" value={valueA}>
                        {valueA} %
            </Progress>
                    <Progress bar color="success" value={valueB}>
                        {valueB} %
            </Progress>
                </Progress>
            </div>

            <div className="progress-group-prepend ml-3 text-right">
                <span
                    className={
                        "progress-group-text" +
                        (valueA <= valueB ? " font-weight-bold" : "")
                    }
                >
                    {typeB}
                </span>
            </div>
        </div>
    );
}

export default PersonnelAssessmentResult;
