import React from 'react'
import { Row, Button, Modal, ModalBody } from 'reactstrap';

export default function AfirmationModal(props) {
    return (

        <Modal isOpen={props.visible} className="top-50" autoFocus={true}>
            <ModalBody>
                <Row>
                    <div className="col-12 text-center">
                        <p className="title-upgrade" style={{fontSize:"30pt", color: props.color ?? '#93aad6' }}>{props.title}</p>
                    </div>
                    <div className="col-12 text-center">
                        <p style={{fontSize:"26pt"}}>{props.text}</p>
                    </div>
                    <div className="col-12 text-center">
                        <Row>
                            <div className="col-12 text-center">
                                <Button type="submit" className="btn btn-info" style={{ color: props.titleColor, width: '90%',fontSize:"22pt" }} onClick={props.onPressButton}>
                                    {props.titleButton}
                                </Button>
                            </div>
                        </Row>
                    </div>
                </Row>
            </ModalBody>
        </Modal>

    )
}