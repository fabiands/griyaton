import React, { useState } from 'react';
import { DatePickerInput } from 'rc-datepicker';

const defaultDate = new Date();
const aWeekAgo = defaultDate.getDate() - 7;
defaultDate.setDate(aWeekAgo);

function useFilterDate({ start, end }) {
    const [startState, setFilterStart] = useState(start);
    const [endState, setFilterEnd] = useState(end);

    const dateStartPicker = () => <DatePickerInput {...{
        showOnInputClick: true,
        name: 'start',
        onChange: setFilterStart,
        value: startState,
        maxDate: endState,
        className: 'my-custom-datepicker-component',
        readOnly: true,
        displayFormat: 'DD MMM YYYY'
    }}/>

    const dateEndPicker = () => <DatePickerInput {...{
        showOnInputClick: true,
        name: 'start',
        onChange: setFilterEnd,
        value: endState,
        minDate: startState,
        className: 'my-custom-datepicker-component',
        readOnly: true,
        displayFormat: 'DD MMM YYYY'
    }}/>

    return [{start: startState, end: endState}, { setFilterStart, setFilterEnd, DateStartPickerComponent: dateStartPicker, DateEndPickerComponent: dateEndPicker}]
}

export default useFilterDate;