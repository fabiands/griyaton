import React, {useEffect} from 'react';
import {Card, Label, Table, Button} from 'reactstrap';
import {translate, t} from 'react-switch-lang';
// import PinInput from 'react-pin-input';
import {useHistory, useLocation, Redirect} from "react-router-dom";
import {useFormik} from 'formik'; 

function RegisterPin(){

    const history = useHistory();
    const location = useLocation();
    const registerData = location.state?.registerData;
    const faceData = location.state?.faceData;    

    const {values, isSubmitting, ...formik} = useFormik({
        initialValues: {
            pin:''           
        },
        onSubmit: (values, {setSubmitting}) => {            
            setSubmitting(true);            
            history.push({
                pathname: '/tab/confirmpin',                            
                state: {
                        registerData: registerData,
                        faceData: faceData,
                        pin: values.pin
                    }
            });
            setSubmitting(false);
        }
    });

    useEffect(() => {
        if (values.pin.length === 6){
            formik.handleSubmit();
        }
    // eslint-disable-next-line
    }, [values.pin])

    const resetPin = function(){
        formik.setFieldValue('pin', "");        
    }

    function changePinValue(value){
        formik.setFieldValue('pin', "" + values.pin + value)        
    }

    if (!registerData || !faceData) {
        return (<Redirect to="/tab"></Redirect>);
    }

    return(
        <div className="app flex-row align-items-center background-login background-tablet2">
            <Button onClick={() => history.push('/tab')} className="back-tablet p-2">
                    <i className="fa fa-home fa-4x" />
            </Button>
            <div className="w-100">
                <div className="h-100">
                    <div className="text-center mx-auto welcome-logo mt-0 mb-5" style={{width:"40%", maxHeight:"100px"}} />
                    <div className="text-center mt-2">
                        <div className="w-75 text-center mb-5 mx-auto">                            
                        <Label htmlFor="user" className="input-label">
                            <h3>{t('Buat PIN Anda')}</h3>
                        </Label>                        
                        </div>
                        <div className="d-flex justify-content-center text-center my-5 mx-auto">
                            <Card className="pin-card align-middle mx-2">{values.pin[0] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[1] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[2] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[3] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[4] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[5] ? <i className="fa fa-circle" /> : null}</Card>                        
                        </div>
                        <Table borderless style={{backgroundColor:"#C0C0C0"}} className="mt-5">
                            <tr>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(1)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>1</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(2)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>2</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(3)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>3</b>
                                    </Button>
                                </td>
                            </tr>
                            <tr>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(4)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>4</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(5)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>5</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(6)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>6</b>
                                    </Button>
                                </td>
                            </tr>
                            <tr>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(7)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>7</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(8)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>8</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(9)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>9</b>
                                    </Button>
                                </td>
                            </tr>
                            <tr>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={resetPin} className="w-100 text-center rounded" color="netis-color" style={{fontSize:"18pt"}}>
                                        <b>Reset</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(0)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>0</b>
                                    </Button>
                                </td>                                
                            </tr>
                        </Table>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    )
}

export default translate(RegisterPin);