import React, { Component, Fragment } from 'react';
import { Table, Col, Row, Input, Button, Modal, ModalBody, FormGroup, Label, Spinner, InputGroup, InputGroupAddon, InputGroupText, Badge, UncontrolledTooltip } from 'reactstrap';
import MapMarker from '../../Personnel/component/atom/MapMarker';
import GoogleMapReact from 'google-map-react';
import { connect } from 'react-redux';
import { DatePickerInput } from 'rc-datepicker';
import { toast } from 'react-toastify';
import moment from '../../../../utils/moment';
import { formatDate } from '../../../../utils/formatter';
import request from '../../../../utils/request';
import {
    translate,
} from 'react-switch-lang';
toast.configure()
class AttendanceHistoryAll extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dropdownOpen: [false, false],
            session: props.token,
            loadingMatch: false,
            loadingNotMatch: false,
            modalDetail: false,
            idEditData: '',
            attendanceDetail: {},
            message: {
                message: ''
            },
            pahFaceFile: '',
            FaceIsMatch: false,
            latLng: {},
            date: new Date(Date.now() - 7 * 24 * 60 * 60 * 1000),
            dateEnd: new Date(),
            search: '',
            start: formatDate(new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)),
            end: formatDate(new Date()),
            resMessage: '',
            confirmFace: false,
            attendanceHistory: [],
            downloadingExcel: false,
            userPrivileges : this.props.user.privileges
        };
        this.modalDetail = this.modalDetail.bind(this);
    }

    componentDidMount = () => {
        this.getDataFromAPI(this.state.start, this.state.end)
    }

    modalDetail = (data) => {
        const attendanceDetail = { ...this.state.attendanceDetail, type: data.type, time: data.time }
        const message = { ...this.state.message, message: data.message }
        const latLng = { ...this.state.latLng, lat: data.latitude, lng: data.longitude }
        this.setState({
            modalDetail: !this.state.modalDetail,
            attendanceDetail,
            message,
            latLng,
            idEditData: data.id,
            pahFaceFile: data.pahFaceFile,
            FaceIsMatch: data.FaceIsMatch,
            confirmFace: false
        })
    }

    handleChange = (e) => {
        const message = { ...this.state.message, message: e.target.value }
        this.setState({ message })
    }

    handleChangeDateStart = date => {
        if (date instanceof Date && date.getTime() < this.state.dateEnd.getTime()) {
            this.setState({
                date,
                start: formatDate(date)
            });
        }
        else {
            let dateStart;
            if (this.state.dateEnd) {
                dateStart = new Date(this.state.dateEnd.getTime() - 7 * 24 * 60 * 60 * 1000)
            } else {
                dateStart = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
            }
            this.setState({
                date: dateStart,
                start: formatDate(dateStart)
            })
        }
    }

    handleChangeDateEnd = date => {
        if (date instanceof Date && date.getTime() > this.state.date.getTime()) {
            this.setState({
                dateEnd: date,
                end: formatDate(date)
            });
        } else {
            let dateEnd;
            if (this.state.date) {
                dateEnd = new Date(this.state.date.getTime() + 7 * 24 * 60 * 60 * 1000)
            } else {
                dateEnd = new Date();
            }
            this.setState({
                dateEnd: dateEnd,
                end: formatDate(dateEnd)
            })
        }
    }

    downloadExcel = () => {
        this.setState({ downloadingExcel: true })
        const { start, end } = this.state;

        request.get(`v1/attendance/recaps/excel?start=${start}&end=${end}`, {
            responseType: 'arraybuffer',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                const type = res.headers['content-type']
                const blob = new Blob([res.data], { type: type, encoding: 'UTF-8' })
                let filename = "rekap.xlsx";
                const disposition = res.headers['content-disposition'];
                if (disposition && disposition.indexOf('inline') !== -1) {
                    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    const matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) {
                        filename = matches[1].replace(/['"]/g, '');
                    }
                }

                const URL = window.URL || window.webkitURL;
                const downloadUrl = URL.createObjectURL(blob);
                let newWindow = null;

                const iOS = window.navigator.platform && /iPad|iPhone|iPod/.test(window.navigator.platform)
                if (iOS) {
                    const reader = new FileReader();
                    reader.onload = function (e) {
                        newWindow = window.open(reader.result);
                        newWindow.onload = function () {
                            newWindow.document.getElementsByTagName('html')[0]
                                .appendChild(document.createElement('head'))
                                .appendChild(document.createElement('title'))
                                .appendChild(document.createTextNode(filename));
                        }
                        setTimeout(() => {
                            newWindow.document.title = filename;
                        }, 100)
                    }
                    reader.readAsDataURL(blob);
                } else {
                    const link = document.createElement('a')
                    link.href = downloadUrl
                    link.download = filename;
                    link.click();
                    setTimeout(() => {
                        link.remove();
                    }, 1500);
                }
            })
            .catch(err => {
                console.error(err)
            }).finally(() => {
                this.setState({ downloadingExcel: false })
            })
    }

    filterData = () => {
        this.setState({ loading: true })
        this.getDataFromAPI(this.state.start, this.state.end)
    }

    handleSeacrh = (e) => {
        this.setState({ search: e.target.value })
    }

    confirmFace = () => {
        this.setState({ confirmFace: !this.state.confirmFace })
    }

    cancelConfirmFace = () => {
        this.setState({ confirmFace: false })
    }

    changeFaceIsMatch = async (e, id) => {
        if(!this.state.userPrivileges.includes('edit-attendance')){
            toast.error('Maaf anda tidah boleh melakukan aksi ini.')
            return
        }
        this.setState({ loadingMatch: true })
        const faceIsMatch = { faceIsMatch: '1' }
        try {
            await request.post(`v1/personnels/attendance/history/${id}/face`, faceIsMatch);
            this.getDataFromAPI(this.state.start, this.state.end);
            setTimeout(() => {
                this.setState({ loadingMatch: false, modalDetail: !this.state.modalDetail });
            }, 400)
            toast.success('Success', { autoClose: 3000 })
        } catch (err) {
            this.setState({ loadingMatch: false })
            if (err.response && err.response.data) {
                toast.error(err.response.data.message || 'Error')
            }
        }
    }

    changeFaceIsNotMatch = async (e, id) => {
        if(!this.state.userPrivileges.includes('edit-attendance')){
            toast.error('Maaf anda tidah boleh melakukan aksi ini.')
            return
        }
        this.setState({ loadingNotMatch: true })
        const faceIsMatch = { faceIsMatch: '0' }
        try {
            await request.post(`v1/personnels/attendance/history/${id}/face`, faceIsMatch);
            this.getDataFromAPI(this.state.start, this.state.end);
            setTimeout(() => {
                this.setState({ loadingNotMatch: false, modalDetail: !this.state.modalDetail });
            }, 400)
            toast.success('Success', { autoClose: 3000 })
        } catch (err) {
            this.setState({ loadingNotMatch: false })
            if (err.response && err.response.data) {
                toast.error(err.response.data.message || 'Error')
            }
        }
    }

    getDataFromAPI = (start, end) => {
        request.get(`v1/attendance/company?start=${start}&end=${end}`)
            .then((res) => {
                const attendanceHistory = res.data.data;
                setTimeout(() => {
                    this.setState({ attendanceHistory }, () => this.setState({ loading: false }))
                }, 500)
            })
            .catch(error => {
                setTimeout(() => {
                    this.setState({ loading: false })
                }, 500)
            });
    }

    render() {
        const { t } = this.props;
        moment.locale(t('id'))
        // eslint-disable-next-line
        const attendanceHistory = this.state.attendanceHistory.filter((data) => {
            if (this.state.search === null)
                return data;
            else if ((data.name || '').toLowerCase().includes(this.state.search.toLowerCase())) {
                return data;
            }
        }).map((data, idx) => {
            return (
                <tr key={idx}>
                    <td className="w-30">{data.name}</td>
                    <td>{t(data.type)}</td>
                    <td className="text-center">{moment(data.date).format('DD MMMM YYYY')}</td>
                    <td className="">
                        {data.time?.substr(0, 5)} {data.type === 'clockIn' && !data.timeIsMatch ?
                            <>
                                <i id="late" className="fa fa-exclamation-circle text-danger"></i>
                                <UncontrolledTooltip placement="top" target="late">{t('terlambat')}</UncontrolledTooltip>
                            </>
                            : null
                        }
                    </td>
                    <td className="text-center">{data.FaceIsMatch ? <Badge color="success">{t('sesuai')}</Badge> : <Badge color="danger">{t('tidaksesuai')}</Badge>}</td>
                    <td className="text-center">{data.loc.name}</td>
                    <td className="text-center">
                        <p className={`action-text${this.state.userPrivileges.includes('read-attendance') ? '' : ' d-none'}`} onClick={() => this.modalDetail(data)}>Details</p>
                    </td>
                </tr>
            );
        });

        return (
            <div className="animated fadeIn" >
                <h4 className="content-title mb-4">{t('riwayat')} {t('absensi')}</h4>
                <div className="content">
                    <Row className="md-company-header mb-3">
                        <Col xs="6" md="2" >
                            <FormGroup>
                                <Label htmlFor="startDate" className="input-label">{t('dari')}</Label>
                                <DatePickerInput
                                    name="startDate"
                                    id="startDate"
                                    onChange={this.handleChangeDateStart}
                                    value={this.state.date}
                                    className='my-custom-datepicker-component'
                                    showOnInputClick={true}
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6" md="2" >
                            <FormGroup>
                                <Label htmlFor="endDate" className="input-label">{t('hingga')}</Label>
                                <DatePickerInput
                                    name="endDate"
                                    id="endDate"
                                    onChange={this.handleChangeDateEnd}
                                    value={this.state.dateEnd}
                                    className='my-custom-datepicker-component'
                                    showOnInputClick={true}
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="8" md="3" className="mt-sm-1 pt-sm-4" >
                            <InputGroup className="">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText className="input-group-transparent">
                                        <i className="fa fa-search"></i>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" placeholder={t('cari') + ' ' + t('nama')} className="input-search" onChange={this.handleSeacrh} />
                            </InputGroup>
                        </Col>
                        <Col xs="4" md="2" >
                            <div className="pt-sm-2">
                                <Button type="submit" className="mt-sm-4" color="netis-color" style={{ width: '100%' }} onClick={this.filterData}>
                                    {this.state.loading ? <Spinner color="light" size="sm" /> : 'Filter'}
                                </Button>
                            </div>
                        </Col>
                        <Col md="3" className="text-md-right">
                            <div className="pt-2">
                                <Button onClick={this.downloadExcel} className={`mt-sm-4${this.state.userPrivileges.includes('download-attendance') ? '' : ' d-none'}`} color="success">
                                    {this.state.downloadingExcel ? <Fragment><Spinner size="sm" /> Downloading...</Fragment> : <Fragment><i className="fa fa-download mr-2"></i> {t('unduhlaporan')}</Fragment>}
                                </Button>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" lg="12">
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th className="w-30">{t('nama')}</th>
                                        <th className="w-10">{t('jenis')}</th>
                                        <th className="text-center w-20">{t('tanggal')}</th>
                                        <th className="w-10">{t('jam')}</th>
                                        <th className="text-center w-10">{t('wajah')}</th>
                                        <th className="text-center w-20">{t('lokasi')}</th>
                                        <th className="text-center w-10"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.attendanceHistory.length > 0 ?
                                        attendanceHistory
                                        :
                                        <tr key={"no_data"}>
                                            <td className="text-center no-data-message" colSpan="5">No Data :(</td>
                                        </tr>
                                    }
                                </tbody>
                            </Table>
                        </Col>
                    </Row>

                    {/* Modal Box Detail Data */}
                    <Modal isOpen={this.state.modalDetail} toggle={this.modalDetail} className={'modal-lg ' + this.props.className}>
                        <ModalBody>
                            <div className="d-flex justify-content-between align-items-center mb-4">
                                <h5 className="content-sub-title mb-0">Details {t('riwayat')} {t('absensi')}</h5>
                                <Button color="link" size="lg" className="text-danger" onClick={this.modalDetail}><strong>&times;</strong></Button>
                            </div>
                            <Row>
                                <div className="col-6">
                                    <FormGroup>
                                        <Label htmlFor="jenis" className="input-label">{t('jenis')} {t('absensi')}</Label>
                                        <Input type="text" name="jenis" id="jenis" placeholder={t('jenis') + ' ' + t('absensi')}
                                            disabled={true}
                                            defaultValue={t(this.state.attendanceDetail.type || 'clockIn')} />
                                    </FormGroup>
                                </div>
                                <div className="col-6">
                                    <FormGroup>
                                        <Label htmlFor="waktu" className="input-label">{t('waktu')} {t('absensi')}</Label>
                                        <Input type="text" name="waktu" id="waktu" placeholder={t('waktu') + ' ' + t('absensi')}
                                            disabled={true}
                                            defaultValue={moment(this.state.attendanceDetail.time, 'HH:mm:ss').format('HH:mm')} />
                                    </FormGroup>
                                </div>
                            </Row>
                            <Row className="mb-2">
                                <div className="col-md-6">
                                    <div className="d-flex justify-content-center">
                                        <img src={this.state.pahFaceFile} style={{ maxHeight: '325px' }} className="img-fluid" alt="personnel-face" />
                                    </div>
                                    <div className="d-flex justify-content-center">
                                        {this.state.FaceIsMatch ?
                                            <div className="mb-3">
                                                <h6 className="mt-2 mb-0 text-success text-center">{t('wajahsesuai')}<i className="fa fa-check"></i></h6>
                                                <div className="text-center">
                                                    {this.state.confirmFace || <Button color="netis-color" className={`pt-0 pb-0 pl-2 pr-2${this.state.userPrivileges.includes('edit-attendance') ? '' : ' d-none'}`} onClick={this.confirmFace}>{t('ubah')}</Button>}
                                                </div>
                                                {this.state.confirmFace &&
                                                    <div className="d-flex items-align-center justify-content-between">
                                                        <div>
                                                            <Button color="success" className="mr-1 pt-0 pb-0 pl-2 pr-2" disabled={this.state.loadingMatch} onClick={(e) => this.changeFaceIsMatch(e, this.state.idEditData)}>
                                                                {this.state.loadingMatch ? <Spinner color="light" size="sm" /> : t('sesuai')}
                                                            </Button>
                                                            <Button color="warning" className="ml-1 pt-0 pb-0 pl-2 pr-2" disabled={this.state.loadingNotMatch} onClick={(e) => this.changeFaceIsNotMatch(e, this.state.idEditData)}>
                                                                {this.state.loadingNotMatch ? <Spinner color="light" size="sm" /> : t('tidaksesuai')}
                                                            </Button>
                                                        </div>
                                                        <Button color="link" size="lg" className="text-danger p-0 ml-3" onClick={this.confirmFace}><strong>&times;</strong></Button>
                                                    </div>
                                                }
                                            </div>
                                            :
                                            <div className="mb-3">
                                                <h6 className="mt-2 mb-0 text-warning text-center">
                                                    {t('wajahbelumsesuai')} <i className="fa fa-times"></i>
                                                </h6>
                                                <div className="text-center">
                                                    {this.state.confirmFace || <Button color="netis-color" className={`pt-0 pb-0 pl-2 pr-2${this.state.userPrivileges.includes('edit-attendance') ? '' : ' d-none'}`} onClick={this.confirmFace}>{t('ubah')}</Button>}
                                                </div>
                                                {this.state.confirmFace &&
                                                    <div className="d-flex items-align-center justify-content-between">
                                                        <div>
                                                            <Button color="success" className="mr-1 pt-0 pb-0 pl-2 pr-2" disabled={this.state.loadingMatch} onClick={(e) => this.changeFaceIsMatch(e, this.state.idEditData)}>
                                                                {this.state.loadingMatch ? <Spinner color="light" size="sm" /> : t('sesuai')}
                                                            </Button>
                                                            <Button color="warning" className="ml-1 pt-0 pb-0 pl-2 pr-2" disabled={this.state.loadingNotMatch} onClick={(e) => this.changeFaceIsNotMatch(e, this.state.idEditData)}>
                                                                {this.state.loadingNotMatch ? <Spinner color="light" size="sm" /> : t('tidaksesuai')}
                                                            </Button>
                                                        </div>
                                                        <Button color="link" size="lg" className="text-danger p-0 ml-3" onClick={this.confirmFace}><strong>&times;</strong></Button>
                                                    </div>
                                                }
                                            </div>
                                        }
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div style={{ height: '350px', width: '100%' }}>
                                        <GoogleMapReact
                                            bootstrapURLKeys={{ key: "AIzaSyDQsNCd2Trmf4MLwcB7k1oqpWZPpTeCkc0" }}
                                            center={this.state.latLng}
                                            defaultZoom={17}
                                        >
                                            <MapMarker
                                                lat={this.state.latLng.lat}
                                                lng={this.state.latLng.lng}
                                                text={""}
                                            />
                                        </GoogleMapReact>
                                    </div>
                                </div>
                            </Row>
                            <Row>
                                <div className="col-12">
                                    <FormGroup>
                                        <Label htmlFor="message" className="input-label">{t('pesan')} {t('karyawan')}</Label>
                                        <Input type="text" name="message" id="message" placeholder={t('pesan') + ' ' + t('karyawan')}
                                            disabled={true}
                                            value={this.state.message.message || ''}
                                            onChange={this.handleChange} />
                                    </FormGroup>
                                </div>
                            </Row>
                        </ModalBody>
                    </Modal>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ isAdminPanel, user, token }) => ({ isAdminPanel, user, token });
export default connect(mapStateToProps)(translate(AttendanceHistoryAll));
