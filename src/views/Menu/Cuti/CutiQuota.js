import React, { useEffect, useState } from 'react';
import{translate, t} from 'react-switch-lang';
import request from "../../../utils/request";
import {Table, Col, Row, Input, Button, InputGroup, InputGroupAddon, InputGroupText, Spinner} from 'reactstrap';
import LoadingAnimation from '../../../components/LoadingAnimation';
import DataNotFound from '../../../components/DataNotFound';
import { toast } from 'react-toastify';


function CutiQuota(){

    const [notFound, setNotFound] = useState(false);
    const [loading, setLoading] = useState(true);
    const [personnel, setPersonnel] = useState([]);
    const [search, setSearch] = useState(null);
    const [data, setData] = useState({});
    const [saveLoading, setSaveLoading] = useState(false);


    useEffect(() => {
        request.get('v1/personnel/holidays/balances')
        .then(res =>{
            setPersonnel(res.data.data);
            const tmpData = {}
            res.data.data.forEach(person => {
                tmpData[person.personnelId] = person.personnelHolidayBalance;
            })
            setData(tmpData)
        })
        .catch(err => {
            if (err.response.status === 404) {
                console.log(err)
                setNotFound(true);
            }
        }).finally(() => setLoading(false))
    },[])

    const handleSearch = (e) => {
        setSearch(e.target.value)
    }

    const saveQuota = () => {
        setSaveLoading(true);
        request.post('v1/personnel/holidays/balances', {
            data:data
        })
        .then(res =>{
            console.log(res)
            toast.success('Berhasil Edit Jatah Cuti');
        })
        .catch(err => {
            toast.error('Gagal mengubah kuota cuti')
            return
        }).finally(()=> setSaveLoading(false))
    }

    if (loading) {
        return <LoadingAnimation />;
    }
    else if (notFound) {
        return <DataNotFound />
    }

    return(
        <div className="animated fadeIn" >
                <div className="content">
                    <Row className="md-company-header mb-3">
                        <Col xs="12" md="6" className="my-1">
                            <InputGroup className="w-75">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText className="input-group-transparent">
                                        <i className="fa fa-search"></i>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" placeholder={t('cari') + ' ' + t('nama')} className="input-search"
                                onChange={handleSearch}
                                />
                            </InputGroup>
                        </Col>
                        {/* <Col xs="12" md="4" className="my-1">
                            <Button type="submit" color="netis-color" className="w-25"
                                onClick={this.filterData}
                            > Filter
                                    {this.state.loading ? <Spinner color="light" size="sm" /> : 'Filter'}
                            </Button>
                        </Col>                        */}
                        <Col xs="12" md="6" className="my-1 text-right">
                            <Button color="success" onClick={saveQuota} disabled={saveLoading}>
                                {saveLoading ? <><Spinner color="light" size="sm" />&nbsp;Loading</> : t('simpan')}
                                {/* {t('simpan')} */}
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" lg="12">
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th className="text-left w-50">{t('nama')}</th>
                                        <th className="text-left">Unit</th>
                                        <th className="text-center">{t('jatahcuti')}</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {personnel.filter((name) => (
                                        search ? name.userFullName.toLowerCase().includes(search.toLowerCase()) :  true
                                    ))
                                    .map((name, idx) => (
                                    <tr key={idx}>
                                        <td className="text-left">{name.userFullName}</td>
                                        <td className="text-left">{name.unitName}</td>
                                        <td className="text-center">
                                            <Input type="number" min="0" max="99" name="quota" id="quota" onChange={(e) => setData({...data, [name.personnelId]: parseInt(e.target.value) })} value={data[name.personnelId]} />
                                        </td>
                                    </tr>
                                    ))}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </div>
            </div>
    )
}

export default translate(CutiQuota);