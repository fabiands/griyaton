import React from "react";
import { useEffect, useState } from "react";
import { Table, Button, Modal, ModalBody, Spinner } from "reactstrap";
import { Link } from "react-router-dom";
import request from "../../../utils/request";
import { translate, t } from "react-switch-lang";
import { toast } from "react-toastify";
import LoadingAnimation from "../../../components/LoadingAnimation";
import { useUserPrivileges } from "../../../store";
import * as moment from "moment";

function RecruitmentMenu(props) {
  const [loading, setLoading] = useState(true);
  const [lowongan, setLowongan] = useState([]);
  const [deletingId, setDeletingId] = useState(null);
  const [updateId, setUpdateId] = useState([]);
  const [deleteLoading, setDeleteLoading] = useState(false);
  const { privileges } = useUserPrivileges();
  const [updateLoading, setUpdateLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    getJob();
  }, []);

  function getJob() {
    request
      .get("v1/recruitment/vacancies")
      .then((res) => {
        setLowongan(res.data.data);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  function doDelete() {
    if (deleteLoading) return;
    setDeleteLoading(true);
    request
      .delete("v1/recruitment/vacancies/" + deletingId)
      .then(() => {
        setLowongan(lowongan.filter((data) => data.id !== deletingId));
        setDeletingId(null);
        toast.success(t("berhasilhapus"));
      })
      .catch((err) => {
        toast.error(t("gagalhapus"));
        throw err;
      })
      .finally(() => {
        setDeleteLoading(false);
      });
  }

  function doUpdate() {
    if (deleteLoading) return;
    setUpdateLoading(true);
    if (updateId[1] === "repost") {
      request
        .put("v1/recruitment/vacancies/renew/" + updateId[0])
        .then(() => {
          getJob();
          setUpdateId([]);
          if (updateId[0]) {
            toast.success(t("berhasilrepost"));
          }
        })
        .catch((err) => {
          toast.error("Terjadi kesalahan");
          throw err;
        })
        .finally(() => {
          setUpdateLoading(false);
        });
    } else if (updateId[1] === "tutup") {
      request
        .put("v1/recruitment/vacancies/" + updateId[0], {
          published: false,
        })
        .then(() => {
          getJob();
          setUpdateId([]);
          if (updateId[0]) {
            toast.success(t("berhasiltutup"));
          }
        })
        .catch((err) => {
          toast.error("Terjadi kesalahan");
          throw err;
        })
        .finally(() => {
          setUpdateLoading(false);
        });
    } else if (updateId[1] === "publish") {
      request
        .put("v1/recruitment/vacancies/" + updateId[0], {
          published: true,
        })
        .then(() => {
          getJob();
          setUpdateId([]);
          if (updateId[0]) {
            toast.success(t("berhasilpublish"));
          }
        })
        .catch((err) => {
          toast.error("Terjadi kesalahan");
          throw err;
        })
        .finally(() => {
          setUpdateLoading(false);
        });
    } else {
      toast.error("Terjadi kesalahan");
    }
  }

  if (loading) {
    return (
      <div className="animated fadeIn">
        <LoadingAnimation />
      </div>
    );
  }

  return (
    <div className="animated fadeIn">
      <div className="d-flex bd-highlight mb-3">
        {privileges.includes("add-job") && (
          <div className="p-2 bd-highlight">
            <Link to={"/recruitment/vacancies/create"}>
              <Button color="netis-color">
                <i className="fa fa-plus mr-2"></i>
                {t("buatlowonganbaru")}
              </Button>
            </Link>
          </div>
        )}

        <div className="ml-auto p-2 bd-highlight">{/* Search */}</div>
      </div>

      <Table responsive>
        <thead>
          <tr>
            <th className="column-header text-left w-50">{t("posisi")}</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {lowongan.map((data) => (
            <tr key={data.id}>
              <td>
                {data.expiredAt !== null && data.published ? (
                  <div>
                    <h5 style={{ display: "inline" }}>
                      {data.name.toUpperCase()}
                    </h5>
                    {moment().format("YYYY-MM-DD") ===
                    moment(data.expiredAt)
                      .add("+1", "d")
                      .format("YYYY-MM-DD") ? (
                      <small
                        className="text-danger"
                        style={{ display: "inline" }}
                      >
                        {" "}
                        Expired
                      </small>
                    ) : (
                      <small
                        className="text-secondary"
                        style={{ display: "inline" }}
                      >
                        {" "}
                        Berlaku hingga{" "}
                        {moment(data.expiredAt)?.format("DD MMM YYYY")}
                      </small>
                    )}
                  </div>
                ) : (
                  <div>
                    <h5 style={{ display: "inline" }}>
                      {data.name.toUpperCase()}
                    </h5>
                    {data.expiredAt ? (
                      <small
                        className="text-warning"
                        style={{ display: "inline" }}
                      >
                        {" "}
                        Closed
                      </small>
                    ) : (
                      <small
                        className="text-secondary"
                        style={{ display: "inline" }}
                      >
                        {" "}
                        Draft
                      </small>
                    )}
                  </div>
                )}
                <br />
                <a href={data.skillanaURL} target="_blank" rel="noopener noreferrer">
                  <Button
                    style={{ border: 0 }}
                    className="btn bg-transparent btn-sm mr-1 mt-1"
                  >
                    <i className="fa fa-eye"></i> Lihat
                  </Button>
                </a>
                <Link to={`/recruitment/vacancies/${data.id}/edit`}>
                  <Button
                    style={{ border: 0 }}
                    className="btn bg-transparent btn-sm mr-1 mt-1"
                  >
                    <i className="fa fa-pencil"></i> Edit
                  </Button>
                </Link>
                <Button
                  style={{ border: 0 }}
                  className="btn bg-transparent btn-sm mx-1 mt-1"
                  onClick={() => setDeletingId(data.id)}
                >
                  <i className="fa fa-trash-o"></i> {t("hapus")}
                </Button>
                {data.expiredAt !== null && data.published ? (
                  <div style={{ display: "inline" }}>
                    {moment().format("YYYY-MM-DD") ===
                    moment(data.expiredAt)
                      .add("+1", "d")
                      .format("YYYY-MM-DD") ? (
                      <Button
                        style={{ border: 0 }}
                        className="btn bg-transparent btn-sm mx-1 mt-1"
                        onClick={() => setUpdateId([data.id, "repost"])}
                      >
                        <p className="text-info" style={{ display: "inline" }}>
                          <i className="fa fa-repeat"></i> Repost
                        </p>
                      </Button>
                    ) : (
                      <Button
                        style={{ border: 0 }}
                        className="btn bg-transparent btn-sm mx-1 mt-1"
                        onClick={() => setUpdateId([data.id, "tutup"])}
                      >
                        <p
                          className="text-danger"
                          style={{ display: "inline" }}
                        >
                          <i className="fa fa-pause"></i> Tutup
                        </p>
                      </Button>
                    )}
                  </div>
                ) : (
                  <div style={{ display: "inline" }}>
                    <Button
                      style={{ border: 0 }}
                      className="btn bg-transparent btn-sm mx-1 mt-1"
                      onClick={() => setUpdateId([data.id, "publish"])}
                    >
                      <p className="text-success" style={{ display: "inline" }}>
                        <i className="fa fa-play"></i>{" "}
                        {data.expiredAt ? "Reopen" : "Publish"}
                      </p>
                    </Button>
                  </div>
                )}
              </td>
              <td className="align-middle text-right text-nowrap">
                <Link
                  to={`/recruitment/vacancies/${data.id}/applicants?status=unprocessed`}
                  // state={data}
                  className="btn text-wrap btn-outline-dark mr-2 text-capitalize"
                >
                  <h5 className="mb-0">{data.unprocessedApplicantCount}</h5>
                  {t("belumproses")}
                </Link>
                <Link
                  to={`/recruitment/vacancies/${data.id}/applicants?status=accepted`}
                  className="btn text-wrap btn-outline-success mr-2 text-capitalize"
                >
                  <h5 className="mb-0">{data.acceptedApplicantCount}</h5>
                  {t("sesuai")}
                </Link>
                <Link
                  to={`/recruitment/vacancies/${data.id}/applicants?status=rejected`}
                  className="btn text-wrap btn-outline-danger mr-2 text-capitalize"
                >
                  <h5 className="mb-0">{data.rejectedApplicantCount}</h5>
                  {t("tidaksesuai")}
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <Modal
        isOpen={!!deletingId}
        toggle={() => {
          if (!deleteLoading) {
            setDeletingId(null);
          }
        }}
      >
        <ModalBody>
          <h6>{t("yakinmenghapus")}</h6>
          <div className="d-flex justify-content-end">
            {!deleteLoading && (
              <Button
                className="mr-2"
                color="light"
                onClick={() => setDeletingId(null)}
              >
                {t("batal")}
              </Button>
            )}
            <Button
              color="netis-primary"
              onClick={doDelete}
              disabled={deleteLoading}
            >
              {deleteLoading ? (
                <React.Fragment>
                  <Spinner size="sm" color="light" /> menghapus...
                </React.Fragment>
              ) : (
                t("hapus")
              )}
            </Button>
          </div>
        </ModalBody>
      </Modal>

      <Modal
        isOpen={updateId[0] ? true : false}
        toggle={() => {
          if (!updateLoading) {
            setUpdateId([]);
          }
        }}
      >
        <ModalBody>
          <h6>{t("yakinmerubah")}</h6>
          <div className="d-flex justify-content-end">
            {!updateLoading && (
              <Button
                className="mr-2"
                color="light"
                onClick={() => setUpdateId([])}
              >
                {t("batal")}
              </Button>
            )}
            <Button
              color="netis-primary"
              onClick={doUpdate}
              disabled={updateLoading}
            >
              {updateLoading ? (
                <React.Fragment>
                  <Spinner size="sm" color="light" /> Merubah...
                </React.Fragment>
              ) : (
                t("ubah")
              )}
            </Button>
          </div>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default translate(RecruitmentMenu);
