import React,{useState, useEffect} from 'react';
import {Label, Button} from 'reactstrap';
import {translate, t} from 'react-switch-lang';
import Select from 'react-select';
import {useFormik} from 'formik';
import {useHistory} from 'react-router-dom';
import CaptureLoading from './components/CaptureLoading'
import request from '../../../utils/request';

function RegisterName(){
    const history = useHistory();
    const [userName, setUserName] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const {values, touched, isSubmitting, ...formik} = useFormik({
        initialValues: {
            user:''
        },
        onSubmit: (values, {setSubmitting}) => {
            setSubmitting(true);
            console.log(values);
            history.push({
                pathname: '/tab/registerface',
                state: { registerData: values.user}
            });
            setSubmitting(false);
        }
    });

    useEffect(() => {
        request.get('/external/personnels')
            .then(response => {
                setIsLoading(false);
                console.log(response.data.data)
                setUserName(response.data.data)
            })
            .catch(error => {
                console.log('Error:', error);
            });
    }, [])

    if (isLoading) {
        return <CaptureLoading title="Loading" visible={true} />;
    }

    const name = userName.map(name =>
        ({value: name.id, label: name.fullName, face: name.hasFace})
    )

    const changeUser = function(user){
        formik.setFieldValue('user', user);
        formik.setFieldTouched('user', true);
    }

    return(
        <div className="app flex-row align-items-center background-login background-tablet2">
            <Button onClick={() => history.push('/tab')} className="back-tablet p-2">
                    <i className="fa fa-home fa-4x" />
            </Button>
            <div className="w-100">
                <div className="h-100">
                    <div className="text-center mx-auto welcome-logo mt-0 mb-5" style={{width:"40%", maxHeight:"100px"}} />                                             
                    <div className="text-center p-5 align-middle">
                        <div className="w-75 text-center my-4 mx-auto">
                        <Label htmlFor="user" className="input-label">
                            <h3>{t('Pilih Nama Anda')}</h3>
                        </Label>
                            <Select
                                value={values.user}
                                isSearchable={true}
                                isClearable
                                name="user"
                                id="user"
                                onChange={changeUser}
                                onBlur={formik.handleBlur}
                                options={name}
                                defaultValue={values.user}
                                className="needs-validation"
                                required
                            />
                        </div>
                        <div className="text-center my-4 mx-auto">
                            <Button
                                className="w-50 shadow"
                                style={{
                                    backgroundColor:"#b7eb8f",
                                    borderRadius:"15px",
                                    // height:"40px",
                                    fontSize:"14pt",
                                    color:"#696969"
                                }}
                                disabled={values.user ? false : true}
                                onClick={formik.handleSubmit}
                            >
                            Selanjutnya
                            </Button>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    )
}

export default translate(RegisterName);
