import React, { Component, Fragment } from 'react';
import { Card, CardBody, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

class Widget01 extends Component {

    render() {
        return (
            <Card className="dashboard-card">
                <CardBody>
                    <div className="h4 m-0">{this.props.header}</div>
                    <div>{this.props.mainText}</div>
                    {/* <Progress className={'progress.style'} color={progress.color} value={progress.value} /> */}
                    {/* <small className="text-muted">{smallText}</small> */}
                    <div className="widget01-children">{this.props.children}</div>
                    {this.props.button &&
                        <Fragment>
                            <hr />
                            <div className="d-flex justify-content-end mt-2">
                                <Link to={`${this.props.to}`}><Button color="netis-color">{this.props.buttonText}</Button></Link>
                            </div>
                        </Fragment>
                    }
                </CardBody>
            </Card>
        );
    }
}


Widget01.defaultProps = {
    smallText: 'Excepteur sint occaecat...',
    button: false,
    buttonText: 'Button'
}

export default Widget01;
