import React from 'react'
import { Row, Button, Modal, ModalBody } from 'reactstrap';

export default function ConfirmationModal(props) {
    return (

        <Modal isOpen={props.visible} className="top-50" autoFocus={true}>
            <ModalBody>
                <Row>
                    <div className="col-12 text-center">
                        <h2 className="title-upgrade" style={{ color: '#93aad6' }}>{props.title}</h2>
                    </div>
                    <div className="col-12 text-center">
                        <p>{props.text}</p>
                    </div>
                    <div className="col-12 text-center">

                        <Row>
                            <div className="col-6 text-right">
                                <Button type="submit" color="netis-danger" onClick={props.onPressLeft}>
                                    {props.titleLeft}
                                </Button>
                            </div>
                            <div className="col-6 text-left">
                                <Button type="submit" color="netis-color" onClick={props.onPressRight} >
                                    {props.titleRight}
                                </Button>
                            </div>
                        </Row>


                    </div>
                </Row>
            </ModalBody>
        </Modal>

    )
}