import React, { Component, Suspense } from 'react';
import * as router from 'react-router-dom';

import {
    // AppAside,
    // AppFooter,
    // AppHeader,
    AppSidebar,
    AppSidebarFooter,
    AppSidebarForm,
    AppSidebarHeader,
    // AppSidebarMinimizer,
    // AppBreadcrumb2 as AppBreadcrumb,
    AppSidebarNav2 as AppSidebarNav,
    AppHeader,
} from '@coreui/react';
import {
    translate,
} from 'react-switch-lang';
import DefaultHeader from './DefaultHeader';
import { Container } from 'reactstrap';
import adminMenus from './admin-menu';
import userMenus from './user-menu';
import LoadingAnimation from '../../views/Menu/Personnel/component/atom/LoadingAnimation';
import AuthRoute from '../../components/AuthRoute';
import { connect } from 'react-redux';

class DefaultLayout extends Component {

    generateMenus(menu) {
        const { privileges: userPrivileges = [] } = this.props.user;
        // console.log(userPrivileges);
        const checkPrivileges = (routeObj) => {
            if (routeObj.privileges) {
                return routeObj.privileges.every(p => userPrivileges.includes(p));
            }

            if (routeObj.oneOfPrivileges) {
                return routeObj.oneOfPrivileges.some(p => userPrivileges.includes(p));
            }

            return true;
        }

        if (
            // userPrivileges.indexOf('access admin panel') >= 0 &&
            menu === '2'
        ) {
            return adminMenus()
                .filter(routeObj => !!routeObj.menu)
                .filter(checkPrivileges)
                .map(({ menu, url }) => {
                    return { ...menu, url }
                });
        } else {
            return userMenus()
                .filter(routeObj => !!routeObj.menu)
                .filter(checkPrivileges)
                .map(({ menu, url }) => ({ ...menu, url }))
        }
    }

    render() {
        const menu = this.props.panelMenu;
        const { Switch, Redirect } = router;
        return (
            <div className="app">
                <AppHeader fixed><DefaultHeader /></AppHeader>
                <div className="app-body">
                    <AppSidebar fixed display="lg">
                        <AppSidebarHeader />
                        <AppSidebarForm />
                        <Suspense>
                            <AppSidebarNav navConfig={{ items: this.generateMenus(menu) }} router={router} />
                        </Suspense>
                        <AppSidebarFooter />
                    </AppSidebar>

                    <main className="main">
                        <Container fluid>
                            <Suspense fallback={<LoadingAnimation />}>
                                <Switch>
                                    <Redirect exact from="/home" to="/dashboard" />
                                    {this.generateRoutes(menu)}
                                </Switch>
                            </Suspense>
                        </Container>
                    </main>
                </div>
            </div>
        );
    }

    generateRoutes(menu) {
        if (
            // privileges.indexOf('access admin panel') >= 0 &&
            menu === '2') {
            return adminMenus()
                .map((props, idx) =>
                    <AuthRoute key={idx} path={props.url} exact={!!props.exact} component={props.component} {...props} />
                );
        } else {
            return userMenus()
                .map((props, idx) =>
                    !!props.redirect ?
                        <router.Redirect from={props.url} to={props.redirect} exact={!!props.exact} />
                        :
                        <AuthRoute key={idx} path={props.url} exact={!!props.exact} component={props.component} {...props} />
                );
        }
    }
}

const mapStateToProps = (reduxState) => ({ user: reduxState.user, panelMenu: reduxState.menu })

export default connect(mapStateToProps)(translate(DefaultLayout));
