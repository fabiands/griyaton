// import 'react-app-polyfill/ie9'; // For IE 9-11 support
// import 'react-app-polyfill/stable';
// // import 'react-app-polyfill/ie11'; // For IE 11 support
// import './polyfill'
// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import * as serviceWorker from './serviceWorker';
// import { createStore } from 'redux';
// import { Provider } from 'react-redux';
// import { appMiddleware } from "./middlewares/app";
// import { apiMiddleware } from "./middlewares/core";
// // const persistedState = loadState();

// // const globalState = {
// //     sessionId: null
// // }
// // //Reducer 
// // const rootReducer = (state = globalState, action) => {
// //     if (action.type === 'SET_SESSION') {
// //         return {
// //             sessionId: action.value
// //         }
// //     }
// //     return state;

// // }

// // //Store
// // const store = createStore(rootReducer, persistedState);
// // store.subscribe(() => {
// //     saveState({
// //         'sessionId': store.getState().sessionId
// //     });
// // })

// ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import { BrowserRouter as Router } from "react-router-dom";

import * as Sentry from '@sentry/browser'
if (process.env.NODE_ENV === 'production') {
    Sentry.init({ dsn: "https://1b5a270617da4c35931db44f2c6723ba@o404034.ingest.sentry.io/5267898" });
}

const rootElement = document.getElementById("root");
ReactDOM.render(<Router><App /></Router>, rootElement);
