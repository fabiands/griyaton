import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import AppStore from '../../assets/assets_ari/appstore.svg';
import GooglePlay from '../../assets/assets_ari/gplay.svg';

class About extends Component {
    render() {
        return (
            <section className="about" id="about">
                <div className="container py-5">
                    <h1 className="text-center">Apa Itu Widya Skilloka ?</h1>
                    <hr className="hr-main" />
                    <div className="row justify-content-center">
                        <div className="col-md-10" style={{ lineHeight: 1.7 }}>
                            <p><span className="text-netis-primary">Widya Skilloka</span> adalah <strong>perusahaan teknologi informasi</strong> yang mengembangkan layanan masa depan bagi permasalahan <strong>Sistem Manajemen SDM</strong>.</p>
                            <p>Widya Skilloka hadir sebagai HR Business Partner untuk mempersingkat proses manajemen SDM seperti manajemen data karyawan, rekruitmen, presensi, memantau lokasi kerja karyawan, pengajuan reimburse, pengajuan cuti serta penilaian kerja bagi karyawan dalam satu platform yang didukung oleh teknologi Face Recognition.
                            </p>
                        </div>
                    </div>
                    <div className="d-flex justify-content-center my-4">
                        <Link to="#" className="unduh-app mr-md-5">
                            <img src={AppStore} alt="appstore" className="img-fluid" />
                        </Link>
                        <a href='https://play.google.com/store/apps/details?id=com.widyaskilloka.hris' target="blank"
                            className="unduh-app">
                            <img src={GooglePlay} alt="googleplay" className="img-fluid" />
                        </a>
                    </div>
                </div>
            </section>
        )
    }
}

export default About;
