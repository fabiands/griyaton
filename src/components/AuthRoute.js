import React, { useMemo } from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router";
import { Spinner } from "reactstrap";
import { getMe } from "../actions/auth";

const Forbidden = () => <div><center><h1>403 Sorry, this page is forbidden.</h1></center></div>;

const AuthRoute = ({isLoggedIn, user, token, type, privileges, oneOfPrivileges, getMe, ...props}) => {
    const shouldAuthenticate = useMemo(() => ['guest', 'private'].includes(type) || privileges !== undefined || oneOfPrivileges !== undefined, [oneOfPrivileges, privileges, type]);

    if (shouldAuthenticate) {

        if (user === null && token) {
            getMe();
            return <div style={{ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, background: 'rgba(255,255,255, 0.5)', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Spinner style={{width: 48, height: 48 }} />
            </div>
        }

        if (type === "guest" && isLoggedIn === true) {
            return <Redirect to="/home" />;
        }
        if (type === "private" && isLoggedIn === false) {
            return <Redirect to="/login" />;
        }

        const userPrivileges = user?.privileges || [];

        if (privileges !== undefined) {
            return privileges.every(p => userPrivileges.includes(p)) ? <Route {...props} /> : <Forbidden/>;
        }

        if (oneOfPrivileges !== undefined) {
            return oneOfPrivileges.some(p => userPrivileges.includes(p)) ? <Route {...props} /> : <Forbidden/>;
        }
    }

    return <Route {...props} />;
};

const mapStateToProps = ({ user, token }) => ({
    isLoggedIn: user != null, user, token
});

export default connect(mapStateToProps, { getMe })(AuthRoute);
