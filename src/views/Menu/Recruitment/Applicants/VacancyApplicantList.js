import React, { useState, useEffect } from 'react';
import { Button, Card, CardHeader, Badge, CardBody, Col, Row } from 'reactstrap';
import request from '../../../../utils/request';
import LoadingAnimation from '../../../../components/LoadingAnimation';
import DataNotFound from '../../../../components/DataNotFound';
import { Link, useLocation, useRouteMatch } from 'react-router-dom';
import { t, translate } from 'react-switch-lang';
import { convertToRupiah } from '../../../../utils/formatter';
import { toast } from 'react-toastify';
import { useUserPrivileges } from '../../../../store';

function VacancyApplicantList(props) {
    const { search, state: locationState = {} } = useLocation();
    const { params } = useRouteMatch();
    const { data: dataVacancy } = locationState;
    const { can } = useUserPrivileges()

    const [vacancy, setVacancy] = useState(dataVacancy);

    const [applicants, setApplicants] = useState([]);
    const [dataNotFound, setDataNotFound] = useState(false);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        const queryParams = new URLSearchParams(search);
        setLoading(true);
        setDataNotFound(false);
        request.get('v1/recruitment/applicants', { params: { job_vacancy: params.id, status: queryParams.get('status') } }).then(res => {
            setApplicants(res.data.data);
        }).catch(err => {
            if (err.response.status === 404) {
                setDataNotFound(true);
            }
        }).finally(() => setLoading(false));
    }, [params.id, search])

    useEffect(() => {
        if (!dataVacancy) {
            request.get(`v1/recruitment/vacancies/${params.id}`).then(res => {
                setVacancy(res.data.data);
            });
        }
    }, [dataVacancy, params.id])

    const UbahStatus = (applicantId, stat) => {
        request.put(`/v1/recruitment/applicants/${applicantId}`, { status: stat })
            .then(res => {
                toast.success(t('statuspelamarberhasildiubah'));
                setApplicants(applicants.filter(applicant => applicant.id !== applicantId));
            })
            .catch(err => {
                toast.error(t('statuspelamargagaldiubah'));
                throw err;
            })
    }

    const queryParams = new URLSearchParams(search);
    const status = queryParams.get('status');

    return (
        <div className="animated fadeIn">
            <div className="bd-highlight mb-4">
                <Button onClick={props.history.goBack} color="netis-color">
                    <i className="fa fa-chevron-left mr-1"></i>
                    {t('kembali')}
                </Button>
            </div>

            <Card>
                <CardHeader className="d-flex align-items-center">
                    <h5 className="mb-0">{vacancy?.name}</h5>
                    <div className="ml-auto mr-n1">
                        <Link className={`btn mx-1 ${status === 'unprocessed' || status === null ? 'btn-dark' : 'btn-link text-dark'}`} replace={true} to={location => ({ ...location, search: '?status=unprocessed', state: { data: vacancy } })} >
                            {status === 'unprocessed' || status === null ?
                                <><Badge pill color="light" className="mr-1" style={{ top: -2 }}>{vacancy?.unprocessedApplicantCount}</Badge> Belum diproses</>
                                :
                                <><Badge pill color="dark" className="mr-1" style={{ top: -2 }}>{vacancy?.unprocessedApplicantCount}</Badge> Belum diproses</>
                            }
                        </Link>

                        <Link className={`btn mx-1 ${status === 'accepted' ? 'btn-success' : 'btn-link text-success'}`} replace={true} to={location => ({ ...location, search: '?status=accepted', state: { data: vacancy } })} >
                            {status === 'accepted' ?
                                <><Badge pill color="light" className="mr-1" style={{ top: -2 }}>{vacancy?.acceptedApplicantCount}</Badge> Sesuai</>
                                :
                                <><Badge pill color="success" className="mr-1" style={{ top: -2 }}>{vacancy?.acceptedApplicantCount}</Badge> Sesuai</>
                            }
                        </Link>

                        <Link className={`btn mx-1 ${status === 'rejected' ? 'btn-danger' : 'btn-link text-danger'}`} replace={true} to={location => ({ ...location, search: '?status=rejected', state: { data: vacancy } })} >
                            {status === 'rejected' ?
                                <><Badge pill color="light" className="mr-1" style={{ top: -2 }}>{vacancy?.rejectedApplicantCount}</Badge> Tidak Sesuai</>
                                :
                                <><Badge pill color="danger" className="mr-1" style={{ top: -2 }}>{vacancy?.rejectedApplicantCount}</Badge> Tidak Sesuai</>
                            }
                        </Link>
                    </div>
                </CardHeader>
            </Card>

            <Row>
                {
                    loading ?
                        <Col xs={12} className="py-5 text-center">
                            <LoadingAnimation />
                        </Col>
                        :
                        dataNotFound ?
                            <Col xs={12} className="py-5 text-center">
                                <DataNotFound />
                            </Col>
                            :
                            applicants.map((applicant, idx) => (
                                // applicants.map(applicant => (
                                <Col sm="6" key={idx}>
                                    <Card>
                                        <CardBody>
                                            <Link to={`/recruitment/applicants/${applicant.id}`} className={`${can('read-job-applicant') ? '' : ' disabled'}`}>
                                                <h4>{applicant.detail?.fullName ?? applicant.userEmail}</h4>
                                            </Link>

                                            <Row className="mt-3">
                                                <Col xs="12">
                                                    <i className="fa fa-graduation-cap mr-2" style={{ width: 20 }}></i>
                                                    {applicant.detail?.lastEducation ?? (<i> - </i>)} {applicant.detail?.major && `(${applicant.detail.major})`}
                                                </Col>
                                                <Col xs="12">
                                                    <i className="fa fa-money mr-2 " style={{ width: 20 }}></i>
                                                    {(applicant.detail?.expectedSalary && convertToRupiah(applicant.detail.expectedSalary)) || ' - '}
                                                </Col>
                                                <Col xs="12">
                                                    {applicant.status === 'pending' && (
                                                        <div className="d-inline-block text-right pt-4 w-100">
                                                            <Button
                                                                color={`link text-success${can('verify-job-applicant') ? '' : ' d-none'}`}
                                                                // style={{ flex: '1 0' }}
                                                                // className="flex-shrink-0"
                                                                onClick={() => { UbahStatus(applicant.id, 'accepted'); }}>
                                                                <i className="fa fa-check mr-1"></i> {t('sesuai')}
                                                            </Button>
                                                            <Button
                                                                color={`link text-danger${can('verify-job-applicant') ? '' : ' d-none'}`}
                                                                className=""
                                                                onClick={() => { UbahStatus(applicant.id, 'rejected'); }}>
                                                                <i className="fa fa-times mr-1"></i> {t('Tidak sesuai')}
                                                            </Button>
                                                        </div>
                                                    )}
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>
                            ))
                }
            </Row>
        </div>
    )
}

export default translate(VacancyApplicantList);
