import React from 'react'
import { Row, Button, Modal, ModalBody } from 'reactstrap';

export default function Notification(props) {
    return (

        <Modal isOpen={props.visible} size="xl" className="top-100" autoFocus={true}>
            <ModalBody>
                <p style={{fontSize:"30pt"}}><b>{props.title}</b></p>
                <p style={{fontSize:"26pt"}}>{props.desc}</p>
                <Row>
                    <div className="col-12 d-flex justify-content-end">
                        <Button type="submit" color="netis-color" style={{ width: '30%',fontSize:"24pt" }} onClick={props.onClick}>
                            {props.actionTitle}
                        </Button>
                    </div>
                </Row>
            </ModalBody>
        </Modal>

    )
}