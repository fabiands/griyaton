import React, {useEffect, useState} from 'react';
import {Card, Label, Button, Table, Modal, Spinner} from 'reactstrap';
import {useLocation, useHistory, Redirect} from "react-router-dom";
import {translate, t} from 'react-switch-lang';
import Select from 'react-select';
import Color from '../../../utils/Colors';
import {useFormik} from 'formik';
import CaptureLoading from './components/CaptureLoading'
import AfirmationModal from './components/AfirmationModal'
import request from '../../../utils/request';

function InputPin(){
    const location = useLocation();
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false);
    const [firstLoad, setFirstLoad] = useState(true);
    const [isSuccess, setIsSuccess] = useState(false);
    const [userName, setUserName] = useState([]);
    const [wrongPin, setWrongPin] = useState(false);
    const [IsErrorCheckIn, setIsErrorCheckIn] = useState(false);
    const data = location.state?.checkInData;
    const userData = location.state?.userData;
    const type = location.state?.type;

    const {values, touched, errors, isSubmitting, ...formik} = useFormik({
        initialValues: {
            user:{
                value:userData.personnelId,
                label:userData.userFullName
            },
            pin:''
        },
        onSubmit: (values, {setSubmitting, setErrors}) => {
            setIsLoading(true);
            setSubmitting(true);
            const formData = new FormData();
            formData.append('latitude', data.latitude);
            formData.append('longitude', data.longitude);
            formData.append('faceImage', data.faceImage, 'faceCheckIn.jpg');
            formData.append('personnelId', values.user.value)
            formData.append('pin', values.pin)
            formData.append('type', type);


            request.post('external/attendance', formData)
                .then(({ status: res, data }) => {
                    if(res === 200){
                        setIsSuccess(true)
                    }
                    else if (res === 422){
                        formik.setFieldValue('pin', "")
                        // setIsLoading(false)
                        setWrongPin(true)
                    }
                    else{
                        formik.setFieldValue('pin', "")
                        // setIsLoading(false)
                        window.alert("Terjadi Kesalahan, silahkan coba lagi")
                    }
                })
                .catch(err => {
                    setErrors(err.response?.data.errors);
                    setIsErrorCheckIn(true);
                    Promise.reject(err);
                    return;
                })
                .finally(() => {
                    setIsLoading(false)
                    setSubmitting(false)
                });
        }
    });

    useEffect(() => {
        request.get('external/personnels')
            .then(response => {
                console.log(response.data.data)
                setUserName(response.data.data)
                setFirstLoad(false);
            })
            .catch(error => {
                console.log('Error:', error);
            });
    }, [])

    useEffect(() => {
        if (values.pin.length === 6){
            formik.handleSubmit();
        }
    // eslint-disable-next-line
    }, [values.pin])

    if (firstLoad) {
        return <CaptureLoading title="Loading..." visible={true} />;
    }

    if(!location.state){
        return (<Redirect to="/tab"></Redirect>);
    }

    const name = userName.map(name =>
        ({value: name.id, label: name.fullName})
    )

    if (isSuccess) {
        return (
            <AfirmationModal
                visible={true}
                title="Success"
                text={`${t('Berhasil melakukan Presensi Masuk ')}`}
                onPressButton={() => {
                    setIsSuccess(false)
                    history.push('/tab');
                }}
                titleButton="OK"
                titleColor={Color.white}
                bgColorButton={Color.primaryColor}
            />
        );
    }

    if (IsErrorCheckIn) {
        return (
            <AfirmationModal
                animationType="none"
                visible={true}
                title="Failed"
                text={t('Gagal melakukan Presensi, silahkan coba kembali')}
                onPressButton={() => {
                    setIsErrorCheckIn(false)
                }}
                titleButton="Kembali"
                titleColor={Color.white}
                bgColorButton={Color.primaryColor}
            />
        );
    }

    if (wrongPin) {
        return (
            <AfirmationModal
                animationType="none"
                visible={true}
                color="#FF2D00"
                title="PIN Salah"
                text={t('PIN yang anda masukkan tidak sesuai. Silahkan masukkan PIN sekali lagi')}
                onPressButton={() => {
                    setWrongPin(false)
                }}
                titleButton="Kembali"
                titleColor={Color.white}
                bgColorButton={Color.primaryColor}
            />
        );
    }

    const changeUser = function(user){
        formik.setFieldValue('user', user);
        formik.setFieldTouched('user', true);
    }

    const resetPin = function(){
        formik.setFieldValue('pin', "");
    }

    function changePinValue(value){
        formik.setFieldValue('pin', "" + values.pin + value)
    }

    return(
        <div className="app flex-row align-items-center background-login background-tablet2">
            <Button onClick={() => history.push('/tab')} className="back-tablet p-2">
                    <i className="fa fa-home fa-4x" />
            </Button>
            <div className="w-100">
                <div className="h-100">
                    <div className="text-center mx-auto welcome-logo mt-0 mb-5" style={{width:"40%", maxHeight:"100px"}} />
                    <div className="text-center mt-2">
                        <div className="w-75 text-center mb-5 mx-auto">
                        <Label htmlFor="user" className="input-label">
                            <h3>{t('Pilih Nama Anda')}</h3>
                        </Label>
                        <Select
                            value={name.find(n => n.value === values.user.value)}
                            isSearchable={true}
                            name="user"
                            id="user"
                            onChange={changeUser}
                            onBlur={formik.handleBlur}
                            options={name}
                            defaultValue={values.user}
                            className="needs-validation"
                            required
                        />
                        </div>
                        <Label htmlFor="user" className="input-label mt-3">
                            <h3>{t('Masukkan PIN Anda')}</h3>
                        </Label>
                        <div className="d-flex justify-content-center text-center mb-5 mx-auto">
                            <Card className="pin-card align-middle mx-2">{values.pin[0] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[1] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[2] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[3] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[4] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[5] ? <i className="fa fa-circle" /> : null}</Card>
                        </div>
                        <Table borderless style={{backgroundColor:"#C0C0C0", bottom:0, marginBottom:0}} className="mt-5">
                            <tbody>
                                <tr>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={() => changePinValue(1)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                            <b>1</b>
                                        </Button>
                                    </td>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={() => changePinValue(2)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                            <b>2</b>
                                        </Button>
                                    </td>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={() => changePinValue(3)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                            <b>3</b>
                                        </Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={() => changePinValue(4)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                            <b>4</b>
                                        </Button>
                                    </td>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={() => changePinValue(5)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                            <b>5</b>
                                        </Button>
                                    </td>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={() => changePinValue(6)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                            <b>6</b>
                                        </Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={() => changePinValue(7)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                            <b>7</b>
                                        </Button>
                                    </td>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={() => changePinValue(8)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                            <b>8</b>
                                        </Button>
                                    </td>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={() => changePinValue(9)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                            <b>9</b>
                                        </Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={resetPin} className="w-100 text-center rounded" color="netis-color" style={{fontSize:"18pt"}}>
                                            <b>Reset</b>
                                        </Button>
                                    </td>
                                    <td style={{width:"33.33%"}}>
                                        <Button onClick={() => changePinValue(0)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                            <b>0</b>
                                        </Button>
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>

            <Modal className="text-center p-3" backdropClassName="back-home" contentClassName="modal-home shadow" style={{borderRadius:"15px", marginTop:"40vh", height:"50%"}} isOpen={isLoading}>
                <Spinner size="xl" className="mx-auto mt-3" />
                <h2 className="mt-2 mb-3">Tunggu Sebentar...</h2>
            </Modal>
        </div>
    )
}

export default translate(InputPin);
