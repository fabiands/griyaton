import React, { useState, useEffect, Fragment } from "react";
import { formatDate } from "../../../utils/formatter";
import request from "../../../utils/request";
import { Form, Row, Col, Label, Button, Spinner, Table, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup, ModalFooter, ModalHeader, Modal, ModalBody, Input, UncontrolledPopover, PopoverBody } from "reactstrap";
import { t } from 'react-switch-lang';
import LoadingAnimation from "../../../components/LoadingAnimation";
import DataNotFound from "../../../components/DataNotFound";
import StatusBadge from "./StatusBadge";
import * as moment from 'moment';
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import useFilterDate from "./filterDateHooks";
import { toast } from "react-toastify";

const defaultDateFilterStart = new Date();
const twoWeekAgo = defaultDateFilterStart.getDate() - 14;
defaultDateFilterStart.setDate(twoWeekAgo);

const defaultDateFilterEnd = new Date();
const twoWeekLater = defaultDateFilterEnd.getDate() + 14;
defaultDateFilterEnd.setDate(twoWeekLater)

function OvertimeApproval(props) {
    const { isAdminPanel, user } = props;
    const searchParams = new URLSearchParams(props.location.search);
    const [filters, { DateStartPickerComponent, DateEndPickerComponent }] = useFilterDate({
        start: searchParams.get('start') || defaultDateFilterStart,
        end: searchParams.get('end') || defaultDateFilterEnd
    })

    const [loading, setLoading] = useState(true);
    const [overtimes, setOvertimes] = useState([]);
    const [updateModal, setUpdateModal] = useState(null);
    const [downloadingExcel, setdownloadingExcel] = useState(false);

    function fetchOvertimes() {
        const params = Object.keys(filters)
            .filter(k => filters[k] !== null)
            .reduce((initial, k) => ({ ...initial, [k]: formatDate(filters[k]) }), {});
        setLoading(true);
        const url = isAdminPanel ? 'v1/overtimes/admin/requests' : 'v1/overtimes/requests';
        request.get(url, { params })
            .then(res => setOvertimes(res.data.data))
            .finally(() => setLoading(false));
    }

    useEffect(fetchOvertimes, []);

    function updateStatus(overtimeId, status) {
        setUpdateModal({ overtimeId, status });
    }

    function onUpdatedStatus(data) {
        const idx = overtimes.findIndex(overtime => overtime.id === data.id);
        overtimes[idx] = data;
        setOvertimes(overtimes);
    }

    function downloadExcel() {
        setdownloadingExcel(true)
        const params = Object.keys(filters)
            .filter(k => filters[k] !== null)
            .reduce((initial, k) => ({ ...initial, [k]: formatDate(filters[k]) }), {});

        request.get(`v1/overtimes/recaps/excel`, {
            params,
            responseType: 'arraybuffer',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                const type = res.headers['content-type']
                const blob = new Blob([res.data], { type: type, encoding: 'UTF-8' })
                let filename = "rekap.xlsx";
                const disposition = res.headers['content-disposition'];
                if (disposition && disposition.indexOf('inline') !== -1) {
                    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    const matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) {
                        filename = matches[1].replace(/['"]/g, '');
                    }
                }

                const URL = window.URL || window.webkitURL;
                const downloadUrl = URL.createObjectURL(blob);
                let newWindow = null;

                const iOS = window.navigator.platform && /iPad|iPhone|iPod/.test(window.navigator.platform)
                if (iOS) {
                    const reader = new FileReader();
                    reader.onload = function (e) {
                        newWindow = window.open(reader.result);
                        newWindow.onload = function () {
                            newWindow.document.getElementsByTagName('html')[0]
                                .appendChild(document.createElement('head'))
                                .appendChild(document.createElement('title'))
                                .appendChild(document.createTextNode(filename));
                        }
                        setTimeout(() => {
                            newWindow.document.title = filename;
                        }, 100)
                    }
                    reader.readAsDataURL(blob);
                } else {
                    const link = document.createElement('a')
                    link.href = downloadUrl
                    link.download = filename;
                    link.click();
                    setTimeout(() => {
                        link.remove();
                    }, 1500);
                }
            })
            .catch(err => {
                console.error(err)
            }).finally(() => {
                setdownloadingExcel(false)
            })
    }

    return (<div className="animated fadeIn md-company-header mb-3">
        <div className="align-items-center mb-4">
            <Form onSubmit={(e) => { e.preventDefault(); fetchOvertimes() }}>
                <Row>
                    <Col sm="4">
                        <Label tag="small" htmlFor="start" className="text-muted">{t('dari')}</Label>
                        <br />
                        <DateStartPickerComponent />
                    </Col>
                    <Col sm="4">
                        <Label tag="small" htmlFor="end" className="text-muted">{t('hingga')}</Label>
                        <br />
                        <DateEndPickerComponent />
                    </Col>
                    <Col sm="2">
                        <div className="pt-sm-0 my-1">
                            <Button type="submit" className="mt-sm-4" color="netis-color" style={{ width: '60px' }} disabled={loading}>
                                {loading ? <Spinner color="light" size="sm" /> : t('cari')}
                            </Button>
                        </div>
                    </Col>
                    <Col sm="2">
                        <div className="pt-sm-0 my-1">
                            <Button onClick={downloadExcel} className="mt-sm-4" color="success">
                                {downloadingExcel ? <Fragment><Spinner size="sm" /> Downloading...</Fragment> : <Fragment><i className="fa fa-download mr-2"></i> {t('unduhlaporan')}</Fragment>}
                            </Button>
                        </div>
                    </Col>
                </Row>
            </Form>
        </div>

        <Row className="md-company-header mb-3">
            <Col xs="12">
                {
                    loading ? <LoadingAnimation /> :
                        (overtimes.length === 0 ? <DataNotFound /> :
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th className="text-center w-5">No.</th>
                                        <th className="text-left w-15">{t('nama')}</th>
                                        <th className="text-left w-15">{t('tanggal')}</th>
                                        <th className="text-center w-5">{t('mulai')}</th>
                                        <th className="text-center w-5">{t('hingga')}</th>
                                        <th className="text-left w-20">{t('tujuan')}</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {overtimes.map((data, index) => (
                                        <tr key={index}>
                                            <td className="text-center">{index + 1}</td>
                                            <td>{data.personnel.user.fullName}</td>
                                            <td className="text-nowrap">{moment(data.date).format('DD MMMM YYYY')}</td>
                                            <td className="text-center">{data.start.substr(0, 5)}</td>
                                            <td className="text-center">{data.end ? data.end.substr(0, 5) : <i className="text-muted">NOT SET</i>}</td>
                                            <td style={{ cursor: 'pointer' }}>
                                                <span id={`overtimePurpose${index}`}>{data.message.substr(0, 20) + (data.message.length > 20 ? '...' : '')}</span>
                                                <UncontrolledPopover placement="top" target={`overtimePurpose${index}`}>
                                                    <PopoverBody style={{ whiteSpace: 'pre-wrap' }}>{data.message}</PopoverBody>
                                                </UncontrolledPopover>
                                            </td>
                                            <td className="text-left text-nowrap"><StatusBadge status={data.status} /></td>
                                            <td className="text-right text-nowrap">
                                                {!['done', 'approved2', 'rejected'].includes(data.status) && (<UncontrolledDropdown className="d-inline-block">
                                                    <DropdownToggle color="netis-color" caret>{t('ubah')} Status</DropdownToggle>
                                                    <DropdownMenu>
                                                        {(data.status === 'pending' || (user.personnel.role.id <= 2 && data.status === 'approved1')) && <DropdownItem onClick={() => updateStatus(data.id, 'approved')}>
                                                            <i className="fa fa-circle text-success mr-1"></i> {t('disetujui')}
                                                        </DropdownItem>}
                                                        <DropdownItem onClick={() => updateStatus(data.id, 'rejected')}>
                                                            <i className="fa fa-circle text-danger mr-1"></i> {t('ditolak')}
                                                        </DropdownItem>
                                                    </DropdownMenu>
                                                </UncontrolledDropdown>)}
                                                <Link className="btn btn-netis-color mx-1" to={`/overtimes/detail/${data.id}`}>{t('lihatdetail')}</Link>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        )
                }
            </Col>
        </Row>

        <UpdateStatusModalForm toggle={() => setUpdateModal(null)} payload={updateModal} onUpdated={onUpdatedStatus} />
    </div>)
}

function UpdateStatusModalForm({ toggle, payload, onUpdated }) {
    const [message, setMessage] = useState('');
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        setMessage('')
    }, [payload])

    function submitForm(e) {
        e.preventDefault();
        setLoading(true);
        request.post(`v1/overtimes/requests/${payload.overtimeId}`, { status: payload.status, message })
            .then(res => {
                const { data } = res.data
                onUpdated(data);
                if (data.status === 'approved2') {
                    toast.success("permintaan lembur berhasil disetujui.");
                } else {
                    toast.success("permintaan lembur berhasil ditolak.");
                }
                toggle();
            })
            .catch((err) => {
                toast.error(err.response?.data.message ?? 'Terjadi kesalahan');
                setLoading(false);
                toggle();
            })
            .finally(() => setLoading(false))
    }

    return (
        <Modal isOpen={!!payload} toggle={() => !loading && toggle()}>
            {!!payload && <Form onSubmit={submitForm}>
                <ModalHeader toggle={() => !loading && toggle()}>Update Status</ModalHeader>
                <ModalBody>
                    <FormGroup row>
                        <Label htmlFor="status" className="text-md-right" md="3">Status</Label>
                        <Col md="9">
                            <Input readOnly type="text" value={payload.status} />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label htmlFor="message" className="text-md-right" md="3">Pesan</Label>
                        <Col md="9">
                            <Input type="textarea" name="message" id="message" rows="2" value={message} onChange={(e) => setMessage(e.target.value)} />
                        </Col>
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button type="submit" color="netis-color" disabled={loading}>
                        {
                            loading ?
                                <><Spinner size="sm" color="light" /> Loading...</> :
                                <><i className="fa fa-check mr-2"></i> Update Status</>
                        }
                    </Button>
                </ModalFooter>
            </Form>}
        </Modal>
    );
}

const mapStateToProps = ({ isAdminPanel, user }) => ({ isAdminPanel, user });
export default connect(mapStateToProps)(OvertimeApproval);
