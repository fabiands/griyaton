import React, { Component } from "react";
import { connect } from 'react-redux'
import request from '../../../utils/request'
import { Button, Card, CardBody, Table, Row, Col, CardHeader } from "reactstrap";
import moment from '../../../utils/moment'
import { convertToRupiah } from '../../../utils/formatter'
import StatusBadge from './components/StatusBadge'
import Loading from '../../../components/LoadingAnimation'
import DataNotFound from '../../../components/DataNotFound'
import { isPdf } from './components/helper'
import {
  translate,
} from 'react-switch-lang';
class ReimburseDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      notFound: false,
      reimbursement: null
    };

    this.isPdf = isPdf
  }

  async componentDidMount() {
    try {
      const { data } = await request.get(`v1/reimbursement/request/${this.props.match.params.id}`);
      this.setState({ reimbursement: data.data });
    } catch (err) {
      if (err.response && err.response.status === 404) {
        this.setState({ notFound: true });
      }
      throw err;
    } finally {
      this.setState({ loading: false });
    }
  }

  render() {
    if (this.state.loading) {
      return <Loading />;
    }

    if (this.state.notFound) {
      return <DataNotFound />
    }
    const { t } = this.props;
    const { reimbursement } = this.state;
    return (<div className="animated fadeIn">
      <div className="d-flex align-items-center mb-4">
        <Button color="netis-color" onClick={() => this.props.history.goBack()}><i className="fa fa-chevron-left mr-2"></i> {t('kembali')} </Button>
      </div>
      <Card>
        <CardBody>
          <Row>
            <Col md="6">
              <Table hover>
                <tbody>
                  <tr>
                    <th>{t('tanggal')}</th><td>{moment(reimbursement.datetime).format('DD MMMM YYYY')}</td>
                  </tr>
                  <tr>
                    <th>{t('deskripsi')}</th><td>{reimbursement.description}</td>
                  </tr>
                  <tr>
                    <th>Nominal</th><td>{convertToRupiah(reimbursement.nominal)}</td>
                  </tr>
                  <tr>
                    <th>Status</th><td><StatusBadge status={reimbursement.status} /></td>
                  </tr>
                </tbody>
              </Table>
            </Col>
            <Col md="6">
              {this.isPdf(reimbursement.document) ?
                <Button href={reimbursement.document} target="_blank" rel="noopener noreferrer" color="netis-color"><i className="fa fa-pdf"></i> Buka Lampiran</Button> :
                <img src={reimbursement.document} width="100%" alt="document" />
              }
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          {t('riwayat')} Status
        </CardHeader>
        <CardBody>
          <Table>
            {reimbursement.status_log && reimbursement.status_log.map((statusLog, index) => (
              <tr key={index}>
                <td style={{ width: 170 }}>
                  <strong>{(statusLog.subject && statusLog.subject.nickName) || 'system'}</strong><br />
                  <span className="text-muted">{statusLog.created}</span>
                </td>
                <td>
                  <strong><StatusBadge status={statusLog.status} /></strong>
                  <br /> {statusLog.message}
                </td>
              </tr>
            ))}
          </Table>
        </CardBody>
      </Card>
    </div>);
  }
}

const mapStateToProps = (state) => ({
  sessionId: state.sessionId
});

export default connect(mapStateToProps)(translate(ReimburseDetail));
