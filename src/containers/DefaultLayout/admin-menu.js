import { lazy } from "react";
import { t } from "react-switch-lang";

// route object
// {url, component, privileges, menu: { name, icon }}
export default () => [
  {
    url: "/dashboard",
    component: lazy(() => import("../../views/Menu/Dashboard/Dashboard")),
    menu: {
      name: "Dashboard",
      icon: "icon-home",
    },
  },
  {
    url: "/company",
    exact: true,
    component: lazy(() => import("../../views/Menu/Company/CompanyMenu")),
    oneOfPrivileges: [
      "read-company-profile",
      "read-company-document",
      "read-company-bank",
      "read-company-location",
      "read-company-structure",
      "read-company-holiday",
      "read-company-overtime",
      "read-company-role",
    ],
    menu: {
      name: t("perusahaan"),
      icon: "icon-note",
    },
  },
  {
    url: "/company/pengaturan-dasar",
    exact: true,
    component: lazy(() =>
      import("../../views/Menu/Company/BasicSetting/PengaturanDasar")
    ),
    privileges: [
      "read-company-profile",
      "read-company-document",
      "read-company-bank",
      "read-company-location",
    ],
  },
  {
    url: "/company/pengaturan-jabatan",
    exact: true,
    component: lazy(() =>
      import("../../views/Menu/Company/PositionSetting/PengaturanJabatan")
    ),
    privileges: ["read-company-structure"],
  },
  {
    url: "/company/pengaturan-role",
    exact: true,
    component: lazy(() =>
      import("../../views/Menu/Company/RoleSetting/RoleSetting")
    ),
    privileges: ["read-company-role"],
  },
  {
    url: "/company/pengaturan-role/create",
    exact: true,
    component: lazy(() =>
      import("../../views/Menu/Company/RoleSetting/RoleCreate")
    ),
    privileges: ["read-company-role"],
  },
  {
    url: "/company/pengaturan-profil-perusahaan",
    exact: true,
    component: lazy(() =>
      import("../../views/Menu/Company/CompanyProfile/CompanyProfile")
    ),
    privileges: ["read-company-profile"],
  },
  {
    url: "/company/pengaturan-cuti-perusahaan",
    exact: true,
    component: lazy(() =>
      import("../../views/Menu/Company/CompanyCuti/CompanyCuti")
    ),
    privileges: ["read-company-holiday"],
  },
  {
    url: "/company/pengaturan-lembur-perusahaan",
    exact: true,
    component: lazy(() =>
      import(
        "../../views/Menu/Company/OvertimePolicySetting/OvertimePolicySetting"
      )
    ),
    privileges: ["read-company-overtime"],
  },
  {
    url: "/personnel",
    component: lazy(() => import("../../views/Menu/Personnel/PersonnelMenu")),
    privileges: ["browse-employee-dataActive"],
    exact: true,
    menu: {
      name: t("karyawan"),
      icon: "icon-people",
    },
  },
  {
    url: "/personnel/working-shift",
    component: lazy(() =>
      import("../../views/Menu/Personnel/PersonnelWorkingShiftv2/PersonnelWorkingShiftWrapper")
    ),
    privileges: ["read-employee-workingShift"],
  },
  {
    url: "/personnels",
    exact: true,
    component: lazy(() => import("../../views/Menu/Personnel/PersonnelList")),
    privileges: ["browse-employee-dataActive", "browse-employee-dataNonactive"],
  },
  {
    url: "/personnels/:id",
    component: lazy(() => import("../../views/Menu/Personnel/PersonnelDetail")),
    privileges: ["read-employee"],
  },
  {
    url: "/attendance",
    component: lazy(() =>
      import("../../views/Menu/Attendance/History/AttendanceHistoryAll")
    ),
    exact: true,
    oneOfPrivileges: ["browse-attendance"],
    menu: {
      name: t("absensi"),
      icon: "icon-pencil",
    },
  },
  {
    url: "/overtimes",
    component: lazy(() => import("../../views/Menu/Overtime/OvertimeWrapper")),
    privileges: ["browse-overtime"],
    menu: {
      name: t("Lembur"),
      icon: "icon-hourglass",
    },
  },
  {
    url: "/calendar",
    component: lazy(() => import("../../views/Menu/Calendar/Calendar")),
    privileges: ["browse-calendar"],
    menu: {
      name: t("kalender"),
      icon: "icon-calendar",
    },
  },
  {
    url: "/cuti",
    component: lazy(() => import("../../views/Menu/Cuti/CutiWrapper")),
    privileges: ["browse-holiday"],
    menu: {
      name: t("cuti"),
      icon: "icon-user-unfollow",
    },
  },
  {
    url: "/reimburse",
    component: lazy(() =>
      import("../../views/Menu/Reimburse/ReimburseWrapper")
    ),
    privileges: ["browse-reimburse"],
    menu: {
      name: t("Reimburse"),
      icon: "icon-wallet",
    },
  },
  {
    url: "/assessment",
    component: lazy(() =>
      import("../../views/Menu/Assesment/ManageAssessmentResult")
    ),
    privileges: ["read-assessment-result"],
    exact: true,
    menu: {
      name: t("Asesmen"),
      icon: "icon-puzzle",
    },
  },
  {
    url: "/assessment/results/:id",
    component: lazy(() =>
      import("../../views/Menu/Assesment/AssessmentResult")
    ),
    privileges: ["read-assessment-result"],
  },
  {
    url: "/recruitment",
    component: lazy(() =>
      import("../../views/Menu/Recruitment/RecruitmentWrapper")
    ),
    privileges: ["browse-job"],
    menu: {
      name: t("Rekrutmen"),
      icon: "icon-user-follow",
    },
  },
  {
    url: "/consulting",
    component: lazy(() => import("../../views/Menu/Consulting/ConsultingMenu")),
    privileges: ["browse-consulting"],
    menu: {
      name: t("Konsul"),
      icon: "icon-directions",
    },
  },
  {
    url: "/profile",
    component: lazy(() =>
      import("../../views/Menu/Personnel/PersonnelDetailUser")
    ),
  },
];
