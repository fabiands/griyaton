import { apiRequest } from "../actions/api";
import { LOGIN, GET_ME, setUser } from "../actions/auth";
import request from "../utils/request";
// import { LOGOUT } from "../actions/auth";

export const appMiddleware = ({ dispatch }) => next => action => {
    next(action);
    switch (action.type) {
        case LOGIN: {
            next(
                apiRequest({
                    url: `auth/login`,
                    data: action.payload
                })
            );
            break;
        }
        case GET_ME: {
            request.get(`/auth/me`)
                .then(response => {
                    dispatch(setUser(response.data))
                })
            break;
        }
        default:
            break;
    }
};
