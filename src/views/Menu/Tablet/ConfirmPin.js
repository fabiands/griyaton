import React,{useMemo, useState, useEffect} from 'react';
import {Card, Label, Button, Table, Modal, Spinner} from 'reactstrap';
import {translate, t} from 'react-switch-lang';
// import PinInput from 'react-pin-input';
import {Redirect, useHistory, useLocation} from "react-router-dom";
import {useFormik} from 'formik';
import * as Yup from 'yup';
import AfirmationModal from './components/AfirmationModal';
import Color from '../../../utils/Colors';
import request from '../../../utils/request';
import { toast } from 'react-toastify';
// import CaptureLoading from './components/CaptureLoading'

function ConfirmPin(){

    const history = useHistory();
    const location = useLocation();
    const registerData = location.state?.registerData;
    const faceData = location.state?.faceData;
    const pin = location.state?.pin;
    const [isSuccess, setIsSuccess] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const ValidatePinSchema = useMemo(() => {
        return Yup.object().shape({
            confirmpin: Yup.string()
                .required()
                .test('samePIN', t('PIN yang diisikan tidak sama'), function(value) {
                    return value === pin
                })
        })
    }, [pin]);

    const {values, touched, errors, isSubmitting, ...formik} = useFormik({
        initialValues:{
            confirmpin:''
        },
        validationSchema: ValidatePinSchema,
        onSubmit: (values, {setSubmitting}) =>{
            setSubmitting(true);
            setIsLoading(true);
                console.log(values.confirmpin);
                console.log(registerData.value);
                console.log(faceData);

                const formData = new FormData();
                formData.append('personnelId', registerData.value);
                formData.append('pin', values.confirmpin);
                faceData.forEach(blob => {
                    formData.append('images[]', blob)
                })
                // formData.append('faceImage', faceData);

                request.post('/external/attendance/register', formData)
                .then(({ status: res, data }) => {
                        console.log(res, data);

                        if(res === 200){
                            setIsLoading(false)
                            setIsSuccess(true)
                        }
                        // else{
                        //     setIsLoading(false)
                        //     console.log(res)
                        // }
                        // setSubmitting(false)

                    })
                    .catch(err => {
                        // setSubmitting(false)
                        setIsLoading(false);
                        console.log(err);
                        toast.error('Gagal melakukan registrasi')
                        Promise.reject(err);
                        return;
                    })
                    .finally(() => setSubmitting(false));
        }
    })

    useEffect(() => {
        if (values.pin.length === 6){
            formik.handleSubmit();
        }
    // eslint-disable-next-line
    }, [values.pin])

    if (!registerData || !faceData || !pin) {
        return (<Redirect to="/tab"></Redirect>);
    }

    const resetPin = function(){
        formik.setFieldValue('pin', "");
    }

    function changePinValue(value){
        formik.setFieldValue('pin', "" + values.pin + value)
    }

    if (isSuccess) {
        return (
            <AfirmationModal
                visible={true}
                title="Success"
                text={`${t('Berhasil melakukan Registrasi ')}`}
                onPressButton={() => {
                    setIsSuccess(false)
                    history.push('/tab');
                }}
                titleButton="OK"
                titleColor={Color.white}
                bgColorButton={Color.primaryColor}
            />
        );
    }

    // if (isLoading) {
    //     return <CaptureLoading title="Loading" visible={true} />;
    // }

    return(
        <div className="app flex-row align-items-center background-login background-tablet2">
            <Button onClick={() => history.push('/tab')} className="back-tablet p-2">
                    <i className="fa fa-home fa-4x" />
            </Button>
            <div className="w-100">
                <div className="h-100">
                    <div className="text-center mx-auto welcome-logo mt-0 mb-5" style={{width:"40%", maxHeight:"100px"}} />
                    <div className="text-center mt-5">
                        <div className="w-75 text-center my-4 mx-auto">
                        <Label htmlFor="user" className="input-label">
                            <h3>{t('Konfirmasi PIN Anda')}</h3>
                        </Label>
                        </div>
                        <div className="d-flex justify-content-center text-center my-5 mx-auto">
                            <Card className="pin-card align-middle mx-2">{values.pin[0] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[1] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[2] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[3] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[4] ? <i className="fa fa-circle" /> : null}</Card>
                            <Card className="pin-card align-middle mx-2">{values.pin[5] ? <i className="fa fa-circle" /> : null}</Card>
                        </div>
                        <div className="text-center">
                            {touched.confirmpin && errors.confirmpin &&
                                <h5 className="text-danger">
                                    {errors.confirmpin}
                                </h5>
                            }
                        </div>
                        <Table borderless style={{backgroundColor:"#C0C0C0"}} className="mt-5">
                            <tr>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(1)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>1</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(2)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>2</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(3)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>3</b>
                                    </Button>
                                </td>
                            </tr>
                            <tr>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(4)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>4</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(5)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>5</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(6)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>6</b>
                                    </Button>
                                </td>
                            </tr>
                            <tr>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(7)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>7</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(8)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>8</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(9)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>9</b>
                                    </Button>
                                </td>
                            </tr>
                            <tr>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={resetPin} className="w-100 text-center rounded" color="netis-color" style={{fontSize:"18pt"}}>
                                        <b>Reset</b>
                                    </Button>
                                </td>
                                <td style={{width:"33.33%"}}>
                                    <Button onClick={() => changePinValue(0)} className="w-100 text-center rounded" style={{backgroundColor:"#fff", fontSize:"24pt"}}>
                                        <b>0</b>
                                    </Button>
                                </td>
                            </tr>
                        </Table>
                    </div>
                </div>
            </div>

            <Modal className="text-center p-3" backdropClassName="back-home" contentClassName="modal-home shadow" style={{borderRadius:"15px", marginTop:"40vh", height:"50%"}} isOpen={isLoading}>
                <Spinner size="xl" className="mx-auto mt-3" />
                <h2 className="mt-2 mb-3">Tunggu Sebentar...</h2>
            </Modal>
        </div>
    )
}

export default translate(ConfirmPin);
