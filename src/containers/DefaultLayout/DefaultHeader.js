import React, { Component, Fragment, useState, useMemo } from 'react';
import request from "../../utils/request";
import { UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, Col, InputGroup, InputGroupAddon, Form, FormText, Spinner } from 'reactstrap';
import PropTypes from 'prop-types';
// import { Redirect } from "react-router-dom";
// import { NavLink } from 'react-router-dom';
import { withRouter } from "react-router";
import {
  Row,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  Label,
  Input
} from 'reactstrap';
import Select from 'react-select';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import icon from '../../assets/img/icon.png'
import { connect } from 'react-redux';
import Axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  translate,
} from 'react-switch-lang';
import langUtils from '../../utils/language/index';
import * as moment from 'moment';
import { AppNavbarBrand, AppSidebarToggler } from "@coreui/react";
import logo from "../../assets/assets_ari/logo.png";
import { logout, setUser } from "../../actions/auth";
import { setPanel } from "../../actions/ui";

// setLanguageCookie()
class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: props.user,
      session: props.token,
      modalData: false,
      currentPass: "",
      confirmPass: "",
      modalLang: false,
      newPass: "",
      newCompany: null,
      oldCompany: null,
      companyList: [],
      modalPIN:false,
      modalChangePIN:false,
      userPrivileges: this.props.user.privileges,
    };
  }

  changeLanguage = (id) => (e) => {
    e.preventDefault();
    console.log(id);
    this.handleSetLanguage(id);
    this.setState({
      modalLang: !this.state.modalLang,
    });
    window.location.reload();
  };
  changePass = () => {
    this.setState({
      modalData: !this.state.modalData,
    });
  };
  modalChangeLang = () => {
    this.setState({
      modalLang: !this.state.modalLang,
    });
  }

  createPIN = () => {
    this.setState({
      modalPIN: !this.state.modalPIN,
    });
  }

  changePIN = () => {
    this.setState({
      modalChangePIN: !this.state.modalChangePIN,
    });
  }

  changeAdmin = async () => {
    await this.props.setPanel("2");
    this.props.history.push("/dashboard");
  };
  changeUser = async () => {
    await this.props.setPanel("3");
    this.props.history.push("/dashboard");
    // window.location.reload();
    // window.location.replace("/home");
  };
  changeProfile = () => {
    const { history } = this.props;
    history.push("/profile");
    // window.location.replace("/profile/user");
  };
  handleChangeCurrent = (e) => {
    this.setState({
      currentPass: e.target.value,
    });
  };
  handleChangeConfirm = (e) => {
    this.setState({
      confirmPass: e.target.value,
    });
  };
  handleChangeNew = (e) => {
    this.setState({
      newPass: e.target.value,
    });
  };
  handleChangeLang = (e) => {
    this.setState({
      modalLang: e.target.value,
    });
  };
  cekSubmitData = (e) => {
    const { t } = this.props;
    if (this.state.newPass !== this.state.confirmPass) {
      toast.error(t("konfirmasipasssalah"), { autoClose: 3000 });
    } else {
      this.submitData();
    }
  };
  submitData = (e) => {
    const dataObject = {
      current: this.state.currentPass,
      new: this.state.newPass,
    }
    Axios.post('/api/auth/changepassword', dataObject, { headers: { "Authorization": `Bearer ${this.state.session}` } })
      .then((res) => {
        this.setState({
          modalData: false,
          currentPass: "",
          confirmPass: "",
          newPass: "",
        });
        this.props.logout();
      })
      .catch((error) => {
        toast.error(JSON.stringify(error.response.data.message), {
          autoClose: 3000,
        });
      });
  };

  toggleChangeCompany = async () => {
    this.setState({
      modalChangeCompany: !this.state.modalChangeCompany,
      newCompany: this.state.oldCompany,
    });

    if (this.state.companyList.length === 0)
      Axios.get('/api/auth/change-company', { headers: { "Authorization": `Bearer ${this.state.session}` } })
        .then((res) => {
          let oldCompany = {};
          const companyList = [];
          res.data.data.data.map((data) => {
            let tmp = {};
            tmp.value = data.id;
            tmp.label = data.name;
            companyList.push(tmp);

            if (data.id === this.state.user.personnel.company.id)
              oldCompany = tmp;
            return data;
          });
          this.setState({ companyList, oldCompany, newCompany: oldCompany });
        })
        .catch((error) => console.log(error));
  };
  handleChangeCompany = (value) => {
    this.setState({
      newCompany: value,
    });
  };
  submitChangeCompany = () => {
    this.setState({ modalChangeCompany: false });

    Axios.post('/api/auth/change-company', { companyId: this.state.newCompany.value }, { headers: { "Authorization": `Bearer ${this.state.session}` } })
      .then((res) => {
        const newUser = { ...this.props.user };
        newUser.personnel.company.id = this.state.newCompany.value;
        newUser.personnel.company.name = this.state.newCompany.label;
        setUser(newUser);

        window.location.href = "/home";
      })
      .catch((error) => console.log(error));
  };
  onSelectFlag = (countryCode) => {
    this.handleSetLanguage(countryCode);
    moment.locale(countryCode.toLowerCase());
  };
  handleSetLanguage = (key) => {
    langUtils.setLanguage(key);
  };

  onAvatarError(e) {
    const img = e.target;
    img.onerror = null;
    img.src = "/assets/img/avatars/avatar.png";
    img.style.border = null;
  }

  render() {
    // eslint-disable-next-line
    const { t, user, panelMenu: menu } = this.props;
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          style={{
            position: "initial",
            top: "unset",
            left: "unset",
            marginLeft: 0,
          }}
          full={{ src: logo, width: 150, alt: "Widya Skilloka Logo" }}
          minimized={{ src: icon, width: 30, alt: "Widya Skilloka Icon" }}
        />

        <Nav navbar>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>{langUtils.getLanguage()}</DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header>{t("Pilih Bahasa")}</DropdownItem>
              <DropdownItem onClick={() => this.onSelectFlag("ID")}>
                Bahasa Indonesia
              </DropdownItem>
              <DropdownItem onClick={() => this.onSelectFlag("EN")}>
                English (US)
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>

          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              {this.state.user.avatar === null ?
              <img src="/assets/img/avatars/avatar.png" style={{ objectFit: 'cover', border: 'solid 1px #000', width: 35, height: 35, marginTop: -1, marginLeft: -1, borderRadius: 9999 }} alt="avatar" />
                :
                <img src={this.state.user.avatar} style={{ objectFit: 'cover', border: 'solid 1px #000', width: 35, height: 35, marginTop: -1, marginLeft: -1, borderRadius: 9999 }} onError={this.onAvatarError} alt="avatar" />
              }
            </DropdownToggle>
            <DropdownMenu right>
              {this.state.userPrivileges.length > 0 &&
                menu === "3" ? (
                  <Fragment>
                    <DropdownItem onClick={this.changeAdmin}>
                      <i className="fa fa-user"></i>Panel Admin
                  </DropdownItem>
                  </Fragment>
                ) : this.state.userPrivileges.length > 0 && menu === "2" ? (
                  <DropdownItem onClick={this.changeUser}>
                    <i className="fa fa-user"></i>Panel User
                  </DropdownItem>
                ) : null}
              {menu === "3" &&
                < DropdownItem onClick={this.changeProfile}>
                  <i className="fa fa-user-circle-o"></i>Profile
                </DropdownItem>
              }
              {this.state.userPrivileges.includes("change-company") &&
                menu === "2" &&
                user.hasSubsidiary === true ? (
                  <DropdownItem onClick={this.toggleChangeCompany}>
                    <i className="fa fa-building-o"></i>
                    {t("lihatperusahaan")}
                  </DropdownItem>
                ) : null}
              <DropdownItem onClick={this.changePass}>
                <i className="fa fa-key"></i>
                {t("gantipassword")}
              </DropdownItem>
              {
                user.pin ?
                <DropdownItem onClick={(this.changePIN)}><i className="fa fa-bullseye"></i>{t('gantipin')}</DropdownItem>
                :
                <DropdownItem onClick={(this.createPIN)}><i className="fa fa-bullseye"></i>{t('buatpin')}</DropdownItem>
              }
              <DropdownItem onClick={this.props.logout}>
                <i className="fa fa-lock"></i>
                {t("keluar")}
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        {/*Change Lang*/}
        <Modal isOpen={this.state.modalLang} toggle={this.modalChangeLang}>
          <ModalBody>
            <h5 className="content-sub-title mb-4"> {t("gantibahasa")}</h5>

            <Row>
              <div className="col-md-12">
                <FormGroup>
                  <Row>
                    <Col sm="12" className="text-center">
                      <Label htmlFor="lang" className="input-label ">
                        {t("pilihbahasa")}
                      </Label>
                    </Col>
                    <Col sm="6" className="text-right">
                      <img
                        src={require("../../assets/assets_ari/indonesia.png")}
                        style={{ width: 80, cursor: "pointer" }}
                        alt="indonesia"
                        onClick={this.changeLanguage("ID")}
                      />
                    </Col>
                    <Col sm="6">
                      <img
                        src={require("../../assets/assets_ari/united-states.png")}
                        style={{ width: 80, cursor: "pointer" }}
                        alt="uk"
                        onClick={this.changeLanguage("US")}
                      />
                    </Col>
                  </Row>
                </FormGroup>
              </div>
            </Row>
            <Row>
              <div className="col-8 d-flex justify-content-end"></div>
              <div className="col-4 d-flex justify-content-end">
                <Button
                  className="mr-2"
                  color="white"
                  onClick={this.modalChangeLang}
                >
                  {t("batal")}
                </Button>
              </div>
            </Row>
          </ModalBody>
        </Modal>
        {/*Change Pass*/}
        <Modal isOpen={this.state.modalData} toggle={this.modalData}>
          <ModalBody>
            <h5 className="content-sub-title mb-4">{t("gantipassword")}</h5>

            <Row>
              <div className="col-md-12">
                <FormGroup>
                  <Label htmlFor="current" className="input-label">
                    {t("passwordlama")}
                  </Label>
                  <Input
                    type="password"
                    name="current"
                    id="current"
                    onChange={this.handleChangeCurrent}
                  />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="new" className="input-label">
                    {t("passwordbaru")}
                  </Label>
                  <Input
                    type="password"
                    name="new"
                    id="new"
                    onChange={this.handleChangeNew}
                  />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="confirm" className="input-label">
                    {t("konfirmasipasswordbaru")}
                  </Label>
                  <Input
                    type="password"
                    name="confirm"
                    id="confirm"
                    onChange={this.handleChangeConfirm}
                  />
                </FormGroup>
              </div>
            </Row>
            <Row>
              <div className="col-8 d-flex justify-content-end"></div>
              <div className="col-4 d-flex justify-content-end">
                <Button
                  className="mr-2"
                  color="white"
                  onClick={this.changePass}
                >
                  {t("batal")}
                </Button>
                <Button
                  type="submit"
                  color="netis-color"
                  onClick={this.cekSubmitData}
                >
                  {t("simpan")}
                </Button>
              </div>
            </Row>
          </ModalBody>
        </Modal>
        {/* Modal Change Company */}
        <Modal
          isOpen={this.state.modalChangeCompany}
          toggle={this.toggleChangeCompany}
        >
          <ModalBody>
            <h5 className="content-sub-title mb-4">
              {t("seesubsidarycompany")}
            </h5>
            <Row>
              <div className="col-md-12">
                <FormGroup>
                  <Select
                    value={this.state.newCompany}
                    onChange={(value) => this.handleChangeCompany(value)}
                    options={this.state.companyList}
                  />
                </FormGroup>
              </div>
            </Row>
            <Row>
              <div className="col-8 d-flex justify-content-end"></div>
              <div className="col-4 d-flex justify-content-end">
                <Button
                  className="mr-2"
                  color="white"
                  onClick={this.toggleChangeCompany}
                >
                  {t("batal")}
                </Button>
                <Button
                  type="submit"
                  color="netis-color"
                  onClick={this.submitChangeCompany}
                >
                  {t("lihat")}
                </Button>
              </div>
            </Row>
          </ModalBody>
        </Modal>
        {/*Create or Change PIN*/}
        <PINCreateModal isOpen={this.state.modalPIN} toggle={this.createPIN} />
        <PINChangeModal isOpen={this.state.modalChangePIN} toggle={this.changePIN} />
      </React.Fragment>
    );
  }
}

const PINCreateModal = translate(({ isOpen, toggle, t }) => {
  const [showPIN, setShowPIN] = useState(false);
  const [editLoading, setEditLoading] = useState(false);
  const CreatePINSchema = useMemo(() => {
      return Yup.object().shape({
        pin: Yup.string()
          .required()
          .matches(/[0-9]+/, {message: t('PIN harus berupa angka'), excludeEmptyString: true})
          .min(6, t('PIN harus berisi tepat 6 digit angka'))
          .max(6, t('PIN harus berisi tepat 6 digit angka'))
          .label("PIN"),
        newPIN: Yup.string()
          .required()
          .test('samePIN', t('PIN yang diisikan tidak sama'), function(value) {
            return value === this.parent.pin
          })
          .label(t('Konfirmasi PIN'))
      })
  }, [t]);

  const { values, touched, errors, isSubmitting, ...formik} = useFormik({
      initialValues: {
          pin: '',
          newPIN:''
      },
      validationSchema: CreatePINSchema,
      onSubmit: (values, {setSubmitting, setErrors}) => {
          setSubmitting(true)
          setEditLoading(true);
          request.put('/personnels/pin/register', values)
                .then(res => {
                    setEditLoading(false);
                    toggle();
                    toast.success(t('pinberhasil'));
                })
                .catch(err => {
                    if (err.response?.status === 422) {
                        setErrors(err.response.data.errors);
                        return;
                    }
                    toast.error(t('pingagal'));
                    setEditLoading(false);
                    Promise.reject(err);
                })
                .finally(() => setSubmitting(false));
      }
  })

  const handleNumberOnly = (evt) => {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
          evt.preventDefault()}

        return true;
  }

  return <Modal isOpen={isOpen} toggle={toggle}>
      <Form onSubmit={formik.handleSubmit} autoComplete="off">
        <Input type="text" style={{display:"none"}} autoComplete="username" id="username" name="username" />
        <Input type="password" style={{display:"none"}} autoComplete="current-password" id="password" name="password" />
        <ModalHeader toggle={toggle}>{t('buatpinbaru')}</ModalHeader>
        <ModalBody>
          <Row form>
          <Col xs="12" className="mb-3">
            <label htmlFor="pin" className="input-label">{t('pinbaru')}</label>&nbsp;&nbsp;<span className="required">*</span>
            <InputGroup className="w-50">
              <Input onKeyPress={handleNumberOnly} pattern="[0-9]*" inputMode="numeric" required type={showPIN ? 'text' : 'password'} value={values.pin} id="pin" name="pin" onChange={formik.handleChange} autoComplete="nope" />
              <InputGroupAddon addonType="append">
                  <Button type="button"
                    tabIndex="-1"
                    onMouseUp={() => setShowPIN(false)}
                    onMouseOut={() => showPIN && setShowPIN(false)}
                    onTouchStart={() => setShowPIN(true)}
                    onTouchCancel={() => showPIN && setShowPIN(false)}
                    onTouchEnd={() => showPIN && setShowPIN(false)}
                    onMouseDown={() => setShowPIN(true)}
                    className="input-group-text input-group-transparent">
                    {showPIN ? <i className="fa fa-eye-slash"></i> : <i className="fa fa-eye"></i>}
                  </Button>
              </InputGroupAddon>
            </InputGroup>
            <FormText color="muted"><i>{t('PIN harus berupa 6 digit angka')}</i></FormText>
            {touched.pin && errors.pin && <small className="text-danger">{errors.pin}</small>}
          </Col>
          <Col xs="12">
            <label htmlFor="newPIN" className="input-label">{t('konfirmpin')}</label>&nbsp;<span className="required">*</span>
            <Input onKeyPress={handleNumberOnly} pattern="[0-9]*" inputMode="numeric" className="w-50" required autoComplete="new-password" type="password" value={values.newPIN} id="newPIN" name="newPIN" onChange={formik.handleChange} />
            {touched.newPIN && errors.newPIN && <small className="text-danger">{errors.newPIN}</small>}
          </Col>
          <div className="d-flex mt-3 justify-content-end">
            <Button type="submit" color="netis-color" disabled={editLoading}>
              {editLoading ? <><Spinner size="sm" color="light" />Loading...</>
              :
              <><i className="fa fa-check mr-2"></i>{t('simpan')}</>
              }
            </Button>
          </div>
          </Row>
          </ModalBody>
      </Form>
  </Modal>
})

const PINChangeModal = translate(({ isOpen, toggle, t }) => {
  const [showPIN, setShowPIN] = useState(false);
  const [showPIN2, setShowPIN2] = useState(false);
  const [editLoading, setEditLoading] = useState(false);
  const ChangePINSchema = useMemo(() => {
      return Yup.object().shape({
        changePIN: Yup.string()
          .required()
          .matches(/[0-9]+/, {message: t('PIN harus berupa angka'), excludeEmptyString: true})
          .min(6, t('PIN harus berisi tepat 6 digit angka'))
          .max(6, t('PIN harus berisi tepat 6 digit angka'))
          .label("PIN"),
        confirmPIN: Yup.string()
          .required()
          .test('samePIN', t('PIN yang diisikan tidak sama'), function(value) {
            return value === this.parent.changePIN
          })
          .label(t('Konfirmasi PIN'))
      })
  }, [t]);

  const { values, touched, errors, isSubmitting, ...formik} = useFormik({
      initialValues: {
          password:'',
          changePIN: '',
          confirmPIN:''
      },
      validationSchema: ChangePINSchema,
      onSubmit: (values, {setSubmitting, setErrors}) => {
          setSubmitting(true)
          setEditLoading(true);
          request.put('/personnels/pin/edit', {
                pin:values.changePIN,
                password:values.password
              })
                .then(res => {
                    console.log(res.data.data)
                    // setEditLoading(false);
                    toggle();
                    toast.success(t('PIN berhasil diganti'));
                })
                .catch(err => {
                    if (err.response?.status === 403) {
                      console.log(err.response.data);
                      setErrors({ password: t('Password yang anda masukkan salah')})
                      return;
                    }
                    else if (err.response?.status === 422) {
                      setEditLoading(false);
                      setErrors(err.response.data.errors);
                      return;
                    }
                    else toast.error(t('terjadikesalahan'))

                    Promise.reject(err);
                })
                .finally(() => {
                  setEditLoading(false);
                  setSubmitting(false);
                });
      }
  })

  const handleNumberOnly = (evt) => {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
          evt.preventDefault()}

        return true;
  }

  return <Modal isOpen={isOpen} toggle={toggle}>
      <Form onSubmit={formik.handleSubmit} autoComplete="off">
        <ModalHeader toggle={toggle}>{t('gantipinbaru')}</ModalHeader>
        <ModalBody>
          <Row form>
          <Col xs="12" className="mb-6">
            <label htmlFor="password" className="input-label">{t('Password akun anda')}</label>&nbsp;&nbsp;<span className="required">*</span>
            <InputGroup className="w-75">
              <Input required type={showPIN ? 'text' : 'password'} value={values.password} id="password" name="password" onChange={formik.handleChange} autoComplete="nope"
                placeholder={t('Password akun anda')+"..."}
              />
              <InputGroupAddon addonType="append">
                  <Button type="button"
                    tabIndex="-1"
                    onMouseUp={() => setShowPIN(false)}
                    onMouseOut={() => showPIN && setShowPIN(false)}
                    onTouchStart={() => setShowPIN(true)}
                    onTouchCancel={() => showPIN && setShowPIN(false)}
                    onTouchEnd={() => showPIN && setShowPIN(false)}
                    onMouseDown={() => setShowPIN(true)}
                    className="input-group-text input-group-transparent">
                    {showPIN ? <i className="fa fa-eye-slash"></i> : <i className="fa fa-eye"></i>}
                  </Button>
              </InputGroupAddon>
            </InputGroup>
            {touched.password && errors.password && <small className="text-danger">{errors.password}</small>}
          </Col>
          <Col xs="12" className="my-2"><hr /></Col>
          <Col xs="12" className="my-2">
            <label htmlFor="changePIN" className="input-label">{t('pinbaru')}</label>&nbsp;&nbsp;<span className="required">*</span>
            <InputGroup className="w-50">
              <Input onKeyPress={handleNumberOnly} pattern="[0-9]*" inputMode="numeric" required type={showPIN2 ? 'text' : 'password'} value={values.changePIN} id="changePIN" name="changePIN" onChange={formik.handleChange} autoComplete="nope" />
              <InputGroupAddon addonType="append">
                  <Button type="button"
                    tabIndex="-1"
                    onMouseUp={() => setShowPIN2(false)}
                    onMouseOut={() => showPIN2 && setShowPIN2(false)}
                    onTouchStart={() => setShowPIN2(true)}
                    onTouchCancel={() => showPIN2 && setShowPIN2(false)}
                    onTouchEnd={() => showPIN2 && setShowPIN2(false)}
                    onMouseDown={() => setShowPIN2(true)}
                    className="input-group-text input-group-transparent">
                    {showPIN2 ? <i className="fa fa-eye-slash"></i> : <i className="fa fa-eye"></i>}
                  </Button>
              </InputGroupAddon>
            </InputGroup>
            <FormText color="muted"><i>{t('PIN harus berupa 6 digit angka')}</i></FormText>
            {touched.changePIN && errors.changePIN && <small className="text-danger">{errors.changePIN}</small>}
          </Col>
          <Col xs="12">
            <label htmlFor="confirmPIN" className="input-label">{t('konfirmpin')}</label>&nbsp;<span className="required">*</span>
            <Input onKeyPress={handleNumberOnly} pattern="[0-9]*" inputMode="numeric" className="w-50" required autoComplete="new-password" type="password" value={values.confirmPIN} id="confirmPIN" name="confirmPIN" onChange={formik.handleChange} />
            {touched.confirmPIN && errors.confirmPIN && <small className="text-danger">{errors.confirmPIN}</small>}
          </Col>
          <div className="d-flex mt-3 justify-content-end">
            <Button type="submit" color="netis-color" disabled={editLoading}>
              {editLoading ? <><Spinner size="sm" color="light" />Loading...</>
              :
              <><i className="fa fa-check mr-2"></i>{t('simpan')}</>
              }
            </Button>
          </div>
          </Row>
          </ModalBody>
      </Form>
  </Modal>
})

DefaultHeader.propTypes = {
  t: PropTypes.func.isRequired,
};
const mapStateToProps = ({ menu: panelMenu, user, token }) => ({
  panelMenu,
  user,
  token,
});
export default connect(mapStateToProps, { logout, setPanel, setUser })(
  withRouter(translate(DefaultHeader))
);
