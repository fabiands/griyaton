import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Card, CardBody, CardTitle, Modal, ModalBody, Button, Alert } from 'reactstrap';
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import Widget01 from './Component/Widget01';
import { formatDate } from '../../../utils/formatter';
import request from '../../../utils/request';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import {
    translate,
} from 'react-switch-lang';

const localizer = momentLocalizer(moment);

toast.configure()
class DashboardUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dropdownOpen: [false, false],
            session: props.token,
            user: props.user,
            eventDate: new Date(),
            date: new Date(),
            startDate: '',
            endDate: '',
            holidayBalance: 0,
            holidayStatus: {},
            reimburseStatus: {},
            events: [],
            event: {}
        };
    }

    componentDidMount = () => {
        this.getHolidayStatus()
        this.getReimburseStatus()
        this.setState({
            startDate: formatDate(new Date(this.state.date.getFullYear(), this.state.date.getMonth(), 1)),
            endDate: formatDate(new Date(this.state.date.getFullYear(), this.state.date.getMonth() + 1, 0))
        }, () => this.getDataEvents(this.state.startDate, this.state.endDate));

    }
    modalDetailEvent = (event) => {
        const eventApi = {
            ...event,
            date: formatDate(event.date),
            start: event.startTime,
            end: event.endTime,
            description: event.title,
            unitId: event.unit.id,
            jobId: event.job.id
        }
        this.setState({
            modalDetailEvent: !this.state.modalDetailEvent,
            idEditEvent: event.id,
            event: eventApi,
            eventDate: new Date(event.date),
        })
    }

    cancelModalDetailEvent = () => {
        this.setState({
            event: {},
            modalDetailEvent: !this.state.modalDetailEvent
        })
    }

    onNavigate = (date, view) => {
        this.setState({
            startDate: formatDate(new Date(date.getFullYear(), date.getMonth(), 1)),
            endDate: formatDate(new Date(date.getFullYear(), date.getMonth() + 1, 0))
        }, () => this.getDataEvents(this.state.startDate, this.state.endDate))
    }

    eventStyleGetter = (event, start, end, isSelected) => {
        var backgroundColor = event.id ? '#4dbd74' : '#f86c6b';
        var style = {
            backgroundColor: backgroundColor
        }
        return { style: style }
    }

    getHolidayStatus = async () => {
        try {
            const { data } = await request.get('v1/holidays/status');
            this.setState({ holidayStatus: data.data })
        } catch (err) {
            console.log(err)
            throw err;
        }
    }

    getReimburseStatus = async () => {
        try {
            const { data } = await request.get('v1/reimbursement/status');
            this.setState({ reimburseStatus: data.data })
        } catch (err) {
            console.log(err)
            throw err;
        }
    }

    getDataEvents = async (start, end) => {
        try {
            const { data } = await request.get(`v1/personnel/event?start=${start}&end=${end}`);
            const eventApi = data.data;
            let events = [];
            let event = {};
            eventApi.map(data => {
                event = {
                    id: data.id,
                    date: data.date,
                    start: new Date(`${data.date} ${data.start || '00:00:00'}`),
                    end: new Date(`${data.date} ${data.end || '00:00:00'}`),
                    startTime: data.start,
                    endTime: data.end,
                    title: data.description,
                    unit: data.unit,
                    job: data.job,
                    personnel: data.personnel,
                    company: data.company.name
                }
                return (
                    events.push(event)
                );
            })
            this.setState({ events })
        } catch (err) {
            console.log(err)
            throw err;
        }
    }

    render() {
        const { t } = this.props;
        const calendarToolbar = {
            today: t("hariini"),
            previous: <i className="fa fa-angle-left"></i>,
            next: <i className="fa fa-angle-right"></i>,
            month: t("bulanan"),
            week: t("mingguan"),
            day: t("harian")
        }
        moment.locale(t('id'))
        return (
            <div className="animated fadeIn">

                <Row className="attendance" >
                    <Alert color="primary">
                        <p>{t('keteranganiphone')}</p>
                    </Alert>
                    {this.state.user.faceId === null ? (
                        <Fragment>
                            <Col xs="12" className="text-align:center">
                                <Link to="/attendance/webcam/register">
                                    <div className="card menu-item">
                                        <div className="card-body check-in">
                                            <div className="menu-img mt-2 mb-3 text-center">
                                                <img src={require("../../../assets/assets_ari/register.png")} alt="" />
                                            </div>
                                            <div className="menu-title mb-2 text-center">
                                                <h3 className="mb-0 title-menu-company"><b>Register</b></h3>
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            </Col>
                        </Fragment>
                    ) : (
                            <Fragment>
                                <Col xs="6">
                                    <Link to="/attendance/webcam/clockin">
                                        <div className="card menu-item">
                                            <div className="card-body check-in">
                                                <div className="menu-img mt-2 mb-3 text-center">
                                                    <img src={require("../../../assets/assets_ari/check_in.png")} alt="" />
                                                </div>
                                                <div className="menu-title mb-2 text-center">
                                                    <h5 className="mb-0 title-menu-company"><b>{t('clockIn')}</b></h5>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </Col>
                                <Col xs="6">
                                    <Link to="/attendance/webcam/clockout">
                                        <div className="card menu-item">
                                            <div className="card-body check-out">
                                                <div className="menu-img mt-2 mb-3 text-center">
                                                    <img src={require("../../../assets/assets_ari/check_out.png")} alt="" />
                                                </div>
                                                <div className="menu-title mb-2 text-center">
                                                    <h5 className="mb-0 title-menu-company"><b>{t('clockOut')}</b></h5>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </Col>
                            </Fragment>
                        )
                    }
                </Row>
                <Row>
                    <Col xs="12" sm="12" md="4" lg="4">
                        <Widget01 header={this.state.user.holidayBalance || 0} mainText={t('sisaJatahCutiTahunan')}
                            button={true} buttonText={t('ajukancuti')} to={'/cuti'}
                        />
                    </Col>

                    <Col xs="12" sm="12" md="4" lg="4">
                        <Widget01 header={t('statuspengajuancuti')} >
                            <div className="d-flex align-items-center justify-content-between mb-2 pt-2">
                                <div><i className="fa fa-circle text-warning mr-1"></i> Pending</div>
                                {this.state.holidayStatus.pending + this.state.holidayStatus.approved1}
                            </div>
                            <div className="d-flex align-items-center justify-content-between mb-2">
                                <div><i className="fa fa-circle text-success mr-1"></i> {t('disetujui')}</div>
                                {this.state.holidayStatus.approved2}
                            </div>
                            <div className="d-flex align-items-center justify-content-between pb-2">
                                <div><i className="fa fa-circle text-danger mr-1"></i> {t('ditolak')}</div>
                                {this.state.holidayStatus.rejected}
                            </div>
                        </Widget01>
                    </Col>

                    <Col xs="12" sm="12" md="4" lg="4">
                        <Widget01 header={t('statuspengajuanreimburse')} >
                            <div className="d-flex align-items-center justify-content-between mb-2 pt-2">
                                <div><i className="fa fa-circle text-warning mr-1"></i> Pending</div>
                                {this.state.reimburseStatus.pending + this.state.reimburseStatus.process1 + this.state.reimburseStatus.process2}
                            </div>
                            <div className="d-flex align-items-center justify-content-between mb-2">
                                <div><i className="fa fa-circle text-success mr-1"></i> {t('ditransfer')}</div>
                                {this.state.reimburseStatus.done}
                            </div>
                            <div className="d-flex align-items-center justify-content-between pb-2">
                                <div><i className="fa fa-circle text-danger mr-1"></i> {t('ditolak')}</div>
                                {this.state.reimburseStatus.rejected}
                            </div>
                        </Widget01>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card className="dashboard-card">
                            <CardBody>
                                <Row className="mb-4">
                                    <Col xs="12">
                                        <CardTitle className="mb-0">
                                            <h4 className="mb-0">{t('kalender')}</h4>
                                        </CardTitle>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="12">
                                        <Calendar
                                            popup={true}
                                            localizer={localizer}
                                            defaultDate={this.state.date}
                                            messages={calendarToolbar}
                                            defaultView="month"
                                            views={["month", "week", "day"]}
                                            events={this.state.events}
                                            style={{ height: "100vh" }}
                                            onSelectEvent={event => this.modalDetailEvent(event)}
                                            onNavigate={this.onNavigate}
                                            eventPropGetter={(this.eventStyleGetter)}
                                        />
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                {/* Modal Box Detail Event */}
                <Modal isOpen={this.state.modalDetailEvent} toggle={this.cancelModalDetailEvent} className={this.props.className} >
                    <ModalBody>
                        {this.state.event.id ?
                            <>
                                <div className="d-flex justify-content-between align-items-center mb-0">
                                    <h4 className="content-sub-title mb-0">{this.state.event.title}</h4>
                                    <Button color="link" size="lg" className="text-danger p-0" onClick={this.cancelModalDetailEvent}><strong>&times;</strong></Button>
                                </div>
                                <div>
                                    &mdash; <span className="text-muted">{moment(this.state.event.date).format('dddd, DD MMMM YYYY')}</span>
                                    <i className="fa fa-circle text-muted ml-2 mr-2" style={{ fontSize: '6px' }}></i>
                                    <span className="text-muted">{moment(this.state.event.start, "HH:mm:ss").format("HH:mm")} {t('s/d')} {moment(this.state.event.end, "HH:mm:ss").format("HH:mm")}</span>
                                </div>
                                <div className="mt-4">
                                    <i className="fa fa-users text-muted mr-2"></i>
                                    <span className="text-muted">Peserta</span>
                                </div>
                                {this.state.event.unit.id &&
                                    <div className="ml-4">
                                        <span className="text-muted">{t('semua')} {t('karyawan')} unit {this.state.event.unit.name || ''}</span>
                                    </div>
                                }
                                {this.state.event.job.id &&
                                    <div className="ml-4">
                                        <span className="text-muted">{t('semua')} {t('karyawan')} {t('jabatan')} {this.state.event.job.name || ''}</span>
                                    </div>
                                }
                                {this.state.event.personnel.length > 0 ?
                                    this.state.event.personnel.map((data, idx) => {
                                        return (
                                            <div key={idx} className="ml-4">
                                                &mdash; <span className="text-muted">{data.name}</span>
                                            </div>
                                        )
                                    })
                                    : null
                                }
                                {this.state.event.unit.id === null && this.state.event.job.id === null && this.state.event.personnel.length <= 0 ?
                                    <div className="ml-4">
                                        <span className="text-muted">{t('semua')} {t('karyawan')} {this.state.event.company}</span>
                                    </div> : null
                                }
                            </>
                            :
                            <>
                                <div className="d-flex justify-content-between align-items-center mb-0">
                                    <h4 className="content-sub-title mb-0">{this.state.event.title}</h4>
                                    <Button color="link" size="lg" className="text-danger p-0" onClick={this.cancelModalDetailEvent}><strong>&times;</strong></Button>
                                </div>
                                <div>
                                    &mdash; <span className="text-muted">{moment(this.state.eventDate).format('dddd, DD MMMM YYYY')}</span>
                                </div>
                                <div className="mt-4">
                                    <i className="fa fa-calendar text-muted mr-2"></i>
                                    <span className="text-muted">{t('hariliburnasional')}</span>
                                </div>
                            </>
                        }
                    </ModalBody>
                </Modal>
            </div >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.token,
        user: state.user
    }
}
export default connect(mapStateToProps)(translate(DashboardUser));
