import React, {useState} from 'react';
import {Row, Col, Button, Modal} from 'reactstrap';
import {translate, t} from 'react-switch-lang';
import { Link } from 'react-router-dom';


function HomeTablet(){

    const [overtime, setOvertime] = useState(false);    

    return(
        <div className="app flex-row align-items-center background-login background-tablet2" style={overtime ? { filter: 'blur(5px)'} : undefined}>
            <div className="w-100">
                <div className="h-100">
                    <div className="text-center mx-auto mt-3" style={{width:"80%", maxHeight:"250px"}}>                        
                        <div className="welcome-logo mx-auto">
                            <h3 className="text-right">{t('selamatdatang')}</h3>
                        </div>
                    </div>
                    <div className="align-middle text-center mx-auto" style={{width:"90%", height:"auto"}}>
                        <Row className="d-flex justify-content-center">
                            <Col md="5" className="text-center my-4 mx-2">
                                <Link to="/tab/checkin">
                                    <Button className="w-100 shadow p-1" style={{backgroundColor:"#E4FAE4", borderRadius:"20px"}}>
                                        <img src={require("../../../assets/tablet/checkin.svg")} alt="" style={{height:"90px"}} className="my-2" />
                                    </Button><br />
                                </Link>
                                <h5 className="text-center mt-2">{t('absenmasuk')}</h5>
                            </Col>
                            <Col md="5" className="text-center my-4 mx-2">
                                <Link to="/tab/checkout">
                                    <Button className="w-100 shadow p-1" style={{backgroundColor:"#FFF1F0", borderRadius:"20px"}}>
                                        <img src={require("../../../assets/tablet/checkout.svg")} alt="" style={{height:"90px"}} className="my-2" />    
                                    </Button><br />
                                </Link>
                                <h5 className="text-center mt-2">{t('absenkeluar')}</h5>
                            </Col>
                            
                            <Col md="5" className="text-center my-4 mx-2">
                                
                                <Button onClick={() => setOvertime(true)} className="w-100 shadow p-1" style={{backgroundColor:"#e6f7ff", borderRadius:"20px"}}>
                                    <img src={require("../../../assets/tablet/overtime.png")} alt="" style={{height:"90px"}} className="my-2" />
                                </Button><br />
                                <h5 className="text-center mt-2">{t('lembur')}</h5>
                            </Col>
                            <Col md="5" className="text-center my-4 mx-2">
                                <Link to="/tab/registername">
                                    <Button className="w-100 shadow p-1" style={{backgroundColor:"#EAE6FF", borderRadius:"20px"}}>
                                        <img src={require("../../../assets/tablet/register.png")} alt="" style={{height:"90px"}} className="my-2" />
                                    </Button><br />
                                </Link>
                                <h5 className="text-center mt-2">{t('registrasi')}</h5>
                            </Col>
                        </Row>                        
                    </div>
                </div>
            </div>

            <Modal backdropClassName="back-home" contentClassName="modal-home shadow" style={{borderRadius:"15px", marginTop:"40vh"}} isOpen={overtime} toggle={() => setOvertime(!overtime)}>
                <Row className="d-flex justify-content-center">
                    <Col md="5" className="text-center my-4 mx-2">
                        <Link to="/tab/overtimein">
                            <Button className="w-100 shadow p-1" style={{backgroundColor:"#E4FAE4", borderRadius:"20px"}}>
                                <img src={require("../../../assets/tablet/checkin.svg")} alt="" style={{height:"90px"}} className="my-2" />
                            </Button><br />
                        </Link>
                        <h5 className="text-center mt-2">{t('Mulai Lembur')}</h5>
                    </Col>
                    <Col md="5" className="text-center my-4 mx-2">
                        <Link to="/tab/overtimeout">
                            <Button className="w-100 shadow p-1" style={{backgroundColor:"#FFF1F0", borderRadius:"20px"}}>
                                <img src={require("../../../assets/tablet/checkout.svg")} alt="" style={{height:"90px"}} className="my-2" />    
                            </Button><br />
                        </Link>
                        <h5 className="text-center mt-2">{t('Akhiri Lembur')}</h5>
                    </Col>
                </Row>
            </Modal>            
        </div>
    )
}

export default translate(HomeTablet);