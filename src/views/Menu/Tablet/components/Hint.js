import React from 'react'
import { Row, Button, Modal, ModalBody } from 'reactstrap';

export default function Hint(props) {
    return (

        <Modal isOpen={props.visible} size="xl" className="top-50" autoFocus={true}>
            <ModalBody>
                <Row>
                    <h2 className="title-upgrade" style={{ color: '#93aad6' }}>{props.title}</h2>
                    <div className="col-12 text-center">
                        <img src={require(`../../../../assets/assets_ari/${props.img}`)} alt="" width="280px" />
                    </div>
                    <div className="col-12 text-center mb-2">
                        <p style={{fontSize:"28pt"}}>{props.desc}</p>
                    </div>
                    <div className="col-12 text-center mt-2">

                        {props.onBack ?
                            <Row>
                                <div className="col-6 text-left">
                                    <Button type="submit" color="netis-color" style={{ width: '90%',fontSize:"22pt" }} onClick={props.onBack}>
                                        Kembali
                        </Button>
                                </div>
                                <div className="col-6 text-right">
                                    <Button type="submit" color="netis-color" style={{ width: '90%',fontSize:"22pt" }} onClick={props.onClick}>
                                        {props.actionTitle}
                                    </Button>
                                </div>
                            </Row>
                            :
                            <Row>
                                <div className="col-12 text-center">
                                    <Button type="submit" style={{ width: '80%',fontSize:"22pt" }} color="netis-color" onClick={props.onClick}>
                                        {props.actionTitle}
                                    </Button>
                                </div>
                            </Row>
                        }

                    </div>
                </Row>
            </ModalBody>
        </Modal>

    )
}