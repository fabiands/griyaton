import React, { Component } from 'react';
import {Offline} from 'react-detect-offline';
import {translate} from 'react-switch-lang';

class OfflineIndicator extends Component{
	render() {
		const { t } = this.props;
		return(
			 <Offline> 
          <div className="fixed-top d-flex p-2 justify-content-center bg-danger">        
            <b>Opps! &nbsp;</b>
            {t('Apptidakterhubunginternet')}
          </div>
        </Offline>
			);
	}
}

export default translate(OfflineIndicator);
