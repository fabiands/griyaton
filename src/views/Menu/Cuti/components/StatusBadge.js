import React from 'react'

export default function StatusBadge(props) {

    switch (props.status) {
        case 'pending': return <><i className="fa fa-circle text-warning mr-1"></i> </>;
        case 'approved1': return <><i className="fa fa-circle-o text-warning mr-1"></i> </>;
        case 'approved2': return <><i className="fa fa-circle text-info mr-1"></i> </>;
        case 'rejected': return <><i className="fa fa-circle text-danger mr-1"></i> </>;
        default: return props.status
    }
}
