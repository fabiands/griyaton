import React, { Component } from "react";
import { connect } from 'react-redux'
import request from '../../../utils/request'
import { Card, CardBody, Table, Row, Col, CardHeader } from "reactstrap";
import moment from '../../../utils/moment'
import StatusBadge from './components/StatusBadge'
import Loading from '../../../components/LoadingAnimation'
import DataNotFound from '../../../components/DataNotFound'
import {
  translate,
} from 'react-switch-lang';
class CutiDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      notFound: false,
      holiday: null
    };
  }

  async componentDidMount() {
    try {
      const { data } = await request.get(`v1/holidays/request/${this.props.match.params.id}`);
      this.setState({ holiday: data.data }, () => {
        const category = this.changeCategory(this.state.holiday.category);
        let data = { ...this.state.holiday };
        data['category'] = category;
        this.setState({ holiday: data })
      });
    } catch (err) {
      if (err.response && err.response.status === 404) {
        this.setState({ notFound: true });
      }
      throw err;
    } finally {
      this.setState({ loading: false });
    }
  }
  changeCategory = (data) => {
    const { t } = this.props;
    switch (data) {
      case 'tahunan': return t('tahunan');
      case 'hamil': return t('hamil');
      case 'sakit': return t('sakit');
      case 'menikah': return t('menikah');
      case 'menikahkanAnak': return t('menikahkan');
      case 'mengkhitankanAnak': return t('mengkhitankan');
      case 'membaptiskanAnak': return t('membaptiskan');
      case 'istriMelahirkan': return t('melahirkan');
      case 'istriKeguguran': return t('keguguran');
      case 'keluargaIntiMeninggal': return t('intimeninggal');
      case 'keluargaBesarMeninggal': return t('besarmeninggal');
      default: return null
    }

  }
  changeStatus = (data) => {
    const { t } = this.props;
    switch (data) {
      case 'pending': return t('diproses');
      case 'approved1': return t('menunggupersetujuanadmin');
      case 'approved2': return t('disetujui');
      case 'rejected': return t('ditolak');
      default: return null
    }
  }
  changeStatusLog = (data) => {
    const { t } = this.props;
    switch (data) {
      case 'pending': return t('diproses');
      case 'approved1': return t('menunggupersetujuanadmin');
      case 'approved2': return t('disetujui');
      case 'rejected': return t('ditolak');
      default: return null
    }
  }
  changeBadge = (data) => {
    switch (data) {
      case 'pending': return 'pending';
      case 'approved1': return 'approved1';
      case 'approved2': return 'approved2';
      case 'rejected': return 'rejected';
      default: return null
    }
  }
  render() {
    if (this.state.loading) {
      return <Loading />;
    }

    if (this.state.notFound) {
      return <DataNotFound />
    }
    const { t } = this.props;
    moment.locale(t('id'))
    const { holiday } = this.state;
    return (<div className="animated fadeIn">
      <Card>
        <CardBody>
          <Row>
            <Col md="6">
              <Table hover>
                <tbody>
                  <tr>
                    <th>{t('tanggal')} {t('mulai')}</th><td>{moment(holiday.start).format('DD MMMM YYYY')}</td>
                  </tr>
                  <tr>
                    <th>{t('tanggal')} {t('selesai')}</th><td>{moment(holiday.end).format('DD MMMM YYYY')}</td>
                  </tr>
                  <tr>
                    <th>{t('kategori')}</th><td>{holiday.category}</td>
                  </tr>
                  <tr>
                    <th>{t('deskripsi')}</th><td>{holiday.message}</td>
                  </tr>
                  <tr>
                    <th>Status</th><td><StatusBadge status={this.changeBadge(holiday.status)} />{this.changeStatus(holiday.status)}</td>
                  </tr>
                </tbody>
              </Table>
            </Col>
            <Col md="6">
              {holiday.document !== null ?
                <img src={"" + holiday.document} width="100%" alt="document" />
                : null}
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          {t('riwayatstatus')}
        </CardHeader>
        <CardBody>
          <Table>
            {holiday.status_log && holiday.status_log.map((statusLog, index) => {
              let status = this.changeStatusLog(statusLog.status);
              let badge = this.changeBadge(statusLog.status);
              return (
                <tr key={index}>
                  <td style={{ width: 170 }}>
                    <strong>{(statusLog.subject && statusLog.subject.nickName) || 'system'}</strong><br />
                    <span className="text-muted">{moment(holiday.created).format('DD MMMM YYYY HH:mm')}</span>
                  </td>
                  <td>
                    <strong><StatusBadge status={badge} />{status}</strong>
                    <br /> {statusLog.message}
                  </td>
                </tr>
              )
            })}
          </Table>
        </CardBody>
      </Card>
    </div>);
  }
}

const mapStateToProps = (state) => ({
  sessionId: state.sessionId
});

export default connect(mapStateToProps)(translate(CutiDetail));
