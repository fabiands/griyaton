import React from "react";
import { Table, Button, Modal, ModalBody, Spinner } from "reactstrap";
import { useEffect, useState } from "react";
import request from "../../../utils/request";
import { Link } from "react-router-dom";
import LoadingAnimation from "../../../components/LoadingAnimation";
import * as moment from 'moment';
import { toast } from 'react-toastify';
import { t, translate } from 'react-switch-lang';
import { useUserPrivileges } from "../../../store";

toast.configure();
function ManageAssessmentResult(props) {
    const [loading, setLoading] = useState(true);
    const [personnelResults, setPersonnelResults] = useState([]);
    const [deletingId, setDeletingId] = useState(null);
    const [deleteLoading, setDeleteLoading] = useState(false);
    const { can } = useUserPrivileges()
    useEffect(() => {
        setLoading(true);
        request.get('v1/assessment/results').then(res => {
            setPersonnelResults(res.data.data);
        }).finally(() => {
            setLoading(false);
        });
    }, []);

    if (loading) {
        return <div className="animated fadeIn">
            <LoadingAnimation />
        </div>
    }

    function doDelete() {
        if (deleteLoading) return;
        setDeleteLoading(true);
        request.delete('v1/assessment/results/' + deletingId).then(() => {
            setPersonnelResults(personnelResults.filter(r => r.id !== deletingId));
            setDeletingId(null);
            toast.success('Berhasil dihapus.');
        })
            .catch(err => {
                toast.error('Terjadi kesalahan');
                throw err;
            })
            .finally(() => {
                setDeleteLoading(false);
            });
    }

    return <div className="animated fadeIn">

        <Table responsive>
            <thead>
                <tr>
                    <th className="text-center w-5">No.</th>
                    <th className="text-center">{t('nama')}</th>
                    <th className="text-center">{t('jenistes')}</th>
                    <th className="text-center">{t('hasil')}</th>
                    <th className="text-left">{t('tanggaltes')}</th>
                    <th className="text-center"></th>
                    </tr>
            </thead>
            <tbody>
                {personnelResults.map((r, idx) => <tr>
                    <td>{idx + 1}</td>
                    <td>
                        <Link to={"/personnels/" + r.personnelId} className={`personnel-name${can('read-employee') ? '' : ' disabled'}`}>
                            {r.userFullName}
                        </Link>
                    </td>
                    <td className="text-center">{r.testName}</td>
                    {/* <td className="text-center">{r.result?.toUpperCase()}</td> */}
                    {  r.testName !== 'disc' ?
                        <td>{r.result?.toUpperCase()}</td>
                        :
                        (() => {
                            const parse = JSON.parse(r.result)
                            const arrResult = Object.values(parse).flat()
                            const setResult = Array.from(new Set(arrResult)).join(", ")
                            return(
                                <td>
                                    {setResult}
                                </td>
                            )
                        })()
                    }
                    <td className="text-left text-nowrap">
                        {moment(r.created_at).format('DD MMM YYYY')}<br />
                        ({moment(r.created_at).format('HH:mm')})
                    </td>
                    <td className="text-nowrap">
                        <Link to={'/assessment/results/' + r.id}>
                            <Button color="netis-color" className={`${can('read-assessment-result') ? '' : ' d-none'}`}>{t('lihatdetail')}</Button>
                        </Link>
                        <Button className={`${can('delete-assessment-result') ? '' : ' d-none'}`} color="netis-danger ml-2" onClick={() => setDeletingId(r.id)}><i className="fa fa-trash-o"></i></Button>
                    </td>
                </tr>)}
            </tbody>
        </Table>
        <Modal isOpen={!!deletingId} toggle={() => {
            if (!deleteLoading) {
                setDeletingId(null)
            }
        }}>
            <ModalBody>
                <h6>{t('yakinmenghapus')}</h6>
                <div className="d-flex justify-content-end">
                    {!deleteLoading && <Button className="mr-2" color="light" onClick={() => setDeletingId(null)}>{t('batal')}</Button>}
                    <Button color="netis-primary" onClick={doDelete} disabled={deleteLoading}>
                        {deleteLoading ? <React.Fragment><Spinner size="sm" color="light" /> menghapus...</React.Fragment> : t('hapus')}
                    </Button>
                </div>
            </ModalBody>
        </Modal>
    </div>
}

export default translate(ManageAssessmentResult);
