import React from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Switch
  // withRouter,
  // Route,
} from "react-router-dom";
import "./App.scss";
import { Provider } from "react-redux";

import AuthRoute from "./components/AuthRoute.js";
import OfflineIndicator from "./components/OfflineIndicator.js";

//Route
import LoginPage from "./views/Pages/Login/Login.js";
import ForgotPassword from "./views/Pages/Password/ForgotPassword.js";
import ResetPassword from "./views/Pages/Password/ResetPassword.js";
// import Containers from "./containers/DefaultLayout/index.js";
import AttendanceWebcamRegister from "./views/Menu/AttendanceUser/Register.js";
import AttendanceWebcamCheckIn from "./views/Menu/AttendanceUser/CheckIn.js";
import AttendanceWebcamCheckOut from "./views/Menu/AttendanceUser/CheckOut.js";
// import AttendanceHistory from './views/Menu/Attendance/History/AttendanceHistory.js';
// import AttendanceRecap from './views/Menu/Attendance/Recapitulation/AttendanceRecap.js';
// import AttendanceRecapDetail from './views/Menu/Attendance/Recapitulation/AttendanceRecapDetail.js';
import RegisterPage from "./views/Pages/Register/Register.js";
// import VerifRegisterPage from "./views/Pages/Register/Verif.js";
import store from './store'
// import LandingPage from "./views/LandingPage/LandingPage";
import Layout from "./containers/DefaultLayout/Layout";
// import ContactUs from "./views/Pages/ContactUs/ContactUs";
// import Services from "./views/Pages/Services/Services";
// import AboutPage from "./views/Pages/About/AboutPage";
// import CareerPage from "./views/Pages/Career/CareerPage";
// import CareerNotFound from "./views/Pages/Career/CareerNotFound";
// import HRISPage from "./views/Pages/Products/HRISPage";
// import { Component } from "react";
// import FAQPage from "./views/Pages/FAQ/FAQPage";

// Tablet Page View
import HomeTablet from "./views/Menu/Tablet/Home";
import PresenseIn from "./views/Menu/Tablet/PresenseIn";
import PresenseOut from "./views/Menu/Tablet/PresenseOut";
import InputPin from "./views/Menu/Tablet/InputPin";
import RegisterFace from "./views/Menu/Tablet/RegisterFace";
import RegisterName from "./views/Menu/Tablet/RegisterName";
import RegisterPin from "./views/Menu/Tablet/RegisterPin";
import ConfirmPin from "./views/Menu/Tablet/ConfirmPin";
import OvertimeIn from "./views/Menu/Tablet/OvertimeIn";
import OvertimeOut from "./views/Menu/Tablet/OvertimeOut";
import OvertimePinIn from "./views/Menu/Tablet/OvertimePinIn";
import OvertimePinOut from "./views/Menu/Tablet/OvertimePinOut";

import OvertimeWebcamStart from "./views/Menu/Overtime/OvertimeWebcamStart";
import OvertimeWebcamEnd from "./views/Menu/Overtime/OvertimeWebcamEnd";
import langUtils from "./utils/language/index";
import { setLocale as setValidationLocale } from "yup";
import validationLocaleID from "./utils/yup/locales/id";
import validationLocaleDefault from "yup/es/locale";
import * as moment from "moment";

// const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

langUtils.onLanguageChanged((lang) => {
  if (lang.toLowerCase() === "id") {
    setValidationLocale(validationLocaleID);
  } else {
    setValidationLocale(validationLocaleDefault);
  }
});
langUtils.onLanguageChanged((lang) => {
  moment.locale(lang.toLowerCase());
});
langUtils.init();

// class LandingPageRouteComponent extends Component {
//   componentDidUpdate(prevProps) {
//     if (
//       this.props.path === this.props.location.pathname &&
//       this.props.location.pathname !== prevProps.location.pathname
//     ) {
//       window.scrollTo(0, 0);
//     }
//   }

//   render() {
//     return <Route {...this.props} />;
//   }
// }
// const LandingPageRoute = withRouter(LandingPageRouteComponent);

export default function App() {
  return (
    <Provider store={store}>
      <OfflineIndicator />

      <Router>
        <Switch>
          <Redirect from="/" to="/login" exact />
          <AuthRoute path="/login" type="guest" exact>
            <LoginPage />
          </AuthRoute>
          <AuthRoute path="/register" type="guest" exact>
            <RegisterPage />
          </AuthRoute>
          {/* Tablet Route */}
          <AuthRoute path="/tab" exact component={HomeTablet} />
          <AuthRoute path="/tab/checkin" exact component={PresenseIn} />
          <AuthRoute path="/tab/checkout" exact component={PresenseOut} />
          <AuthRoute path="/tab/inputpin" exact component={InputPin} />
          <AuthRoute path="/tab/registerface" exact component={RegisterFace} />
          <AuthRoute path="/tab/registername" exact component={RegisterName} />
          <AuthRoute path="/tab/registerpin" exact component={RegisterPin} />
          <AuthRoute path="/tab/confirmpin" exact component={ConfirmPin} />
          <AuthRoute path="/tab/overtimein" exact component={OvertimeIn} />
          <AuthRoute path="/tab/overtimeout" exact component={OvertimeOut} />
          <AuthRoute path="/tab/overtimepinin" exact component={OvertimePinIn} />
          <AuthRoute path="/tab/overtimepinout" exact component={OvertimePinOut} />

          <AuthRoute
            path="/overtimes/webcam/start"
            exact
            type="private"
            component={OvertimeWebcamStart}
          />
          <AuthRoute
            path="/overtimes/webcam/end"
            exact
            type="private"
            component={OvertimeWebcamEnd}
          />
          {/* <AuthRoute
            path="/verify/:token"
            type="guest"
            exact
            component={VerifRegisterPage}
          ></AuthRoute> */}

          <AuthRoute
            path="/attendance/webcam/register"
            type="private"
            exact
            component={AttendanceWebcamRegister}
          />
          <AuthRoute
            path="/attendance/webcam/clockin"
            type="private"
            exact
            component={AttendanceWebcamCheckIn}
          />
          <AuthRoute
            path="/attendance/webcam/clockout"
            type="private"
            exact
            component={AttendanceWebcamCheckOut}
          />

          <AuthRoute
            path="/forgot"
            type="guest"
            exact
            component={ForgotPassword}
          ></AuthRoute>
          <AuthRoute
            path="/reset/:token"
            type="guest"
            exact
            component={ResetPassword}
          ></AuthRoute>

          <AuthRoute type="private" exact component={Layout} />
        </Switch>
      </Router>
    </Provider>
  );
}
