import React, { Fragment } from 'react'
import { translate } from "react-switch-lang";
import { useState } from 'react';
import { useEffect } from 'react';
import Webcam from 'react-webcam';
import { useRef } from 'react';
import Colors from '../../../utils/Colors';
import CaptureLoading from '../AttendanceUser/components/CaptureLoading';
import { Modal, ModalBody, Row, Button } from 'reactstrap';
import PropTypes from 'prop-types'

function CustomWebcam({ onCaptured, t }) {

    const [loading, setLoading] = useState(false);
    const [position, setPosition] = useState({ latitude: null, longitude: null });
    const [isSupport, setSupport] = useState({ camera: null, location: null });
    const [isAllowed, setAllowed] = useState({ camera: null, location: null });
    const [result, setResult] = useState({
        isSuccess: null,
        title: null,
        message: null,
        onDone: null,
        previewImage: null,
    });
    const [dataPhoto, setDataPhoto] = useState(null);
    const webcamRef = useRef(null);

    useEffect(() => {
        navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
        if (! (navigator.getUserMedia || navigator.mediaDevices.getUserMedia)) {
            setSupport((support) => ({...support, camera: false}))
        }

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                position => {
                    setAllowed((allows) => ({...allows, location: true }))
                    setPosition({latitude: position.coords.latitude, longitude: position.coords.longitude});
                },
                () => setAllowed((allows) => ({...allows, location: false }))
            );
        } else {
            setSupport((support) => ({...support, location: false}))
        }

    }, []);

    function capture() {
        const imageUri = webcamRef.current.getScreenshot();
        setDataPhoto(imageUri);
        setLoading(true);
        const reset = () => {
            setResult({isSuccess: null, title: null, message: null, onDone: null, previewImage: null})
            setDataPhoto(null);
        };
        const setError = (params) => setResult((res) => ({...res, ...params, isSuccess: false, title: t('gagal'), onDone: () => reset()}))
        onCaptured({ imageUri: imageUri, position },{ setLoading, setResult, setError, reset })
    }

    return (
        <div>
            <div className="margin-30 d-flex justify-content-center align-items-center" style={{ height: '100vh', width: '100vw' }}>
                {
                    // Show Browser/Device Not Support.
                    isSupport.camera === false || isSupport.location === false ?
                    <div className="text-center">
                        <i className="fa fa-2x fa-warning"></i><br />
                        <h2 className="h5 text-danger">Maaf, browser ini tidak mendukung fitur camera atau lokasi</h2>
                        <h2 className="h6 text-netis-primary">Silahkan menggunakan browser atau <i>device</i> yang mendukung.</h2>
                    </div> :

                    // Show Camera
                    <Fragment>
                        {loading && <CaptureLoading title="Loading" visible={true} />}
                        {result?.isSuccess !== null && 
                            <AfirmationModal 
                                visible={true}
                                title={result?.title || 'Sukses'}
                                text={result?.message}
                                onPressButton={result?.onDone}
                                titleButton={t('Oke')}
                                titleColor={Colors.white}
                                bgColorButton={Colors.primaryColor}
                                image={result?.previewImage}
                            />
                        }
                        {dataPhoto ? <img src={dataPhoto} width="95%" style={{ border: 'solid 5px #63c2de', boxSizing: 'content-box'}} alt="screenshot" /> : 
                        <Webcam
                            audio={false}
                            ref={webcamRef}
                            screenshotFormat="image/jpeg"
                            forceScreenshotSourceSize={true}
                            mirrored={true}
                            style={{ height: '100%', width: '100vw' }}
                            videoConstraints={{ facingMode: 'user' }}
                            onUserMedia={() => setAllowed((allows) => ({...allows, camera: true }))}
                            onUserMediaError={() => setAllowed((allows) => ({...allows, camera: false }))}
                        />}
                        
                        {isAllowed.location && isAllowed.camera ?
                            <button className="button-capture" onClick={capture} />
                            :
                            <div className="border-warning" style={{ zIndex: 1000, position: 'absolute', left: '1rem', right: '1rem', marginLeft: 'auto', marginRight: 'auto', textAlign: 'center', bottom: 30, padding: '2rem', background: '#fff', borderRadius: '50rem', borderWidth: 5, borderStyle: 'solid'}}>
                                {!(isAllowed.camera && isAllowed.location) && <h6 className="mb-0">Anda perlu mengaktifkan kamera dan lokasi untuk melanjutkan</h6>}
                            </div>
                        }
                    </Fragment>
                }
            </div>
        </div>
    )
}

function AfirmationModal(props) {
    return (
        <Modal isOpen={props.visible} className="top-50" autoFocus={true}>
            <ModalBody>
                <Row>
                    <div className="col-12 text-center">
                        <h2 className="title-upgrade" style={{ color: '#93aad6' }}>{props.title}</h2>
                    </div>
                    {props.image && <div className="col-12 text-center mb-3">
                        <img src={props.image} alt="screenshot" width="100%" />
                    </div>}
                    <div className="col-12 text-center">
                        <p>{props.text}</p>
                    </div>
                    <div className="col-12 text-center">
                        <Row>
                            <div className="col-12 text-center">
                                <Button type="submit" className="btn btn-info" style={{ color: props.titleColor }} onClick={props.onPressButton}>
                                    {props.titleButton}
                                </Button>
                            </div>
                        </Row>
                    </div>
                </Row>
            </ModalBody>
        </Modal>
    )
}
CustomWebcam.propTypes = {
    onCaptured: PropTypes.func.isRequired
}

export default translate(CustomWebcam);