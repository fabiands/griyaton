import React from 'react'
import { translate, t } from 'react-switch-lang';

function StatusBadge({ status, asUser = true}) {
    switch (status) {
        case 'pending': return <><i className="fa fa-circle text-warning mr-1"></i> Pending</>;
        case 'approved1': return <><i className="fa fa-circle text-info mr-1"></i> {t(asUser?'menunggu persetujuan admin':'disetujuiatasan')}</>;
        case 'approved2': return <><i className="fa fa-circle text-success mr-1"></i> {t('disetujui')}</>;
        case 'done': return <><i className="fa fa-check-circle text-success mr-1"></i> {t('selesai')}</>;
        case 'rejected': return <><i className="fa fa-circle text-danger mr-1"></i> {t('ditolak')}</>;
        default: return status
    }
}

export default translate(StatusBadge)
