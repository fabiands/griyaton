import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Button, Modal, Form, Row, ModalBody, ModalFooter, ModalHeader, FormGroup, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Spinner, Label, Input, Col } from 'reactstrap';
import { toast } from 'react-toastify';
import { DatePickerInput } from 'rc-datepicker';
import StatusBadge from './components/StatusBadge'
import request from '../../../utils/request'
import { formatDate } from '../../../utils/formatter';
import { convertToRupiah } from '../../../utils/formatter'
import moment from '../../../utils/moment'
import { Link } from 'react-router-dom';
import LoadingAnimation from '../../../components/LoadingAnimation'
import DataNotFound from '../../../components/DataNotFound'
import { isPdf } from './components/helper'
import {
    translate,
} from 'react-switch-lang';
toast.configure()

const PANEL_ADMIN = '2';

class ReimburseApproval extends Component {

    constructor(props) {
        super(props);

        const searchParams = new URLSearchParams(props.location.search);

        this.isPdf = isPdf
        this.state = {
            loading: true,
            reimbursements: [],
            showModal: false,
            updating: false,
            update: {
                reimburseId: null,
                status: '',
                message: '',
            },
            dropdownOpened: {},
            showDocument: false,
            user: props.user,
            filter: {
                start: searchParams.get('start'),
                end: searchParams.get('end')
            },
        };
    }

    fetchData = async () => {
        const params = Object.keys(this.state.filter)
            .filter(k => this.state.filter[k] !== null)
            .reduce((acc, k) => ({ ...acc, [k]: formatDate(this.state.filter[k]) }), {});

        try {
            this.setState({ loading: true })
            const menu = this.props.menu;
            const url = menu === PANEL_ADMIN ? 'v1/reimbursement/admin/request' : 'v1/reimbursement/request';
            const { data } = await request.get(url, { params });
            this.setState({ reimbursements: data.data })
        } catch (err) {
            if (err.response) {
                toast.error(err.message, { autoClose: 2000 });
            }
            console.log(err)
            throw err;
        } finally {
            this.setState({ loading: false })
        }
    }

    async componentDidMount() {
        this.fetchData()
    }

    handleChangeFilter = (name, date) => {
        let filter = { ...this.state.filter }
        filter[name] = date
        this.setState({ filter })
    }


    toggleModal = () => {
        const closing = this.state.showModal;
        this.setState({ showModal: !this.state.showModal })
        if (closing) {
            this.setState({ update: { status: '', message: '', reimburseId: null } })
        }
    }

    updateStatus = (reimburseId, status) => (e) => {
        const update = { ...this.state.update }
        update.status = status
        update.reimburseId = reimburseId
        this.setState({ update })
        this.toggleModal()
    }

    submitUpdateStatus = async (e) => {
        e.preventDefault();
        this.setState({ updating: true })
        const { reimburseId, status, message } = this.state.update
        try {
            const { data } = await request.post(`v1/reimbursement/request/${reimburseId}`, { status, message })
            this.setState(state => {
                const updatedReimburseId = state.reimbursements.findIndex(reimburse => reimburse.id === data.data.id);
                const reimbursements = [...state.reimbursements];
                reimbursements[updatedReimburseId].status = data.data.status;
                return { reimbursements };
            })
            toast.success(data.message || 'Success')
            this.toggleModal();
        } catch (err) {
            if (err.response && err.response.data) {
                toast.error(err.response.data.message);
            }
            throw err;
        } finally {
            this.setState({ updating: false })
        }
    }

    toggleDropdown = (index) => (e) => {
        let dropdownOpened = { ...this.state.dropdownOpened }
        dropdownOpened[index] = !dropdownOpened[index]
        this.setState({ dropdownOpened })
    }

    showDocument = (document) => {
        this.setState({ showDocument: document })
    }

    toggleShowDocument = () => {
        this.setState({ showDocument: false })
    }

    handleInputChange = (e) => {
        const update = { ...this.state.update }
        update[e.target.name] = e.target.value
        this.setState({ update })
    }

    render() {
        const { t } = this.props;
        const { privileges: userPrivileges = [] } = this.props.user;
        return (
            <div className="animated fadeIn md-company-header mb-3">
                <div className="d-flex align-items-center mb-4">
                    <Form onSubmit={(e) => { e.preventDefault(); this.fetchData() }}>
                        <Row>
                            <Col sm="5">
                                <Label tag="small" htmlFor="start" className="text-muted">{t('dari')}</Label>
                                <br />
                                <DatePickerInput
                                    readOnly
                                    showOnInputClick={true}
                                    name="start"
                                    id="start"
                                    onChange={(value) => this.handleChangeFilter('start', value)}
                                    value={this.state.filter.start}
                                    className='my-custom-datepicker-component'
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                />
                            </Col>
                            <Col sm="5">
                                <Label tag="small" htmlFor="end" className="text-muted">{t('hingga')}</Label>
                                <br />
                                <DatePickerInput
                                    readOnly
                                    showOnInputClick={true}
                                    name="end"
                                    id="end"
                                    onChange={(value) => this.handleChangeFilter('end', value)}
                                    value={this.state.filter.end}
                                    className='my-custom-datepicker-component'
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                />
                            </Col>
                            <Col sm="2">
                                <div className="pt-sm-0 my-2">
                                    <Button type="submit" className="mt-sm-4" color="netis-color" style={{ width: '60px' }} disabled={this.state.loading}>
                                        {this.state.loading ? <Spinner color="light" size="sm" /> : t('cari')}
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </div>

                {this.state.loading ? <LoadingAnimation />
                    : !this.state.reimbursements.length ? <DataNotFound />
                        : (<Table responsive>
                            <thead>
                                <tr>
                                    <th className="text-center w-5">No.</th>
                                    <th className="w-20">{t('nama')}</th>
                                    <th>{t('deskripsi')}</th>
                                    <th className="w-15">Nominal</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>{this.state.reimbursements.map((data, index) => (
                                <tr key={index}>
                                    <td className="text-center">{index + 1}</td>
                                    <td>
                                        {data.personnel.user.fullName} <br />
                                &mdash; <span className="text-muted">{moment(data.datetime).format('DD MMM YYYY')}</span>
                                    </td>
                                    <td>
                                        {this.isPdf(data.document) ?
                                            <Button href={data.document} target="_blank" rel="noopener noreferrer" color="link"><i className="fa fa-file-pdf-o"></i> {t('bukalampiran')}</Button> :
                                            <><Button color="link" onClick={() => this.showDocument(data.document)}><i className="fa fa-file-picture-o"></i></Button> {data.description}</>
                                        }
                                    </td>
                                    <td className="text-nowrap">{convertToRupiah(data.nominal)}</td>
                                    <td className="text-nowrap"><StatusBadge status={data.status} /></td>
                                    <td className="text-nowrap">
                                        {!['done', 'rejected'].includes(data.status) && (<Dropdown className="d-inline-block" isOpen={this.state.dropdownOpened[index]} toggle={this.toggleDropdown(index)}>
                                            <DropdownToggle className="mr-1" color="netis-color" caret>
                                                {t('ubah')} Status
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                {(data.status === 'pending' || (userPrivileges.includes('verify-reimburse') && data.status === 'process1')) && (<DropdownItem onClick={this.updateStatus(data.id, 'process')}>
                                                    <i className="fa fa-circle text-info"></i> {t('diproses')}
                                                </DropdownItem>)}
                                                {userPrivileges.includes('verify-reimburse') && (<DropdownItem onClick={this.updateStatus(data.id, 'done')}>
                                                    <i className="fa fa-circle text-success"></i> {t('ditransfer')}
                                                </DropdownItem>)}
                                                <DropdownItem onClick={this.updateStatus(data.id, 'rejected')}>
                                                    <i className="fa fa-circle text-danger"></i> {t('ditolak')}
                                                </DropdownItem>
                                            </DropdownMenu>
                                        </Dropdown>)}
                                        <Link to={'/reimburse/detail/' + data.id}>
                                            <Button color="netis-color">{t('detail')}</Button>
                                        </Link>
                                    </td>
                                </tr>
                            ))}</tbody>
                        </Table>)}

                <Modal isOpen={!!this.state.showDocument} toggle={this.toggleShowDocument}>
                    <ModalBody>
                        <img src={this.state.showDocument} width="100%" alt="document" />
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
                    <form onSubmit={this.submitUpdateStatus}>
                        <ModalHeader toggle={this.toggleModal}>Update Status</ModalHeader>
                        <ModalBody>
                            <FormGroup row>
                                <Label htmlFor="status" className="text-md-right" md="3">Status</Label>
                                <Col md={9}>
                                    <Input type="select" name="status" id="status" value={this.state.update.status} onChange={this.handleInputChange}>
                                        <option value="process">{t('diproses')}</option>
                                        <option value="rejected">{t('ditolak')}</option>
                                        <option value="done">{t('ditransfer')}</option>
                                    </Input>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label htmlFor="message" className="text-md-right" md={3}>{t('pesan')}</Label>
                                <Col md={9}>
                                    <Input type="textarea" name="message" id="message" rows="2" value={this.state.update.message} onChange={this.handleInputChange}></Input>
                                </Col>
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            {/*  */}
                            <Button type="submit" color="netis-color" disabled={this.state.updating}>
                                {this.state.updating ?
                                    (<><Spinner size="sm" color="light"></Spinner> Loading</>) :
                                    (<><i className="fa fa-check mr-2"></i> Update Status</>)
                                }
                            </Button>
                        </ModalFooter>
                    </form>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = (reduxState) => ({ user: reduxState.user, menu: reduxState.menu })

export default connect(mapStateToProps)(translate(ReimburseApproval));
