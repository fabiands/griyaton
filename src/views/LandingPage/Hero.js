import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Hero extends Component {
    render() {
        return (
            <section className="hero hero-image">

                <div className="container">
                    <div className="hero-content mx-auto mx-sm-0">
                        <h1 className="mb-4" style={{ lineHeight: 1.4 }}>Permudah Sistem Presensi<br /> dengan <span style={{ color: '#1472BB' }}>Teknologi Pengenalan Wajah</span></h1>
                        {/* <div className="d-sm-none" style={{ height: '1rem'}}></div> */}
                        <div className="text-left">
                        <p style={{ marginBottom: '0.5rem'}}>Perusahaanmu butuh sistem presensi yang bisa dilakukan dimana aja?</p>
                        <p style={{ marginBottom: '0.5rem'}}>Pengen pengurusan cuti dapat diakses melalui gadget?</p>
                        <p style={{ marginBottom: '0.5rem'}}>Ganti cara presensi dengan metode <strong style={{ fontSize: '120%' }}>#AbsenTanpaRibet</strong></p>
                        </div>
                        <div className="text-center text-sm-left">
                            <Link className="btn btn-netis-primary text-uppercase rounded-12 btn-hero-cta" to="/login">
                                Coba Gratis
                                <img height="14" src={require('../../assets/img/arrow-right.svg')} alt=""/>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Hero;
