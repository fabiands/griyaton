import React, { useState, useEffect, useMemo, Fragment, useRef } from "react";
import {
  Button,
  Card,
  CardHeader,
  Spinner,
  CardBody,
  Row,
  Col,
  Progress,
  Modal,
  ModalBody,
  ModalHeader,
  ListGroup,
  ListGroupItem,
} from "reactstrap";
import { TabContent, TabPane, Nav, NavItem, NavLink, Table } from "reactstrap";
import LoadingAnimation from "../../../../components/LoadingAnimation";
import DataNotFound from "../../../../components/DataNotFound";
import request from "../../../../utils/request";
import StatusBadge from "./StatusBadge";
import classnames from "classnames";
import { toast } from "react-toastify";
import { t, translate } from "react-switch-lang";
import StarRatingComponent from "react-star-rating-component";
import moment from "moment";
import ReactMarkdown from "react-markdown";
import { requestDownload } from "../../../../utils/request";
import { useUserPrivileges } from "../../../../store";
import "./videoasesmen.css";
import Star from "../../../../components/Star.js";
import { Responsive } from "responsive-react";
import { Radar } from "react-chartjs-2";

function ApplicantDetail({ match, history }) {
  const [applicant, setApplicant] = useState(null);
  const [notFound, setNotFound] = useState(false);
  const [loading, setLoading] = useState(true);
  const { can } = useUserPrivileges();

  useEffect(() => {
    request
      .get(`v1/recruitment/applicants/${match.params.applicantId}`)
      .then((res) => {
        setApplicant(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 404) {
          setNotFound(true);
        }
      })
      .finally(() => setLoading(false));
  }, [match.params.applicantId]);

  const UbahStatus = (stat) => {
    request
      .put(`/recruitment/applicants/${match.params.applicantId}`, {
        status: stat,
      })
      .then((res) => {
        toast.success(t("statuspelamarberhasildiubah"));
        history.goBack();
      })
      .catch((err) => {
        toast.error(t("statuspelamargagaldiubah"));
        throw err;
      });
  };

  const [activeTab, setActiveTab] = useState("1");
  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  if (loading) {
    return <LoadingAnimation />;
  } else if (notFound) {
    return <DataNotFound />;
  }

  return (
    <div className="animated fadeIn">
      <div className="d-flex bd-highlight mb-3">
        <div className="mr-auto bd-highlight">
          <Button onClick={() => history.goBack()} color="netis-color">
            <i className="fa fa-chevron-left mr-1"></i>
            {t("kembali")}
          </Button>
        </div>
        {(function () {
          if (applicant.status === "pending") {
            return (
              <div className="d-flex bd-highlight mb-3">
                <div className="p-2 bd-highlight">
                  <Button
                    color="netis-color"
                    className={`${
                      can("verify-job-applicant") ? "" : " d-none"
                    }`}
                    onClick={() => {
                      UbahStatus("accepted");
                    }}
                  >
                    <i className="fa fa-check mr-1"></i> {t("sesuai")}
                  </Button>
                </div>
                <div className="p-2 bd-highlight">
                  <Button
                    color="netis-danger"
                    className={`${
                      can("verify-job-applicant") ? "" : " d-none"
                    }`}
                    onClick={() => {
                      UbahStatus("rejected");
                    }}
                  >
                    <i className="fa fa-times mr-1"></i> {t("tidaksesuai")}
                  </Button>
                </div>
              </div>
            );
          }
        })()}
      </div>

      <Nav tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === "1" })}
            onClick={() => {
              toggle("1");
            }}
          >
            {t("Profil")}
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === "2" })}
            onClick={() => {
              toggle("2");
            }}
          >
            Resume
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === "3" })}
            onClick={() => {
              toggle("3");
            }}
          >
            {t("Asesmen")}
          </NavLink>
        </NavItem>
      </Nav>

      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
          <Row>
            <Col sm="12">
              <ApplicantProfile data={applicant} />
            </Col>
          </Row>
        </TabPane>

        <TabPane tabId="2">
          <Row>
            <Col sm="12">
              <ApplicantResume data={applicant} />
            </Col>
          </Row>
        </TabPane>

        <TabPane tabId="3">
          <Row>
            <Col sm="12">
              <ApplicantAssesment data={applicant} />
            </Col>
          </Row>
        </TabPane>
      </TabContent>
    </div>
  );
}

const ApplicantProfile = ({ data: applicant }) => {
  const { detail, job_vacancy: job } = applicant;

  return (
    <Card>
      <CardBody>
        <Row>
          <div className="col-12 d-flex justify-content-between align-items-center">
            <h5 className="text-uppercase content-sub-title mb-0">
              {job.name}
            </h5>
            <StatusBadge status={applicant.status} />
          </div>
          <div className="col-12 d-flex mb-3">
            <i>{job.type}</i>
          </div>
        </Row>
        <Row>
          <Col sm="6">
            <Table>
              <tbody>
                <tr>
                  <td className="col-1">{t("namalengkap")}</td>
                  <td className="col-1 text-right">:</td>
                  <td className="col-1 text-left ml-2">{detail.fullName}</td>
                </tr>
                <tr>
                  <td className="col-1">{t("namapanggilan")}</td>
                  <td className="col-1 text-right">:</td>
                  <td className="col-1 text-left ml-2">{detail.nickName}</td>
                </tr>
                <tr>
                  <td className="col-1">Email</td>
                  <td className="col-1 text-right">:</td>
                  <td className="col-1 text-left ml-2">{detail.email}</td>
                </tr>
                <tr>
                  <td className="col-1">{t("notelpon")}</td>
                  <td className="col-1 text-right">:</td>
                  <td className="col-1 text-left ml-2">{detail.phone}</td>
                </tr>
                <tr>
                  <td className="col-1">{t("jk")}</td>
                  <td className="col-1 text-right">:</td>
                  <td className="col-1 text-left ml-2">
                    {detail.gender?.name ? t(detail.gender.name) : ""}
                  </td>
                </tr>
                <tr>
                  <td className="col-1">{t("statuspernikahan")}</td>
                  <td className="col-1 text-right">:</td>
                  <td className="col-1 text-left ml-2">
                    {detail.maritalStatus?.name ?? "-"}
                  </td>
                </tr>
                <tr>
                  <td className="col-1">{t("alamat")}</td>
                  <td className="col-1 text-right">:</td>
                  <td className="col-1 text-left ml-2">
                    {[
                      detail.currentAddress.city?.name,
                      detail.currentAddress.province?.name,
                      detail.currentAddress.country?.name,
                    ]
                      .filter(Boolean)
                      .join(", ")}
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
          <Col sm="6" className="justify-content-center">
            {
              // eslint-disable-next-line
              <img
                src={detail.avatar}
                alt="No Photo"
                width="100%"
                height="100%"
              />
            }
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

const ApplicantResume = ({ data: applicant }) => {
  const {
    detail: { dataEducation, dataWorkingHistory, dataSkill },
  } = applicant;

  const [
    // eslint-disable-next-line
    downloadingProfile,
    setdownloadingProfile,
  ] = useState(false);
  const [downloadProfileLoading, setdownloadProfileLoading] = useState(false);

  function downloadProfile() {
    if (!setdownloadingProfile) {
      return;
    }
    setdownloadProfileLoading(true);
    requestDownload(
      `v1/recruitment/applicants/download-profile?email=${applicant.userEmail}`
    ).finally(() => {
      setdownloadProfileLoading(false);
    });
  }
  return (
    <Card>
      <CardHeader>
        <div className="bd-highlight pull-right">
          <Button
            onClick={downloadProfile}
            color="netis-color"
            disabled={downloadProfileLoading}
          >
            {downloadProfileLoading ? (
              <Fragment>
                <Spinner size="sm" /> Downloading...
              </Fragment>
            ) : (
              <Fragment>
                <i className="fa fa-download mr-2"></i> Download PDF
              </Fragment>
            )}
          </Button>
        </div>
      </CardHeader>
      <CardBody>
        <Row className="md-company-header mb-3 mt-2">
          <Col className="d-flex justify-content-between align-items-center">
            <h5 className="content-sub-title mb-0">{t("pendidikanformal")}</h5>
          </Col>
        </Row>
        <Row>
          <Col xs="12" lg="12">
            <Table responsive>
              <thead>
                <tr>
                  <th className="text-center w-10">No.</th>
                  <th className="text-center">{t("jenjangpendidikan")}</th>
                  <th className="text-center">{t("institusi")}</th>
                  <th className="text-center">{t("jurusan")}</th>
                  <th className="text-center">{t("tahunmulai")}</th>
                  <th className="text-center">{t("tahunselesai")}</th>
                </tr>
              </thead>
              <tbody>
                {dataEducation &&
                  dataEducation.map((edu, idx) => (
                    <tr key={edu.id}>
                      <td className="text-center">{idx + 1}</td>
                      <td className="text-center">{edu.level}</td>
                      <td className="text-center">{edu.institution}</td>
                      <td className="text-center">{edu.major}</td>
                      <td className="text-center">{edu.yearStart}</td>
                      <td className="text-center">{edu.yearEnd}</td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </Col>
        </Row>

        <Row className="md-company-header mb-3 mt-5">
          <Col className="d-flex justify-content-between align-items-center">
            <h5 className="content-sub-title mb-0">{t("pengalamankerja")}</h5>
          </Col>
        </Row>
        <Row>
          <Col xs="12" lg="12">
            <Table responsive>
              <thead>
                <tr>
                  <th className="text-center w-10">No.</th>
                  <th className="text-center">{t("institusi")}</th>
                  <th className="text-center">{t("posisi")}</th>
                  <th className="text-center">{t("tahunmulai")}</th>
                  <th className="text-center">{t("tahunselesai")}</th>
                </tr>
              </thead>
              <tbody>
                {dataWorkingHistory &&
                  dataWorkingHistory.map((work, idx) => (
                    <tr key={work.id}>
                      <td className="text-center">{idx + 1}</td>
                      <td className="text-center">{work.institution}</td>
                      <td className="text-center">{work.description}</td>
                      <td className="text-center">{work.yearStart}</td>
                      <td className="text-center">{work.yearEnd}</td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </Col>
        </Row>

        <Row className="md-company-header mb-3 mt-5">
          <Col className="d-flex justify-content-between align-items-center">
            <h5 className="content-sub-title mb-0">{t("keahlian")}</h5>
          </Col>
        </Row>
        <Row>
          <Col xs="12" lg="12">
            <Table responsive>
              <thead>
                <tr>
                  <th className="text-center w-10">No.</th>
                  <th className="text-center">{t("keahlian")}</th>
                  <th className="text-center">Level</th>
                </tr>
              </thead>
              <tbody>
                {dataSkill &&
                  dataSkill.map((skill, idx) => (
                    <tr key={skill.id}>
                      <td className="text-center">{idx + 1}</td>
                      <td className="text-center">{skill.name}</td>
                      <td className="text-center">
                        <StarRatingComponent
                          name="testLevel"
                          starCount={5}
                          value={skill.level}
                        />
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

const ApplicantAssesment = ({ data: applicant }) => {
  const {
    detail: { dataAssessment, dateOfBirth: date },
  } = applicant;
  const {
    mbti: assessmentMbti,
    papikostik: assessmentPapikostick,
    video,
    bazi: assessmentBazi,
    fisiognomi,
  } = dataAssessment;

  return (
    <div>
      {assessmentMbti &&
        Boolean(assessmentMbti.length) &&
        assessmentMbti.map((ases, index) => (
          <AssessmentMbti ases={ases} key={index} />
        ))}

      {assessmentPapikostick[0] && (
        <AssessmentPapikostick papi={assessmentPapikostick[0]} />
      )}

      {video[0] && <AssessmentVideo vid={video[0]} />}

      {fisiognomi[0] && <AssessmentFisiognomi data={fisiognomi[0]} />}

      {assessmentBazi && <AssessmentBazi bazi={assessmentBazi} date={date} />}
    </div>
  );
};

function shuffleArray(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

const AssessmentFisiognomi = ({ data: fisiognomi }) => {
  const data = fisiognomi?.answers?.data;
  const [modalFisiog, setModalFisiog] = useState(false);
  const toggleFisiog = () => setModalFisiog(!modalFisiog);

  const karakterList = useMemo(() => {
    const arrKarakter = Object.values(data.result);
    return shuffleArray(arrKarakter);
  }, [data.result]);

  // const shuffleKarakter = (karakter) =>{
  //     var arrKarakter = Object.values(karakter);
  //         var currentIndex = arrKarakter.length, temporaryValue, randomIndex;
  //         while (0 !== currentIndex) {
  //             randomIndex = Math.floor(Math.random() * currentIndex);
  //             currentIndex -= 1;
  //             temporaryValue = arrKarakter[currentIndex];
  //             arrKarakter[currentIndex] = arrKarakter[randomIndex];
  //             arrKarakter[randomIndex] = temporaryValue;
  //         }
  // }

  return (
    <Card>
      <CardBody>
        <Row className="md-company-header mb-3 mt-2">
          <Col>
            <h5 className="text-uppercase content-sub-title mb-0">
              {t("hasilfisiognomi")}
            </h5>
          </Col>
        </Row>
        <Row>
          <Col md="6" lg="6" className="mb-2">
            <img
              src={data?.processed ?? data?.raw}
              width="100%"
              alt="fisiognomi"
            />
          </Col>
          <Col md="6" lg="6">
            {Object.values(data.result).length > 0 ? (
              <>
                <div className="mb-3">
                  {t("Karakteristik")} : <br />
                  <br />
                  {karakterList.map((ciri) => (
                    <ul>
                      <li>{ciri}</li>
                    </ul>
                  ))}
                </div>
                <div className="d-flex flex-row-reverse">
                  <Button
                    className="btn btn-netis-color"
                    onClick={toggleFisiog}
                  >
                    {t("lihatdetail")}
                  </Button>
                </div>
              </>
            ) : (
              <div className="alert alert-dark">
                {t("Belum ada hasil fisiognomi yang tersedia")}
              </div>
            )}
          </Col>
        </Row>
      </CardBody>

      <Modal isOpen={modalFisiog} toggle={toggleFisiog} className="modal-lg">
        <ModalHeader toggle={toggleFisiog}>{t("detailfisiognomi")}</ModalHeader>
        <ModalBody>
          {t("Bentuk Wajah")} : <br />
          <br />
          {Object.keys(data?.result ?? {}).map((ciriWajah) => (
            <ul>
              <li>{ciriWajah}</li>
            </ul>
          ))}
          {t("Karakteristik")} : <br />
          <br />
          {karakterList.map((ciriDetail) => (
            <ul>
              <li>{ciriDetail}</li>
            </ul>
          ))}
        </ModalBody>
      </Modal>
    </Card>
  );
};

const AssessmentMbti = ({ ases }) => {
  const [modalAses, setModalAses] = useState(false);
  const toggleAses = () => setModalAses(!modalAses);

  return (
    <>
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col className="d-flex justify-content-between align-items-center">
              <h5 className="text-uppercase content-sub-title mb-0">
                {ases.testName} - {ases.result}
              </h5>
              <button className="btn btn-netis-color" onClick={toggleAses}>
                {t("lebihlanjut")}
              </button>
            </Col>

            <div className="col-12 d-flex mb-3 text-muted">
              <i>{moment(ases.created_at).format("DD MMMM YYYY")}</i>
            </div>
          </Row>

          <div className="p-3 rounded border">
            <ProgressGroup
              typeA={"Introvert"}
              valueA={ases.scores.introvert}
              typeB={"Extrovert"}
              valueB={ases.scores.extrovert}
            />
            <ProgressGroup
              typeA={"Sensing"}
              valueA={ases.scores.sensing}
              typeB={"Intuition"}
              valueB={ases.scores.intuition}
            />
            <ProgressGroup
              typeA={"Feeling"}
              valueA={ases.scores.feeling}
              typeB={"Thinking"}
              valueB={ases.scores.thinking}
            />
            <ProgressGroup
              typeA={"Judging"}
              valueA={ases.scores.judging}
              typeB={"Perceiving"}
              valueB={ases.scores.perceiving}
            />
          </div>
        </CardBody>
      </Card>

      <Modal isOpen={modalAses} toggle={toggleAses} className="modal-lg">
        <ModalHeader toggle={toggleAses}>{t("detailasesmen")}</ModalHeader>
        <ModalBody>
          <div className="mb-8">
            <h3>{t("karakteristik")}</h3>
            <ReactMarkdown source={ases.result_detail.characterisctics} />
          </div>

          <div className="mb-3 mt-2">
            <h3>{t("fungsikognitif")}</h3>
            <h4 className="h5">
              <i>{t("kemampuanberpikir")}</i>
            </h4>
          </div>

          <div className="row">
            <div className="col-sm-6">
              <div className="border rounded p-3 h-100">
                <h5>{t("fungsidominan")}</h5>
                <i>
                  <ReactMarkdown source={ases.result_detail.dominan.name} />
                </i>
                <ReactMarkdown source={ases.result_detail.dominan.desc} />
              </div>
            </div>

            <div className="col-sm-6">
              <div className="border rounded p-3 h-100">
                <h5>{t("fungsisekunder")}</h5>
                <i>
                  <ReactMarkdown source={ases.result_detail.sekunder.name} />
                </i>
                <ReactMarkdown source={ases.result_detail.sekunder.desc} />
              </div>
            </div>
            <div className="col-sm-12 mt-3">
              <h3>{t("partneralami")}</h3>
              <span className="text-uppercase h5">
                &nbsp;&nbsp;&nbsp;&nbsp;<i>{ases.result_detail.partner1}</i> &{" "}
                <i>{ases.result_detail.partner2}</i>
              </span>
            </div>
            <div className="col-sm-12 mt-3">
              <h3>{t("saranpengembangan")}</h3>
              <ReactMarkdown source={ases.result_detail.suggestion} />
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
};

const AssessmentPapikostick = ({ papi }) => {
  const [modalPapi, setModalPapi] = useState(false);
  const togglePapi = () => setModalPapi(!modalPapi);

  const groupingLabel = {
    F: { category: "Followership", color: "#e53935" },
    W: { category: "Followership", color: "#e53935" },
    N: { category: "Work Direction", color: "#8e24aa" },
    G: { category: "Work Direction", color: "#8e24aa" },
    A: { category: "Work Direction", color: "#8e24aa" },
    L: { category: "Leadership", color: "#3949ab" },
    P: { category: "Leadership", color: "#3949ab" },
    I: { category: "Leadership", color: "#3949ab" },
    T: { category: "Activity", color: "#039be5" },
    V: { category: "Activity", color: "#039be5" },
    X: { category: "Social Nature", color: "#00897b" },
    S: { category: "Social Nature", color: "#00897b" },
    B: { category: "Social Nature", color: "#00897b" },
    O: { category: "Social Nature", color: "#00897b" },
    C: { category: "Work Style", color: "#7cb342" },
    D: { category: "Work Style", color: "#7cb342" },
    R: { category: "Work Style", color: "#7cb342" },
    Z: { category: "Temperament", color: "#fb8c00" },
    E: { category: "Temperament", color: "#fb8c00" },
    K: { category: "Temperament", color: "#fb8c00" },
  };
  const colorsByIndex = Object.keys(groupingLabel).map(
    (label) => groupingLabel[label].color
  );

  const data = {
    labels: "FWNGALPITVXSBORDCZEK".split(""),
    datasets: [
      {
        label: "RESULT",
        borderWidth: 1,
        pointRadius: 2,
        backgroundColor: "rgba(0,0,0,0.3)",
        borderColor: (option) => {
          // console.log(option);
          return colorsByIndex[option.dataIndex];
        },
        pointBackgroundColor: (option) => colorsByIndex[option.dataIndex],
        pointBorderColor: (option) => colorsByIndex[option.dataIndex],
        data: "FWNGALPITVXSBORDCZEK".split("").map((huruf) => {
          if (huruf === "Z") {
            return 9 - papi.scores[huruf];
          } else if (huruf === "K") {
            return 9 - papi.scores[huruf];
          } else return papi.scores[huruf];
        }),
      },
    ],
  };

  const options = {
    tooltips: {
      callbacks: {
        title: (tooltipItem, data) => {
          return (
            groupingLabel[data.labels[tooltipItem[0].index]].category +
            " (" +
            data.labels[tooltipItem[0].index] +
            ")"
          );
        },
        label: (tooltipItem) => parseInt(tooltipItem.value),
      },
    },
    legend: {
      display: false,
    },
    title: {
      display: true,
      // text: 'Hasil PAPIKostick'
    },
    scale: {
      gridLines: {
        display: false,
        circular: true,
      },
      angleLines: {
        // display: false,
      },
      ticks: {
        display: false,
        max: 9,
        min: 0,
        stepSize: 1,
        beginAtZero: true,
        showLabelBackdrop: true,
      },
      pointLabels: {
        display: false,
        fontStyle: "bold",
        fontSize: 12,
        fontColor: Object.keys(groupingLabel).map(
          (label) => groupingLabel[label].color
        ),
      },
    },
  };

  const template = {
    Followership: ["F", "W"],
    "Work Direction": ["N", "G", "A"],
    Leadership: ["L", "P", "I"],
    Activity: ["T", "V"],
    "Social Nature": ["X", "S", "B", "O"],
    "Work Style": ["C", "D", "R"],
    Temperament: ["Z", "E", "K"],
  };
  const groupingDesc = {
    F: t("Keb Membantu Atasan"),
    W: t("Keb Mengikuti Aturan dan Pengawasan"),
    N: t("Keb dalam Menyelesaikan Tugas (Kemandirian)"),
    G: t("Peran Pekerja Keras"),
    A: t("Keb dalam Berprestasi"),
    L: t("Peran Kepemimpinan"),
    P: t("Keb Mengatur Orang Lain"),
    I: t("Peran Dalam Membuat Keputusan"),
    T: t("Peran Dalam Kesibukan Kerja"),
    V: t("Peran Dalam Semangat Kerja"),
    X: t("Keb untuk Diperhatikan"),
    S: t("Peran Dalam Hubungan Sosial"),
    B: t("Keb Diterima dalam Kelompok"),
    O: t("Keb Kedekatan dan Kasih Sayang"),
    C: t("Peran Dalam Mengatur"),
    D: t("Peran Bekerja dengan Hal-hal Rinci"),
    R: t("Peran Penalaran Teoritis"),
    Z: t("Keb untuk Berubah"),
    E: t("Peran Dalam Pengendalian Emosi"),
    K: t("Keb untuk Agresif"),
  };

  const group =
    papi.result &&
    Object.keys(papi.result).map((category) => {
      const item =
        papi.result &&
        template[category].map((code, idx) => ({
          code: code,
          scores: papi.scores[code],
          description: papi.result[category][idx],
        }));

      return {
        kategory: category,
        items: item,
      };
    });

  function titleColor(resultTitle) {
    switch (resultTitle) {
      case "Followership":
        return "#e53935";
      case "Work Direction":
        return "#8e24aa";
      case "Leadership":
        return "#3949ab";
      case "Activity":
        return "#039be5";
      case "Social Nature":
        return "#00897b";
      case "Work Style":
        return "#7cb342";
      case "Temperament":
        return "#fb8c00";
      default:
        return "#FFFFFF";
    }
  }

  return (
    <>
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col className="d-flex justify-content-between align-items-center">
              <h5 className="content-sub-title mb-0">PAPI Kostick</h5>
              <button className="btn btn-netis-color" onClick={togglePapi}>
                {t("lihatdetail")}
              </button>
            </Col>
          </Row>

          <div className="mx-auto grafik-papikostick-admin">
            <Responsive displayIn={["mobile"]}>
              <Radar data={data} options={options} width={100} height={92} />
            </Responsive>
            <Responsive displayIn={["tablet"]}>
              <Radar data={data} options={options} width={100} height={67} />
            </Responsive>
            <Responsive displayIn={["laptop"]}>
              <Radar data={data} options={options} width={100} height={48} />
            </Responsive>
          </div>
        </CardBody>
      </Card>

      <Modal isOpen={modalPapi} toggle={togglePapi} className="modal-xl">
        <ModalHeader toggle={togglePapi}>Hasil PAPI Kostick</ModalHeader>
        <ModalBody>
          <Table responsive className="table-black">
            <thead>
              <tr className="border-bottom">
                <th className="text-center w-10">{t("Kategori")}</th>
                <th className="text-center w-25">{t("Aspek")}</th>
                <th className="text-center w-15">{t("Nilai")}</th>
                <th className="text-center w-50">{t("deskripsi")}</th>
              </tr>
            </thead>
            <tbody>
              {group &&
                group.map((objKategori) => {
                  return (
                    objKategori &&
                    objKategori.items.map((objItem, idx) => (
                      <tr>
                        {idx === 0 && (
                          <td
                            rowSpan={objKategori.items.length}
                            style={{
                              backgroundColor: titleColor(objKategori.kategory),
                              color: "#FFFFFF",
                            }}
                          >
                            {objKategori.kategory}
                          </td>
                        )}
                        <td>
                          {objItem.code} : {groupingDesc[objItem.code]}
                        </td>
                        <td>
                          <Star value={(objItem.scores + 1) / 2} />(
                          {objItem.scores})
                        </td>
                        <td>{objItem.description}</td>
                      </tr>
                    ))
                  );
                })}
            </tbody>
          </Table>
        </ModalBody>
      </Modal>
    </>
  );
};

const AssessmentVideo = ({ vid }) => {
  const vidRef = useRef(null);
  const vidRef2 = useRef(null);
  const [showButton, setShowButton] = useState(true);
  const [showButton2, setShowButton2] = useState(true);
  const firstVideo = React.useMemo(
    () =>
      vid.answers.data[0]?.processed ?? { resultUrl: vid.answers.data[0]?.raw },
    [vid]
  );
  const firstWidya = React.useMemo(() => vid.answers.data[0].video, [vid]);
  const firstTitle = React.useMemo(() => vid.answers.data[0].title, [vid]);
  const [activeVideo, setActiveVideo] = useState(firstVideo);
  const [activeWidya, setActiveWidya] = useState(firstWidya);
  const [activeTitle, setActiveTitle] = useState(firstTitle);

  function changeVideo(video) {
    setActiveWidya(video.video);
    setActiveTitle(video.title);
    if (video.processed) {
      setActiveVideo(video.processed);
    } else {
      setActiveVideo({ resultUrl: video.raw });
    }
  }

  let highestEmotion = null;
  const resultEmotion = Object.keys(
    activeVideo?.resultData?.emotion ?? {}
  ).map((label) => ({ label, value: activeVideo.resultData.emotion[label] }));
  for (const emotion of resultEmotion) {
    if (highestEmotion == null) {
      highestEmotion = emotion;
    } else if (parseFloat(emotion.value) > parseFloat(highestEmotion.value)) {
      highestEmotion = emotion;
    }
  }

  let highestEyecue = null;
  const resultEyecues = Object.keys(
    activeVideo?.resultData?.eyecues ?? {}
  ).map((label) => ({ label, value: activeVideo.resultData.eyecues[label] }));
  for (const emotion of resultEyecues) {
    if (highestEyecue == null) {
      highestEyecue = emotion;
    } else if (emotion.value > highestEyecue.value) {
      highestEyecue = emotion;
    }
  }

  const playVid = () => {
    vidRef2.current.pause();
    vidRef.current.play();
    setShowButton(false);
  };

  const playVid2 = () => {
    vidRef.current.pause();
    vidRef2.current.play();
    setShowButton2(false);
  };

  const pauseVid2 = () => {
    vidRef2.current.pause();
    setShowButton2(true);
  };

  return (
    <>
      <Card>
        <CardBody>
          <Row className="md-company-header flex-column mb-3 mt-2">
            <Col className="col-12 d-flex mb-1">
              <h5 className="text-uppercase content-sub-title mb-0">
                {vid.testName} ASESMEN{" "}
              </h5>
            </Col>
            {/* <img src={require("../../../../assets/img/16personalities/entj.png")} alt="transparent" /> */}
            <Col className="col-12 d-flex mb-3 text-muted">
              <i>{moment(vid.created_at).format("DD MMMM YYYY")}</i>
            </Col>
          </Row>

          <Row>
            <Col lg="6" md="8">
              {activeVideo && (
                <>
                  <div className="vidwrapper">
                    <video
                      ref={vidRef}
                      className="vid"
                      src={activeVideo.resultUrl}
                      width="100%"
                      height="100%"
                      id="dataVid"
                      controls
                      onPlaying={() => setShowButton(false)}
                      onPause={() => setShowButton(true)}
                    />
                    {showButton ? (
                      <>
                        <p className="title text-capitalize">
                          &nbsp;&nbsp;&nbsp;{activeTitle}&nbsp;&nbsp;&nbsp;
                        </p>
                        <Button rounded className="play" onClick={playVid}>
                          <i className="fa fa-2x fa-align-center fa-play"></i>
                        </Button>
                        <div className="vidwrapperWidya">
                          <video
                            ref={vidRef2}
                            className="smallVid"
                            src={activeWidya}
                            onPlaying={() => setShowButton2(false)}
                            onPause={() => setShowButton2(true)}
                          />
                          {showButton2 ? (
                            <>
                              <small className="question">
                                {t("Pertanyaan")} :&nbsp;
                              </small>
                              <Button
                                rounded
                                className="play2"
                                onClick={playVid2}
                              >
                                <i className="fa fa-sm fa-align-center fa-play"></i>
                              </Button>
                            </>
                          ) : (
                            <Button
                              rounded
                              className="pause"
                              onClick={pauseVid2}
                            >
                              <i className="fa fa-sm fa-align-center fa-pause"></i>
                            </Button>
                          )}
                        </div>
                      </>
                    ) : null}
                  </div>

                  {highestEmotion || highestEyecue ? (
                    <>
                      {highestEmotion && (
                        <div>
                          <strong>{highestEmotion.label}</strong> :{" "}
                          {highestEmotion.value}
                        </div>
                      )}
                      {highestEyecue && (
                        <div>
                          <strong>{highestEyecue.label}</strong> :{" "}
                          {highestEyecue.value}
                        </div>
                      )}
                    </>
                  ) : (
                    <div className="alert alert-dark">
                      {t("Sistem sedang menganalisa video ini")}
                    </div>
                  )}
                </>
              )}
            </Col>
            <Col lg="6" md="4">
              {vid.answers.data &&
                vid.answers.data.map((video, idx) => (
                  <ListGroup className="align-middle">
                    <ListGroupItem
                      className="d-flex align-items-center"
                      onClick={() => changeVideo(video)}
                      key={video.title}
                      tag="button"
                      action
                    >
                      <i className="fa fa-video-camera mr-2"></i>
                      {idx + 1}. {video.title}
                      {video?.processed == null ? (
                        <i className="fa fa-hourglass-half text-info ml-auto"></i>
                      ) : (
                        <i className="flex-row-reverse fa fa-check-circle-o text-success ml-auto"></i>
                      )}
                    </ListGroupItem>
                  </ListGroup>
                ))}
              {/* <Modal isOpen={Boolean(showVideo)} toggle={toggleVideo} size="xl">
                                        <ModalHeader toggle={toggleVideo}>{showVideo?.title}
                                        </ModalHeader>
                                        <ModalBody>
                                            <Row>
                                                <Col lg="7" style={{ minHeight: 500 }}>
                                                    <video width="100%" src={showVideo?.processed?.resultUrl ?? showVideo?.raw} height="100%" controls autoPlay>
                                                    </video>
                                                </Col>
                                                <Col lg="5">
                                                    {showVideo?.processed?.resultData && Object.keys(showVideo.processed.resultData).map((key,idx) =>
                                                        <div key={idx} className="p-3 rounded border mb-3"><p className="text-uppercase">{key}</p>
                                                        {showVideo?.processed.resultData && Object.keys(showVideo.processed.resultData[key]).map((media,value,id) =>
                                                            <div key={id} className="p-3 rounded border mb-1 text-uppercase">
                                                                <ProgressVideo
                                                                    type={media}
                                                                    value={showVideo.processed.resultData[key][media]}
                                                                />
                                                            </div>
                                                        )}
                                                        </div>
                                                    )}
                                                </Col>
                                            </Row>
                                        </ModalBody>
                                    </Modal> */}
            </Col>
          </Row>
        </CardBody>
      </Card>
    </>
  );
};

const AssessmentBazi = ({ bazi, date }) => {
  const [showBaziDescription, setShowBaziDescription] = useState(false);

  return (
    <>
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col className="d-flex flex-column">
              <h5 className="text-uppercase content-sub-title mb-2">
                BAZI - {bazi.title}
              </h5>
              {/* <br /> */}
              <div className="my-2">
                {t("harilahir")} : &nbsp;
                {moment(date).format("dddd")}
                &nbsp;<i>({moment(date).format("DD MMMM YYYY")})</i>
              </div>
              <div>
                <button
                  className="btn btn-netis-color"
                  onClick={() => setShowBaziDescription(true)}
                >
                  {t("Baca Deskripsi")}
                </button>
              </div>
            </Col>
          </Row>
          <Row>
            {bazi.positive_traits && (
              <Col sm="6" md="4" lg="3">
                <h5>{t("Sifat Positif")}</h5>
                <ReactMarkdown source={bazi.positive_traits} />
              </Col>
            )}
            {bazi.negative_traits && (
              <Col sm="6" md="4" lg="3">
                <h5>{t("Sifat Negatif")}</h5>
                <ReactMarkdown source={bazi.negative_traits} />
              </Col>
            )}
          </Row>
        </CardBody>
      </Card>
      <Modal
        isOpen={showBaziDescription}
        toggle={() => setShowBaziDescription(false)}
        size="lg"
      >
        <ModalHeader toggle={() => setShowBaziDescription(false)}>
          {bazi.title}
        </ModalHeader>
        <ModalBody>
          <div>
            <ReactMarkdown source={bazi.description} />
            <hr />
            <div className="row">
              <div className="col-6">
                <h5>{t("Sifat Positif")}</h5>
                <ReactMarkdown source={bazi.positive_traits} />
              </div>
              <div className="col-6">
                <h5>{t("Sifat Negatif")}</h5>
                <ReactMarkdown source={bazi.negative_traits} />
              </div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
};

function ProgressGroup({ typeA, typeB, valueA, valueB }) {
  return (
    <div className="progress-group">
      <div className="progress-group-prepend">
        <span
          className={
            "progress-group-text" +
            (valueA >= valueB ? " font-weight-bold" : "")
          }
        >
          {typeA}
        </span>
      </div>

      <div className="progress-group-bars">
        <Progress multi>
          <Progress bar color="netis-color" value={valueA}>
            {valueA} %
          </Progress>
          <Progress bar color="success" value={valueB}>
            {valueB} %
          </Progress>
        </Progress>
      </div>

      <div className="progress-group-prepend ml-3 text-right">
        <span
          className={
            "progress-group-text" +
            (valueA <= valueB ? " font-weight-bold" : "")
          }
        >
          {typeB}
        </span>
      </div>
    </div>
  );
}

// function ProgressVideo({ type, value }) {
//     return (
//         <div className="progress-group">
//             <div className="progress-group-prepend">
//                 <span className='progress-group-text font-weight-bold'>
//                     {type}
//                 </span>
//             </div>

//             <div className="progress-group-bars">
//                 <Progress multi>
//                     <Progress bar color="netis-color" value={value}>{value} %</Progress>
//                     {/* <Progress bar color="light" value={100-value}></Progress> */}
//                 </Progress>
//             </div>
//         </div>
//     );
// }

export default translate(ApplicantDetail);
