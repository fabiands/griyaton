import React, { Component } from 'react';
import Hero from './Hero';
import About from './About';
import Features from './Features';
import PageLayout from '../Pages/Layout/PageLayout';

class LandingPage extends Component {
    render() {
        return (
            <PageLayout>
                <div className="landing-page">
                    <Hero />
                    <Features />
                    <About />
                </div>
            </PageLayout>
        )
    }
}

export default LandingPage;
