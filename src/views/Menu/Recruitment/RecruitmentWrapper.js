import React, { Suspense } from 'react'
import { Switch, Redirect, withRouter } from 'react-router-dom'
import { TabContent, TabPane } from 'reactstrap';
// import { matchWildcard } from '../../../utils/formatter';
import LoadingAnimation from '../../../components/LoadingAnimation';
import { t, translate } from 'react-switch-lang';
import AuthRoute from '../../../components/AuthRoute';

const RecruitmentFirst = React.lazy(() => import('./RecruitmentFirst'));
const RecruitmentMenu = React.lazy(() => import('./RecruitmentMenu'));
const RecruitmentCreate = React.lazy(() => import('./RecruitmentCreate'));
// const ApplicantList = React.lazy(() => import('./Applicants/ApplicantList'));
const ApplicantDetail = React.lazy(() => import('./Applicants/ApplicantDetail'));
const RecruitmentDetail = React.lazy(() => import('./RecruitmentDetail'));
const VacancyApplicantList = React.lazy(() => import('./Applicants/VacancyApplicantList'));

function RecruitmentWrapper({ location, match }) {
    const routes = [
        { path: match.path + '/', exact: true, privileges: ['browse-job'], component: RecruitmentFirst },
        { path: match.path + '/vacancies', exact: true, privileges: ['browse-job'], component: RecruitmentMenu, tab: t('lowongan'), active: match.path + '/vacancies*' },
        { path: match.path + '/vacancies/create', exact: true, privileges: ['add-job'], component: RecruitmentCreate },
        { path: match.path + '/vacancies/:id/edit', exact: true, privileges: ['edit-job'], component: RecruitmentDetail },
        { path: match.path + '/vacancies/:id/applicants', exact: true, privileges: ['browse-job-applicant'], component: VacancyApplicantList },
        // {path: match.path + '/applicants', exact: true, component: ApplicantList, tab: t('daftarpelamar'), active: match.path + '/applicants*'},
        { path: match.path + '/applicants/:applicantId', exact: true, privileges: ['read-job-applicant'] , component: ApplicantDetail },
    ]
    return (
        <div>
            <TabContent>
                <TabPane>
                    <Suspense fallback={<LoadingAnimation />}>
                        <Switch>
                            {routes.map(route => (
                                <AuthRoute key={route.path} {...route} />
                            ))}
                            {routes[0] && <Redirect exact from={match.path} to={routes[0].path} />}
                        </Switch>
                    </Suspense>
                </TabPane>
            </TabContent>
        </div>
    )
}

export default translate(withRouter(RecruitmentWrapper));
